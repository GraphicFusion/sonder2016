(function() {

    wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true        // default
    });
    wow.init();

})();

jQuery(document).ready(function($) {
	// Used for sticky footer
	/*$(window).on('load resize', function () {
		$('.site-content').css('margin-bottom', $('.site-footer').outerHeight() );
	});*/

});
