jQuery(document).ready(function($) {
	$('.block-slider .slider-block').slick({
		centerMode: true,
		centerPadding: '11.875%',
		slidesToShow: 3,
		easing: 'swing',
		arrows: true,
		autoplay: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				}
			}
		]
	});
});