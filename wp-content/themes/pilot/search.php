<?php get_header(); ?>
	
	<?php if ( have_posts() ) : ?>
    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	<header class="page-header" style="background-position: right; background-repeat:no-repeat; background-size: cover; background-image: url('<?php echo $thumb['0'];?>')">
	   <h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'pilot' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
       
       <?php //the_post_thumbnail( 'full' ); ?>
       <div class="banner-overlay">&nbsp;</div>
	</header><!-- .page-header -->
	<div class="clearfix"></div>
    <div class="container">
    	<div class="search-result-div">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'views/content', 'search' ); ?>
		<?php endwhile; ?>
        </div>
        </div>
		<div class="container">
        <div class="search-result-div">
		<?php the_posts_navigation(); ?>
        </div>
        </div>
	<?php else : ?>
      	
		<?php get_template_part( 'views/content', 'none' ); ?>
        
	<?php endif; ?>
	
<?php get_footer(); ?>