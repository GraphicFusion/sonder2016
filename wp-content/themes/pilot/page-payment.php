<?php 
/*
Template Name: Payment
*/
get_header(); 
global $output;
?>

<div class="layout-container">
   <header>
      <img class="wow animated fadeInRight payment-logo" src="<?php echo get_site_url(); ?>/wp-content/themes/pilot/images/sonder-logo-full.svg" alt="Sonder Agency" title="Sonder Agency" />
      <h1>Payment</h1>
   </header>
	
	<?php while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>
	<?php if(!empty($output) && !empty($output['id'])) : ?>
	<?php $receipt = false; if( $receipt ) : ?>
   <table style="width: 700px; margin: 0 auto; margin-top:50px; font-family: arial; font-size:16px; color: #333; border: 1px solid #C0C0C0; border-collapse:collapse; ">
      <tbody>
         <tr>
            <td style="text-align:center; padding:15px 0;"><img width="120" height="40" src="https://ccc.basecommercesandbox.com/email_templates/images/bc_logo_colored_small_400x120.png" alt=""></td>
         </tr>
         <tr style="center center no-repeat; background-color: #14426E;">
            <td style="text-align:center; padding:20px 0;">
               <h1 style="color: #fff; font-size: 28px; letter-spacing:1px; font-weight: 300;">PAYMENT RECEIPT</h1>
            </td>
         </tr>
         <tr>
            <td style="text-align:center; padding:10px 0 20px;"><b style="font-size:20px; color:#434a66; line-height: 30px;">Sonder Agency, LLC</b><br>Sandbox DBA<br>TEMPE, AZ 85280<br>(623) 694 - 7560<br></td>
         </tr>
         <tr style="background: #14426E;">
            <td style="text-align:center; padding: 10px 0;"><span style="color:#FFFFFF; letter-spacing: .5px;">Transaction ID:&nbsp;<?php echo $output['id']; ?></span></td>
         </tr>
         <tr style="padding-top:30px;">
            <td>
               <h1 style="text-align:center; font-size:28px; font-weight:300; letter-spacing:1px; color:#434a66;">Transaction Details</h1>
            </td>
         </tr>
         <tr style=" right bottom  no-repeat;">
            <td><br><table style="font-family: arial;">
                  <tbody>
                     <tr style="line-height: 10px;">
                        <td width="80"><img src="https://ccc.basecommercesandbox.com/email_templates/images/clock_icon.png" style="padding-left:42px;" width="23" height="23" alt=""></td>
                        <td width="215" style="color: #163f64;"><b>Date/Time:</b></td>
                        <td><?php echo date('l, F j Y h:i A T' ); ?></td>
                     </tr>
                  </tbody>
               </table><br><table style="font-family: arial;">
                  <tbody>
                     <tr style="line-height: 10px;">
                        <td width="80"><img src="https://ccc.basecommercesandbox.com/email_templates/images/folder_icon.png" style="padding-left: 42px;" height="23" width="23" alt=""></td>
                        <td width="215" style="color: #163f64;"><b>Account Name:</b></td>
                        <td><?php echo $output['card_name']; ?></td>
                     </tr>
                     <tr>
                        <td width="80"></td>
                        <td width="215" style="color: #163f64;"><b>Card:</b></td>
                        <td>
                           Ending in
                           <?php echo $output['card_num']; ?>
                        </td>
                     </tr>
                  </tbody>
               </table><br><table style="font-family: arial;">
                  <tbody>
                     <tr style="line-height: 10px;">
                        <td width="80"><img src="https://ccc.basecommercesandbox.com/email_templates/images/transaction_icon.png" style="padding-left: 42px;" height="23" width="23" alt=""></td>
                        <td width="215" style="color: #163f64;"></td>
                        <td></td>
                     </tr>
                     <tr>
                        <td width="80"></td>
                        <td width="215" style="color: #163f64;"><b>Transaction Type:</b></td>
                        <td>SALE</td>
                     </tr>
                     <tr>
                        <td width="80"></td>
                        <td width="215" style="color: #163f64;"><b>Transaction Status:</b></td>
                        <td>CAPTURED</td>
                     </tr>
                  </tbody>
               </table><br><table style="padding:0 80px; font-family: arial; width: 100%;">
                  <tbody>
                     <tr>
                        <td style="padding-top:20px; text-align:center;">
                           This will appear on your statement as<br>Sonder Agency, LLC
                        </td>
                     </tr>
                     <tr>
                        <td style="text-align:center; padding-top: 20px; padding-bottom:30px;"></td>
                     </tr>
                  </tbody>
               </table>
               <table style="font-family: arial; width: 100%;">
                  <tbody></tbody>
               </table>
            </td>
         </tr>
         <tr style="background: #14426E;">
            <td style="padding:10px 20px 10px 80px; color: white; font-size:18px;"><span style="display: inline-block; width:  475px;">
                  Total:
                  </span>
               $<?php echo $output['amount']; ?>
            </td>
         </tr>
      </tbody>
   </table>
<?php endif; ?>
	<?php elseif( !empty($mssgs)) : ?>
		There was an error processing your payment.
		Message: <?php print_r($mssgs); ?>
	<?php endif; ?>
</div>
<img class="footer-logo" src="https://sonderagency.com/wp-content/themes/pilot/images/Sonder-Footer-Logo.svg" alt="">
<?php get_footer(); ?>