			</main>
			<?php pilot_get_sidebar(); ?>
			</div><!-- /site-content -->
			<?php get_template_part( 'views/footer' ); ?>
		</div><!-- /site -->
		<?php wp_footer(); ?>
        
	</body>
    
<?php /*?><script type="text/javascript">
$(document).ready(function(){ 
$('.menu-toggle').click(function(){
   //$('.office').slideUp();
   $(this).next('.menu-navigation-container').slideToggle();
   return false;
});
});
</script><?php */?>

<script src="<?php echo get_template_directory_uri(); ?>/dest/js/html5lightbox.js" type="text/javascript"></script>

<script type="text/javascript">
$(window).load(function(){
$('#menu-id').on('click', function(e){
    e.preventDefault();
    var target= $(this).get(0).id == 'menu-id' ? $('#footer_menu') : $('#up');
    $('html, body').stop().animate({
       scrollTop: target.offset().top
    }, 1000);
});
});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-99969665-1', 'auto');
  ga('send', 'pageview');

</script>
  
</html>