<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if( $pilot->use_default_page_titles && !get_field('hide_title') ) : ?>
		<header class="entry-header no-media-title">
        <section class="wow fadeInUp" data-wow-duration="1s">
			<?php the_title( '<h2 class="entry-title"><span class="entry-title-span">', '</span></h2>' ); ?>
		</section>
        </header>
	<?php endif; ?>
	<div class="entry-content">
    <div class="no-media-page-content">
    	<div class="container">
	    	<div class="no-media-content-inner">
					<?php the_content(); ?>
        </div>
      </div>
    </div>
		<?php 
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pilot' ),
				'after'  => '</div>',
			) );
	get_all_blocks(); // defined in /inc/content-blocks.php
		?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'pilot' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer>
</article>