<footer class="site-footer loading">
	<div class="site-info">
    <div class="container">
    <div class="footer_col_sec">
    <div class="wow fadeInUp" data-wow-duration="2s">
    <div class="col-4 col-sm-4" id="footer_menu">
    <section class="footer-sec">
    	<h2 class="widget-title">Menu</h2>
		<?php	if ( has_nav_menu( 'footer' ) ) { 
        wp_nav_menu( array( 'theme_location' => 'footer') ); 
        } ?>
        </section>
    </div>
    </div>
    
    <div class="wow fadeInUp" data-wow-duration="2s">
    <div class="col-4 col-sm-4">
    <?php dynamic_sidebar( 'footer2' ); ?>
    </div>    
    </div>
    
    <div class="wow fadeInUp" data-wow-duration="2s">
    <div class="col-right-4">
    <?php dynamic_sidebar( 'footer3' ); ?>
    </div>
    </div>
    
    </div>
    <p class="copyright"><span>&copy;</span><?php echo date("Y"); ?> <a href="http://sonderagency.com/">Sonder Agency.</a> All Rights Reserved. <a href="<?php echo get_template_directory(); ?>/docs/PrivacyPolicy.doc">Privacy Policy</a></p>
	</div>
	</div><!-- .site-info -->
    
        <div class="footer_bottom_img">
        <div class="container">
			<img src="https://sonderagency.com/wp-content/themes/pilot/images/Sonder-Footer-Logo.svg" alt="Sonder Agency" />
            </div>
        </div>
    
</footer>