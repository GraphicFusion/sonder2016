<?php
	$color_id = get_field('page_header_color');
	$header_class = get_color_class( $color_id );
?>

<header id="masthead" class="site-header <?php echo $header_class; ?>">
    <div class="top_section">
	<div class="container">
    <div class="brandingNav">
    <div class="col-3" style="position: relative; z-index: 1">

    <div class="site-branding">
        <h1>Sonder Agency - Web Design and Branding in Tucson, Arizona</h1>
        <div class="logo"><?php //$theme = wp_get_theme(); echo $theme->name; ?> 
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><svg xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 102 24"><defs><style>.cls-1{fill:#fff;}</style></defs><title>Sonder-Logo</title><path class="cls-1" d="M84.33,19.66h0a6,6,0,0,1-2.22.39,4.3,4.3,0,0,1-4.24-3.74L87,13.84h0C87,10.08,84.15,7.4,80,7.4c-4.36,0-7.77,3.3-7.77,8.64,0,5.16,3.51,8,7.9,8a6.86,6.86,0,0,0,7-5.63l-0.77-.21A5,5,0,0,1,84.33,19.66ZM80,8.7c1.41,0,1.9,1.67,1.9,4.58l-4.2,1.12C77.67,13.12,77.75,8.7,80,8.7ZM70.55,0L62.9,2.06V2.9l1.56,1V8.34a4.46,4.46,0,0,0-2.79-.79c-4.39,0-8,3-8,8.87,0,5,2.53,7.58,6,7.58a5.48,5.48,0,0,0,4.8-2.78V24l7.65-2.06V21.1l-1.56-1V0ZM64.47,20.49a4.09,4.09,0,0,1-1.29.2c-2.09,0-3.33-2.42-3.33-6,0-3.18.9-5.28,2.84-5.28,1.31,0,1.72.81,1.77,2.35v8.76ZM99.12,7.4A2.78,2.78,0,0,0,97,8.28a16.45,16.45,0,0,0-2.08,3.22l0-4.34L87.23,9.24v0.85l1.56,1V24l7.65-2.06V21.1l-1.56-1,0-6.62,1.74-1.91A2.75,2.75,0,1,0,99.12,7.4ZM52,12.24C52,9,50.34,7.4,47.48,7.4a6.76,6.76,0,0,0-5.51,3V7.14L34.33,9.19v0.89l1.56,1V21.85l-1.56,1V23.7h9.21V22.86l-1.56-1V11.32a6.4,6.4,0,0,1,2.18-.38A1.56,1.56,0,0,1,46,12.77V23.7h7.65V22.86l-1.56-1V12.24Zm-42.6-2c-3-.83-4.22-1.59-4.22-3.42,0-1.62,1.21-2.56,2.89-2.56,2.74,0,4.3,1.92,5,4.78h1.44V4.48A11,11,0,0,0,8.12,2.71C3.67,2.71.34,5.31,0.34,9.64c0,3.63,2.15,5.66,6.25,6.78C9.74,17.28,11,18.1,11,19.87s-1.33,2.59-3.27,2.59c-3,0-5-2.42-5.72-5.6H0.25v5A12.3,12.3,0,0,0,7.71,24c4.6,0,8.34-2.06,8.34-6.81C16.05,12.86,13.34,11.29,9.45,10.2ZM25.68,7.4c-5,0-8.55,3.38-8.55,8.3h0c0,4.92,3.51,8.3,8.55,8.3s8.55-3.38,8.55-8.3h0C34.23,10.78,30.72,7.4,25.68,7.4Zm0,15.3c-1.56,0-2.09-3.32-2.09-7s0.53-7,2.09-7,2.09,3.32,2.09,7S27.24,22.7,25.68,22.7Z"/><wappalyzerData id="wappalyzerData" style="display: none"/></svg></a></div>
    </div><!-- .site-branding -->

    </div>
    
    <div class="col-9">
    <nav id="site-navigation" class="main-navigation">
        <?php /*?><button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'pilot' ); ?></button><?php */?>
        <a href="#footer_menu" id="menu-id" class="menu-toggle">Menu</a>
        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
    </nav>
    </div>
    </div>
	</div>
    </div>
</header>