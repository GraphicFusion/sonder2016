<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if( $pilot->use_default_page_titles && !get_field('hide_title') ) : ?>
		<header class="entry-header">
        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
       <div class="single-blog-header" style="background-repeat:no-repeat; background-size: cover; background-image: url('<?php echo $thumb['0'];?>')">
       <div class="banner-overlay">&nbsp;</div>
       <div class="single-post-meta">
			<?php
				if ( is_single() ) {
					the_title( '<h2 class="entry-title">', '</h2>' );
				} else {
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				}
	
			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<time class="published"> <?php the_time('F j, Y') ?></time></span>
			</div><!-- .entry-meta -->
            </div>
			<?php
			endif; ?>
            </div>
		</header><!-- .entry-header -->
	<?php endif; ?>
	<div class="entry-content">
    
    <div class="container">
    <div class="single-post-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'pilot' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'pilot' ),
				'after'  => '</div>',
			) );
		?>
        </div>
        </div>
        
	</div><!-- .entry-content -->

	<?php /*?><footer class="entry-footer">
		<?php pilot_entry_footer(); ?>
	</footer><?php */?><!-- .entry-footer -->
</article><!-- #post-## -->