<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" href="<?php echo get_site_url(); ?>/wp-content/themes/pilot/images/favicon.ico" />
	<?php wp_head(); ?>
	<?php if ( is_home() || is_front_page() ) : ?>
		<meta name="google-site-verification" content="uCPlZmxycDSyKamLjUeSDytm94Opo8jnXU3CO7hZQR4" />
	<?php endif; ?>
</head>