<?php

	/*
	 * Global settings for Pilot Theme -- Dev is free to pull in via ACF
	 **/
	global $pilot;
		$pilot = new stdClass;	

		$pilot->sidebar = 0;		 			// set to 1 to use sidebar
		$pilot->comments = 0; 					// set to 1 to use comments
		$pilot->default_admin = 1; 				// set to 0 to remove extraneous meta boxes in admin
		$pilot->use_colormaker = 1;				// set to 1 to use theme-wide color-maker plugin - not yet available
		$pilot->add_acf_options_pages = 1;		// set to 1 to add options pages
		$pilot->use_default_page_titles = 1;	// set to 1 to use default page titles (adds a hide option for indiv pages); 0 to not use default page titles;

		/* Module Settings */
			$pilot->include_modules = 1; 	// set to 1 to use view modules
			$pilot->use_global_modules = 1;	// set to 1 to use global modules
			$pilot->module_classes = " module ";		// a string of classes that will be added to every module wrapper. i.e., ' wow fadeInUP';
			$pilot->additional_modules = array();		// add modules that may have failed to be included automatically
		/* End Module Settings */

		/* Automatic settings. Generally avoid. */
		$pilot->is_superadmin = 0;		// checked and set below for web dev role (set to '1' if not using web dev role)  
		$pilot->slick_enqueued = 0; 	// will be set to '1' when the first slideshow is called
	/*
	 * Conditionally set Pilot Parameters
	 **/
	// check if is superadmin
    $user = wp_get_current_user();
	if ( in_array( 'superadmin', (array) $user->roles ) ) {
		$pilot->is_superadmin = true;
	}

	/*
	 * All Requires
	 **/
	require get_template_directory() . '/includes/pilot-theme-setup.php';
	require get_template_directory() . '/includes/pilot-enqueue.php';
	require get_template_directory() . '/includes/pilot-theme-functions.php';
	require get_template_directory() . '/includes/template-tags.php';
	require get_template_directory() . '/includes/customizer.php';

	/* Conditional Requires */
	if( !$pilot->comments ){
		require get_template_directory() . '/includes/pilot-remove-comments.php';
	}
	if(	$pilot->default_admin ){
		require get_template_directory() . '/includes/pilot-admin-cleanup.php';
	}
	// ensure at least one module is included above and acf is active
	if(	$pilot->include_modules && function_exists('acf_add_local_field_group') ){ 
		require get_template_directory() . '/includes/modules/modules.php';
	}
	if( $pilot->is_superadmin ){
		require get_template_directory() . '/includes/pilot-superadmin-settings.php';
	}
	if( $pilot->use_default_page_titles ){
		require get_template_directory() . '/includes/pilot-acf-title-override.php';
	}
	if( $pilot->add_acf_options_pages ){
		require get_template_directory() . '/includes/pilot-acf-options-pages.php';
	}
	if( $pilot->use_colormaker && function_exists('acf_add_local_field_group') ){
		require get_template_directory() . '/includes/colormaker/colormaker.php';
	}

function pilot_widgets_area() {
	register_sidebar( array(
		'name'          => esc_html__( 'Front page', 'pilot' ),
		'id'            => 'front',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Col1', 'pilot' ),
		'id'            => 'footer1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Col2', 'pilot' ),
		'id'            => 'footer2',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Col3', 'pilot' ),
		'id'            => 'footer3',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

} 
add_action( 'widgets_init', 'pilot_widgets_area' );

add_action( 'send_headers', 'send_frame_options_header', 10, 0 );
	// Register extra menus

function register_menu() {
	register_nav_menu('footer', __('Footer Menu'));
}
add_action('init', 'register_menu');

// File Upload Size

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );
