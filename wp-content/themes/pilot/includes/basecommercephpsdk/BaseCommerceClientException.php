<?php
/**
 * Description of BaseCommerceClientException
 * BaseCommerceClientException is a simple re-usable entity that extends the Exception class.
 *
 * © Base Commerce
 */
class BaseCommerceClientException extends Exception {
    
}

?>
