<?php

include_once 'BankCardTransaction.php';
include_once 'BankAccountTransaction.php';
include_once 'BaseCommerceClient.php';
include_once 'BankCard.php';
include_once 'BankAccount.php';
include_once 'codeSamples.php';
include_once 'SettlementBatch.php';
include_once 'CryptInfo.php';


/*
 * © Base Commerce
 * This files contains all available bank card transactions.
 */

date_default_timezone_set('UTC');
$o_cs = new codeSamples();


//$o_cs->processRecurringTransactionDailyWithNewBankAccountAndNoEndDate();
//
////Bank Account
//$o_cs->addBankAccountToVault();
//$o_cs->addBankAccountwithMICRData();
//$o_cs->updateBankAccountToVault();
//$o_cs->deleteBankAccount();
//$o_cs->processBankAccountVaultRecord();
//$o_cs->getBankAccountTransactionByMerchantTransactionID();
//
//
////Bank Card 
//$o_cs->addBankCardToVault();
//$o_cs->updateBankCardToVault();
//$o_cs->deleteBankCard();
//$o_cs->testBankCardUpdateRequest();
//$o_cs->getBankCardTransaction();
//$o_cs->testBankCardUpdateRequest();
//$o_cs->getBankCardTransactionByMerchantTransactionID();
//
////Bank Recurring Card Transaction
//$o_cs->processRecurringTransactionDailyWithNewBankCardAndNoEndDate();
//$o_cs->processRecurringTransactionBiweeklyWithNewBankCardAndNoEndDate();
//$o_cs->processRecurringTransactionMonthlyWithNewBankCardAndNoEndDate();
//$o_cs->processRecurringTransactionAnnuallyWithBankCardTokenAndEndDate();
//$o_cs->processRecurringTransactionAnnuallyWithNewBankCardAndEndDate();
//$o_cs->processRecurringTransactionAnnuallyWithNewBankCardAndNoEndDate();
//$o_cs->cancelRecurringTransaction();
//
////Bank Recurring Account Transaction
//$o_cs->processRecurringTransactionDailyWithNewBankAccountAndNoEndDate();
//
//
////BCT
//$o_cs->processBankCardVaultRecord();
//$o_cs->refundBankCardTransaction();
//$o_cs->voidBankCardTransaction();
//$o_cs->processBankCardTransaction();
//$o_cs->getBankAccountTransactionByMerchantTransactionID();
//$o_cs->getBankCardTransactionByMerchantTransactionID();
//
////ACH
//$o_cs->processACHTransaction();
//$o_cs->reverseACHTransaction();
//$o_cs->cancelACHTransaction();
//$o_cs->getBankAccountTransaction();   
//
////PushNotification
//$o_cs->testPushNotificationID();
//$o_cs->testPingPong();
//$o_cs->testPingPushNotification();
//$o_cs->testJSONPushNotification();
//$o_cs->setPushNotificationURL();
//$o_cs->handlePushNotification();
//
////Process Request
//$o_cs->processRequest();
//
////Test Merchant
//$o_cs->testBothACHAndBankCardforAutoBoard();
//$o_cs->testBothACHAndBankCardforNonAutoBoard();  
//
////Bank Card Batch Execution
//$o_cs->testCreateBankCardTransactionSettlementBatch();
//
////Split Funding for Bank Card Transaction
//$o_cs->testBCTSplitFunding();
//
////Split Funding for Bank Account Transaction
//$o_cs->testBATSplitFunding();

//Cipher crypt
$o_cs->testGenerateCipherCrypt();
//$o_cs->testEncryptCipherCrypt();
//$o_cs->testDecryptCipherCrypt();

?>
