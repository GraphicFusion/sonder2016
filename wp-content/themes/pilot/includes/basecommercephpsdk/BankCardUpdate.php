<?php

/**
 * A Bank Card Update class
 *
 */
class BankCardUpdate {
    
    static $XS_BCU_STATUS_NEW = "NEW";
    static $XS_BCU_STATUS_PENDING = "PENDING";
    static $XS_BCU_STATUS_UPDATED = "UPDATED";
    
    private $in_update_id=0;
    private $is_original_number;
    private $is_original_token;
    private $is_original_expiration_month;
    private $is_original_expiration_year;
    private $is_updated_number;
    private $is_updated_expiration_month;
    private $is_updated_expiration_year;
    private $is_status;
    private $io_creation_date;
    private $io_updated_date;
    private $io_latest_request_date;
    
    
    
    /**
     *  returns the update id
     *  @return the in_update_id
     */
    public function getUpdateID() {
        return $in_update_id;
    }

    /**
     *  sets the update id
     *  @param in_update_id the in_update_id to set
     */
    public function setUpdateID($vn_update_id) {
        $this->in_update_id = $vn_update_id;
    }
    
    
    /**
     * returns the original number
     * @return type string of the original number
     */
    public function getOriginalNumber() {
        return $this->is_original_number;
    }
    
    /**
     * sets the original number
     * @param type $vs_original_number the original number
     */
    public function setOriginalNumber( $vs_original_number ) {
        $this->is_original_number = $vs_original_number;
    }
    
    /**
     * sets the original token
     * @param type $vs_original_token the original token
     */
    public function setOriginalToken( $vs_original_token ) {
        $this->is_original_token = $vs_original_token;
    }
    
    /**
     * returns the original token
     * @return string of the original token
     */
    public function getOriginalToken() {
        return $this->is_original_token;
    }
    
    /**
     * returns the original expiration month
     * @return type string of the original expiration month
     */
    public function getOriginalExpirationMonth() {
        return $this->is_original_expiration_month;
    }
    
    /**
     * sets the original expiration month
     * @param type $vs_original_expiration_month the original expiration month
     */
    public function setOriginalExpirationMonth( $vs_original_expiration_month ) {
        $this->is_original_expiration_month = $vs_original_expiration_month;
    }
    
    /**
     * returns the original expiration year
     * @return type string of the original expiration year
     */
    public function getOriginalExpirationYear() {
        return $this->is_original_expiration_year;
    }
    
    /**
     * sets the original expiration year
     * @param type $vs_original_expiration_year the original expiration year
     */
    public function setOriginalExpirationYear( $vs_original_expiration_year ) {
        $this->is_original_expiration_year = $vs_original_expiration_year;
    }
    
    /**
     * returns the updated number
     * @return type string of the updated number
     */
    public function getUpdatedNumber() {
        return $this->is_updated_number;
    }
    
    /**
     * sets the updated number
     * @param type $vs_updated_number the updated number
     */
    public function setUpdatedNumber( $vs_updated_number ) {
        $this->is_updated_number = $vs_updated_number;
    }
    
    /**
     * returns the updated expiration month
     * @return type string of the updated expiration month
     */
    public function getUpdatedExpirationMonth() {
        return $this->is_updated_expiration_month;
    }
    
    /**
     * sets the updated expiration month
     * @param type $vs_updated_expiration_month the updated expiration month
     */
    public function setUpdatedExpirationMonth( $vs_updated_expiration_month ) {
        $this->is_updated_expiration_month = $vs_updated_expiration_month;
    }
    
    /**
     * returns the updated expiration year
     * @return type string of the updated expiration year
     */
    public function getUpdatedExpirationYear() {
        return $this->is_updated_expiration_year;
    }
    
    /**
     * sets the updated expiration year
     * @param type $vs_updated_expiration_year the updated expiration year
     */
    public function setUpdatedExpirationYear( $vs_updated_expiration_year ) {
        $this->is_updated_expiration_year = $vs_updated_expiration_year;
    }
    
    /**
     * returns the status
     * @return type string of the status
     */
    public function getStatus() {
        return $this->is_status;
    }
    
    /**
     * sets the status
     * @param type $vs_status the status
     */
    public function setStatus( $vs_status ) {
        $this->is_status = $vs_status;
    }
    
    /**
     * returns the creation date
     * @return type string of the creation date
     */
    public function getCreationDate() {
        return $this->io_creation_date;
    }
    
    /**
     * sets the creation date
     * @param type $io_creation_date the creation date
     */
    public function setCreationDate( $io_creation_date ) {
        $this->io_creation_date = $io_creation_date;
    }
    
    /**
     * returns the updated date
     * @return type string of the updated date
     */
    public function getUpdatedDate() {
        return $this->io_updated_date;
    }
    
    /**
     * sets the updated date
     * @param type $vo_updated_date the updated date
     */
    public function setUpdatedDate( $vo_updated_date ) {
        $this->io_updated_date = $vo_updated_date;
    }
    
    /**
     * returns the latest request date
     * @return type string of the latest request date
     */
    public function getLatestRequestDate() {
        return $this->io_latest_request_date;
    }
    
    /**
     * sets the latest request date
     * @param type $vo_latest_request_date the latest request date
     */
    public function setLatestRequestDate( $vo_latest_request_date ) {
        $this->io_latest_request_date =$vo_latest_request_date;
    }
    
    /**
     * Checks if the given status matches the status on the object
     * 
     * @param $vs_status the status being checked for
     * @return true if the status matches, false otherwise
     */
    public function isStatus( $vs_status ) {
        return $this->is_status == $vs_status;
    }
    
    /**
     * builds a BankCardUpdate from the JSON object passed in
     * @param type $vo_data JSON object of a BankCardUpdate
     * @return \BankCardUpdate a BankCArdUpdate object
     */
    static function buildFromJSON( $vo_data ) {
        
        $o_instance = new BankCardUpdate();
        
        if( array_key_exists( 'bank_card_update_id', $vo_data ) ) { $o_instance->setUpdateID( $vo_data['bank_card_update_id'] ); }
        
        if( array_key_exists( 'bank_card_update_original_number', $vo_data ) ) { $o_instance->setOriginalNumber( $vo_data['bank_card_update_original_number'] ); }
        if( array_key_exists( 'bank_card_update_original_token', $vo_data ) ) { $o_instance->setOriginalToken( $vo_data['bank_card_update_original_token'] ); }
        if( array_key_exists( 'bank_card_update_original_expiration_month', $vo_data ) ) { $o_instance->setOriginalExpirationMonth( $vo_data['bank_card_update_original_expiration_month'] ); }
        if( array_key_exists( 'bank_card_update_original_expiration_year', $vo_data ) ) { $o_instance->setOriginalExpirationYear( $vo_data['bank_card_update_original_expiration_year'] ); }
        if( array_key_exists( 'bank_card_update_updated_number', $vo_data ) ) { $o_instance->setUpdatedNumber( $vo_data['bank_card_update_updated_number'] ); }
        if( array_key_exists( 'bank_card_update_updated_expiration_month', $vo_data ) ) { $o_instance->setUpdatedExpirationMonth( $vo_data['bank_card_update_updated_expiration_month'] ); }
        if( array_key_exists( 'bank_card_update_updated_expiration_year', $vo_data ) ) { $o_instance->setUpdatedExpirationYear( $vo_data['bank_card_update_updated_expiration_year'] ); }
        if( array_key_exists( 'bank_card_update_creation_date', $vo_data ) ) { $o_instance->setCreationDate( $vo_data['bank_card_update_creation_date'] ); }
        if( array_key_exists( 'bank_card_update_latest_request_date', $vo_data ) ) { $o_instance->setLatestRequestDate( $vo_data['bank_card_update_latest_request_date'] ); }
        if( array_key_exists( 'bank_card_update_updated_date', $vo_data ) ) { $o_instance->setUpdatedDate( $vo_data['bank_card_update_updated_date'] ); }
        
        if(array_key_exists( 'bank_card_update_status', $vo_data) ) {
            
            if( gettype( $vo_data['bank_card_update_status'] ) === "array" ){
                
                $o_status = (array)$vo_data['bank_card_update_status'];
                
                if( array_key_exists( 'bank_card_update_status_name', $o_status ) ){
                    
                    $o_instance->setStatus( $o_status['bank_card_update_status_name'] );
                }
                else{
                    $o_instance->setStatus( $vo_data[ 'bank_card_update_status'] );
                }
            }
        }
        
        return $o_instance;
        
    }
    
    /**
     * returns a JSON representation of the BankCardUpdate
     * @return type JSON object
     */
    public function getJSON() {
        
        $o_array = array();
        
              
        if( $this->in_update_id != 0 ) {
            $o_array['bank_card_update_id'] = $this->in_update_id;
        }
        
        if( !is_null( $this->is_original_number ) ) {
            $o_array['bank_card_update_original_number'] = $this->is_original_number;
        }
        
        if( !is_null( $this->is_original_token ) ) {
            $o_array['bank_card_update_original_token'] = $this->is_original_token;
        }
        
        if( !is_null( $this->is_original_expiration_month ) ) {
            $o_array['bank_card_update_original_expiration_month'] = $this->is_original_expiration_month;
        }
        
        if( !is_null( $this->is_original_expiration_year ) ) {
            $o_array['bank_card_update_original_expiration_year'] = $this->is_original_expiration_year;
        }
        
        if( !is_null( $this->is_updated_number ) ) {
            $o_array['bank_card_update_updated_number'] = $this->is_updated_number;
        }
        
        if( !is_null( $this->is_updated_expiration_month ) ) {
            $o_array['bank_card_update_updated_expiration_month'] = $this->is_updated_expiration_month;
        }
        
        if( !is_null( $this->is_updated_expiration_year ) ) {
            $o_array['bank_card_update_updated_expiration_year'] = $this->is_updated_expiration_year;
        }
        
        if( !is_null( $this->io_creation_date ) ) {
            $date = new DateTime( $this->io_creation_date );
            $timestamp = $date->format('U');
            $o_array['bank_card_update_creation_date'] = $timestamp;
        }
        
        if( !is_null( $this->io_latest_request_date ) ) {
            $date = new DateTime( $this->io_creation_date );
            $timestamp = $date->format('U');
            $o_array['bank_card_update_latest_request_date'] = $timestamp;
        }
        
        if( !is_null( $this->io_updated_date ) ) {
            $date = new DateTime( $this->io_creation_date );
            $timestamp = $date->format('U');
            $o_array['bank_card_update_updated_date'] = $timestamp;
        }
        
        if( !is_null( $this->is_status ) ) {
            $o_array['bank_card_update_status'] = $this->is_status;
        }
                
        return $o_array;
        
    }
    
}
