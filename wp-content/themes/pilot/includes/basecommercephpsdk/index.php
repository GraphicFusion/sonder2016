<?php


include_once 'BankCardTransaction.php';
include_once 'BankAccountTransaction.php';
include_once 'BaseCommerceClient.php';
include_once 'BankCard.php';
include_once 'BankAccount.php';
include_once 'codeSamples.php';


/*
 * © Base Commerce
 * 
 * This files contains all available bank card transactions.
 * 
 * 
 * 
 * 
 */

   
    date_default_timezone_set('UTC');
    var_dump("in index.php");
     $o_cs = new codeSamples();
	 $o_cs->testBothACHAndBankCardforNonAutoBoard();

//     $o_cs->updateBankAccountToVault();
     //$o_cs->addBankAccountToVault();
     // $o_cs->testPushNotificationID();

   // $o_cs->testPingPong();



//    $o_cs->testBankCardUpdate();
//    $o_cs->testBankCardUpdateRequest();
    
//    $o_cs->deleteBankCard();
//    $o_cs->deleteBankAccount();
    
    // New BankCard with no end date
//    $o_cs->processRecurringTransaction('account','XS_FREQUENCY_ANNUALLY','6/16/2015','6/15/2016',true);
//    $o_cs->processRecurringTransactionDailyWithNewBankCardAndNoEndDate();
//    $o_cs->processRecurringTransactionMonthlyWithNewBankCardAndNoEndDate();
//    $o_cs->processRecurringTransactionQuarterlyWithNewBankCardAndNoEndDate();
//    $o_cs->processRecurringTransactionBiweeklyWithNewBankCardAndNoEndDate();
//    $o_cs->processRecurringTransactionWeeklyWithNewBankCardAndNoEndDate();
    
    // New BankCard with end date
//    $o_cs->processRecurringTransactionAnnuallyWithNewBankCardAndEndDate();
    
    // Existing BankCard with end date
//    $o_cs->processRecurringTransactionAnnuallyWithBankCardTokenAndEndDate();
    
    // Cancel recurring transaction
//    $o_cs->cancelRecurringTransaction();
    
//    $o_cs->testPushNotificationID();

//    $o_cs->testPingPong();
//    $o_cs->testMerchApp();

    //$o_cs->testPingPong();

//    $o_cs->testPingPong();


//    $o_cs->setPushNotificationURL();


//    $o_cs->processBankCardTransaction();
//    $o_cs->voidBankCardTransaction();
//    $o_cs->refundBankCardTransaction();
    //$o_cs->processACHTransaction();

      //$o_cs->addBankCardToVault();

      //$o_cs->addBankAccountToVault();
      //$o_cs->updateBankAccountToVault();
      

//    $o_cs->addBankAccountToVault();
//    $o_cs->processBankCardVaultRecord();
//    $o_cs->processBankAccountVaultRecord();
//    $o_cs->getBankCardTransaction();
//    $o_cs->getBankAccountTransaction();
//    $o_cs->reverseACHTransaction();
//    $o_cs->cancelACHTransaction();
//    $o_cs->testPingPushNotification();



    //$o_cs->testJSONPushNotification();

    //$o_cs->processRequest();


//    $o_cs->testJSONPushNotification();

//    $o_cs->processRequest();



    

 
//    $o_billing_address = new Address();
//    $o_billing_address->setCountry("US");
//    $o_billing_address->setCity("Scottsdale");
//    $o_billing_address->setLine1("111 East 2nd Street");
//    $o_billing_address->setName(Address::$XS_BILLING);
//    $o_billing_address->setState("AZ");
//    $o_billing_address->setZipcode("85254");
    
    
    // Sale
//    $o_bct = new BankCardTransaction();
//    $o_bct->setType(BankCardTransaction::$XS_SALE);
//
//    $o_bct->setCardNumber("41111111111111112222");
//    $o_bct->setCardExpirationMonth("09");
//    $o_bct->setCardExpirationYear("2015");
//    $o_bct->setCardCVV2( "800" );
//    $o_bct->setAmount(0.01); 
//    $o_bct->setCheckSecureCode(false);
//    $o_bct->setBillingAddress($o_billing_address);
//    $o_bct->setVerifyCompleteURL("www.google.com");
//    $o_bct->setRecurringIndicator(false);
//    $o_bct->setSettlementBatchID( 212 );
//    $o_bct->setSettlementDate( date('d-m-Y H:i:s') );
//    $o_bct->setCreationDate( date('d-m-Y H:i:s') );
    
//    $o_bct->setCustomField1( "The" );
//    $o_bct->setCustomField2( "sky" );
//    $o_bct->setCustomField3( "was" );
//    $o_bct->setCustomField4( "falling" );
//    $o_bct->setCustomField5( "," );
//    $o_bct->setCustomField6( "but" );
//    $o_bct->setCustomField7( "now" );
//    $o_bct->setCustomField8( "it" );
//    $o_bct->setCustomField9( "is" );
//    $o_bct->setCustomField10( "ok" );
    
//    $o_bcpc = new BaseCommerceClient( 'username', 'password', 'key');
//    $o_bcpc->setSandbox( true );
    
//    $o_bct2 = $o_bcpc->processBankCardTransaction( $o_bct );
//    var_dump( $o_bct );
/*    
    // Auth
    $o_bct = new BankCardTransaction();
    $o_bct->setType(BankCardTransaction::$XS_AUTH);

    $o_bct->setCardNumber("4111111111111111");
    $o_bct->setCardExpirationMonth("09");
    $o_bct->setCardExpirationYear("2015");
    $o_bct->setCardCVV2( "800" );
    $o_bct->setAmount(0.01); 
    $o_bct->setBillingAddress($o_billing_address);
    $o_bct->setVerifyCompleteURL("www.google.com");

    $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );
    
    // Credit
    $o_bct = new BankCardTransaction();
    $o_bct->setType(BankCardTransaction::$XS_CREDIT);

    $o_bct->setCardNumber("4111111111111111");
    $o_bct->setCardExpirationMonth("09");
    $o_bct->setCardExpirationYear("2015");
    $o_bct->setAmount(0.01); 

    $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );
    
    // Void
    $o_bct = new BankCardTransaction();
    $o_bct->setType(BankCardTransaction::$XS_VOID);
    $o_bct->setTransactionId(43);      // Should be a transaction id that was passed back from a previous transaction
    $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );
    
    // Capture
    $o_bct = new BankCardTransaction();
    $o_bct->setType(BankCardTransaction::$XS_CAPTURE);
    $o_bct->setTransactionId(43);      // Should be a transaction id that was passed back from a previous transaction
    $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );
    
    // Refund
    $o_bct = new BankCardTransaction();
    $o_bct->setType(BankCardTransaction::$XS_REFUND);
    $o_bct->setTransactionId(43);      // Should be a transaction id that was passed back from a previous transaction
    $o_bct->setAmount( 10.00 );
    $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );
    */
    
    
    // Add Bank Card
//    $o_address = new Address();
//    $o_address->setName(Address::$XS_BILLING);
//    $o_address->setLine1("123 Some Address");
//    $o_address->setCity("Tempe");
//    $o_address->setState("AZ");
//    $o_address->setZipcode("12345");
//        
//    $o_bc = new BankCard();
//    $o_bc->setBillingAddress($o_address);
//    $o_bc->setExpirationMonth("02");
//    $o_bc->setExpirationYear("2015");
//    $o_bc->setName("John Smith");
//    $o_bc->setNumber("411111111111111133333");
//    var_dump("\n\n\nvo_bc.getJSON\n");
//    var_dump($o_bc->getJSON());
//    var_dump($o_bc->getBillingAddress());
//    $o_bc = $o_bcpc->addBankCard( $o_bc );
//    var_dump($o_bc);
    
//    $o_bat = new BankAccountTransaction();
//    $o_bat->setRoutingNumber( "021000021" );
//    $o_bat->setAccountNumber( "12312312312" );
//    $o_bat->setAccountType( BankAccountTransaction::$XS_CHECKING );
//    $o_bat->setMethod( BankAccountTransaction::$XS_CCD );
//    $o_bat->setType( BankAccountTransaction::$XS_CREDIT );
//    $o_bat->setEffectiveDate( date('d-m-Y H:i:s') );
//    $o_bat->setRecurringIndicator(false);
//    var_dump($o_bat);
//    $o_bat->setCustomField1( "The" );
//    $o_bat->setCustomField2( "sky" );
//    $o_bat->setCustomField3( "was" );
//    $o_bat->setCustomField4( "falling" );
//    $o_bat->setCustomField5( "," );
//    $o_bat->setCustomField6( "but" );
//    $o_bat->setCustomField7( "now" );
//    $o_bat->setCustomField8( "it" );
//    $o_bat->setCustomField9( "is" );
//    $o_bat->setCustomField10( "ok" );

//    $o_bat = $o_bcpc->processBankAccountTransaction( $o_bat );
//    var_dump( $o_bat );
    
//    $o_bank_card = new BankCard();
//    
//    $o_bank_card->setBillingAddress( $o_billing_address );
//    $o_bank_card->setExpirationMonth(09);
//    $o_bank_card->setExpirationYear(16);
//    $o_bank_card->setName("John Doe");
//    $o_bank_card->setNumber("4111111111111111");
//    $o_bank_card->setStatus("FAILED");
//    $o_bank_card->setAlias("bankcard alias");
    
//    $o_bank_card = $o_bcpc->addBankCard( $o_bank_card );
//    var_dump( $o_bank_card );
//    
//    
//    $o_bank_account = new BankAccount();
//    $o_bank_account->setName("Bobby K");
//    $o_bank_account->setAlias("Secret savings account");
//    $o_bank_account->setAccountNumber("1111111111");
//    $o_bank_account->setRoutingNumber("111000025");
//    $o_bank_account->setStatus("ACTIVE");
//    
//    var_dump($o_bank_account);
    
?>

