<?php

include_once 'Account.php';
include_once 'Address.php';
include_once 'ACHDetails.php';
include_once 'BankCardDetails.php';
include_once 'Internet.php';
include_once 'Location.php';
include_once 'MerchantApplication.php';
include_once 'BaseCommerceClient.php';
include_once 'BaseCommerceClientException.php';
include_once 'MOTO.php';
include_once 'POS.php';
include_once 'PrincipalContact.php';
include_once 'Terminal.php';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

	date_default_timezone_set('America/Phoenix');
	
	$s_gateway_username = "";
	$s_gateway_password = "";
	$s_gateway_key = "";


        
//	testFiveCards();
	
	testBothACHAndBankCard();
/*	
	testACHOnly();
	
	testBankCardOnly();
	
	testTwoSameTerminals();
	
	testTwoDifferentTerminals();
	
	testInternetObject();
	
	testMOTOObject();
	
	testPOSObject(); */
	
	function testBothACHAndBankCard() {
		global $s_gateway_username, $s_gateway_password, $s_gateway_key;
	
		$o_client = new BaseCommerceClient($s_gateway_username, $s_gateway_password, $s_gateway_key);
                   $o_client->setSandbox( true );
                   
	    $o_merch_app = new MerchantApplication();
    
	    $o_merch_app->setApiVersion("1.2");
	    
	    $o_account = new Account();
	    $o_account->setAccountName( "TEST Account" );
    	$o_account->setTestAccount( True );
	    $o_account->setAcceptACH( True );
	    $o_account->setAcceptBC( True );
    
		$o_legal_address = new Address( Address::$XS_ADDRESS_NAME_LEGAL );
		$o_legal_address->setLine1( "123 Oak Street" );
		$o_legal_address->setCity( "Toontown" );
		$o_legal_address->setState( "AZ" );
		$o_legal_address->setZipcode( "85284" );
		$o_legal_address->setCountry( "USA" );
	
		$o_account->setLegalAddress( $o_legal_address );
	
		$o_dba_address = new Address( Address::$XS_ADDRESS_NAME_DBA );
		$o_dba_address->setLine1( "456 Maple Street" );
		$o_dba_address->setCity( "Toontown" );
		$o_dba_address->setState( "AZ" );
		$o_dba_address->setZipcode( "85284" );
		$o_dba_address->setCountry( "USA" );
	
		$o_account->setAccountPhoneNumber( "123-456-7891" );
		$o_account->setReferralPartnerID( "a0Fi0000006Om7a" );
		$o_account->setCustomerServicePhoneNumber( "981-124-6431" );
		$o_account->setDBA("ACME Inc");
		$o_account->setEntityType( Account::$XS_ENTITY_TYPE_CORP );
		$o_account->setAssociationNumber( "1111" );
		$o_account->setCongaTemplateId( "1111" );
		$o_account->setEIN( "123456789" );
		$o_account->setIpAddressOfAppSubmission( "192.168.0.1" );
		$o_account->setWebsite( "www.google.com" );
		$o_account->setSettlementAccountBankName( "test bank name" );
		$o_account->setSettlementAccountBankPhone( "123-123-1234" );
		$o_account->setSettlementAccountName( "test account name" );
		$o_account->setSettlementAccountNumber( "123456789" );
		$o_account->setSettlementAccountRoutingNumber( "000000000" );
		$o_account->setSettlementSameAsFeeAccount( true );
	
		$o_merch_app->setAccount( $o_account );
	
		$o_location = new Location();
		$o_location->setAlternativeFax("111-111-1111");
		$o_location->setAnnualRevenue( 10000000 );
		$o_location->setContactEmail( "contact@gmail.com" );
		$o_location->setContactMobile( "222-222-2222" );
		$o_location->setContactName( "Roger Rabbit" );
		$o_location->setContactSameAsOwner( false );
		$o_location->setDescription( Location::$XS_DESCRIPTION_CONSULTING );
		$o_location->setEntityStartDate( date('Y-m-d') );
		$o_location->setEntityState( "AZ" );
		$o_location->setFax( "333-333-3333" );
		$o_location->setIndustry( Location::$XS_INDUSTRY_CONSULTING );
		$o_location->setLengthOfCurrentOwnership( "1" );
		$o_location->setOrganizationMission( "We take your business where it needs to go" );
		$o_location->setOwnership( Location::$XS_OWNERSHIP_PRIVATE );
		$o_location->setQuantityOfLocation( 1 );
		$o_location->setSalesAgentName( "John Doe" );
		$o_location->setTaxExempt( false );
		$o_location->setTotalCustomers( 1500 );
		$o_location->setYearIncorporated( "2013" );
		$o_location->setYearsInBusiness( 1 );
		$o_location->setDBAAddress( $o_dba_address );

		$o_contact = new PrincipalContact();
		$o_contact->setLastName( "Rabbit" );
		$o_contact->setFirstName( "Roger" );

		$o_mailing = new Address( Address::$XS_ADDRESS_NAME_MAILING );
		$o_mailing->setLine1( "789 Maple Street" );
		$o_mailing->setCity( "Toontown" );
		$o_mailing->setState( "AZ" );
		$o_mailing->setZipcode( "85284" );
		$o_mailing->setCountry( "USA" );
	
		$o_contact->setMailingAddress( $o_mailing );
		$o_contact->setPhoneNumber( "111-111-1111" );
		$o_contact->setMobilePhoneNumber( "222-222-2222" );
		$o_contact->setEmail( "test-princ-contact@test.com" );
		$o_contact->setTitle( "CEO" );
		$o_contact->setBirthdate( date('m/d/y') );
		$o_contact->setAuthorizedUser( True );
		$o_contact->setAccountSigner( True );
		$o_contact->setSSN( "111-11-1111" );
		$o_contact->setIsPrimary( True );
		$o_contact->setFax( "111-111-1112" );
		$o_contact->setOwnershipPercentage( 100 );
		$o_contact->setContactType( PrincipalContact::$XS_CONTACT_TYPE_OWNER );
		$o_contact->setDriverLicenseId( "123 456 789" );
		$o_contact->setLicenseState( "AZ" );
		$o_location->addPrincipalContact( $o_contact );

		$o_ach_details = new ACHDetails();
		$o_ach_details->setAuthMethodConversionPercentage( 100 );
		$o_ach_details->setAverageTicketAmount( 1 );
		$o_ach_details->setChargebackFee( 1 );
		$o_ach_details->setCompanyNameDescriptor( "comp name descriptor" );
		$o_ach_details->setDescriptor( "descript" );
		$o_ach_details->setMaxDailyAmount( 1 );
		$o_ach_details->setMaxSingleTransactionAmount( 1 );
		$o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_BUSINESSES_ORGANIZATIONS );
		$o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_CONSUMER_INDIVIDUALS );
		$o_ach_details->setReturnFee( 1 );
		$o_ach_details->addSubmissionMethod( ACHDetails::$XS_SUBMISSION_METHOD_MANUALLY_VIA_VIRTUAL_TERMINAL );
		$o_ach_details->setTransactionFee( 1 );
		$o_ach_details->setUnauthReturn( 1 );
		$o_ach_details->setAverageMonthlyAmount( 1 );

		$o_location->setAchDetails( $o_ach_details );

		$o_bc_details = new BankCardDetails();
		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT );
		$o_bc_details->setAuthorizationFee( 1 );
		$o_bc_details->setAverageMonthlyVolume( 1 );
		$o_bc_details->setAverageTicket( 1 );
		$o_bc_details->setCardPresentPercentage( 80 );
		$o_bc_details->setFlatRate( 1 );
		$o_bc_details->setInternetPercentage( 10 );
		$o_bc_details->setMailOrderPercentage( 1 );
		$o_bc_details->setTelephoneOrderPercentage( 9 );
		$o_bc_details->setMaxMonthlyVolume( 1 );
		$o_bc_details->setMaxTicket( 1 );
		$o_bc_details->setTransactionFee( 1 );
	
		$o_location->setBankCardDetails( $o_bc_details );
	
		$o_merch_app->addLocation( $o_location );
		$o_merch_app->addLocation( $o_location );
		try {
			$o_merch_applications = $o_client->submitApplication( $o_merch_app );
//                        print_r($o_merch_applications);
                        for ( $n_index = 0, $n_size = count( $o_merch_applications ); $n_index < $n_size; $n_index++ ) {                            
                            $o_merch_app = $o_merch_applications[ $n_index ];                            
                            echo "\n\nSUCCESS";
                            echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
                            echo "\n\n";
                            echo "\n\n o_merch_app id = " . $o_merch_app->getId();
                            echo "\n\n";
                            echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
                            echo "\n\n";
                            echo "\n\n o_merch_app salesforce ids = " . print_r($o_merch_app->getSalesforceID());                           
                        }
		} catch ( BaseCommerceClientException $e ) {
			echo "ERROR";
			var_dump($e->getMessage());
			//var_dump($e->getTrace());
		}
	}
	
    function testACHOnly() {
		global $s_gateway_username, $s_gateway_password, $s_gateway_key;
		
    	$o_client = new BaseCommerceClient($s_gateway_username, $s_gateway_password, $s_gateway_key);
    
	    $o_merch_app = new MerchantApplication();
    
	    $o_merch_app->setApiVersion("1.2");
	    $o_merch_app->setCode($s_gateway_username);
    
	    $o_account = new Account();
	    $o_account->setAccountName( "TEST Account" );
    	$o_account->setTestAccount( True );
	    $o_account->setAcceptACH( True );
	    $o_account->setAcceptBC( False );
    
		$o_legal_address = new Address( Address::$XS_ADDRESS_NAME_LEGAL );
		$o_legal_address->setLine1( "123 Oak Street" );
		$o_legal_address->setCity( "Toontown" );
		$o_legal_address->setState( "AZ" );
		$o_legal_address->setZipcode( "85284" );
		$o_legal_address->setCountry( "USA" );
	
		$o_account->setLegalAddress( $o_legal_address );
	
		$o_dba_address = new Address( Address::$XS_ADDRESS_NAME_DBA );
		$o_dba_address->setLine1( "456 Maple Street" );
		$o_dba_address->setCity( "Toontown" );
		$o_dba_address->setState( "AZ" );
		$o_dba_address->setZipcode( "85284" );
		$o_dba_address->setCountry( "USA" );
	
		$o_account->setAccountPhoneNumber( "123-456-7891" );
		$o_account->setReferralPartnerID( "a0Fi0000006Om7a" );
		$o_account->setCustomerServicePhoneNumber( "981-124-6431" );
		$o_account->setDBA("ACME Inc");
		$o_account->setEntityType( Account::$XS_ENTITY_TYPE_CORP );
		$o_account->setAssociationNumber( "1111" );
		$o_account->setCongaTemplateId( "1111" );
		$o_account->setEIN( "123456789" );
		$o_account->setIpAddressOfAppSubmission( "192.168.0.1" );
		$o_account->setWebsite( "www.google.com" );
		$o_account->setSettlementAccountBankName( "test bank name" );
		$o_account->setSettlementAccountBankPhone( "123-123-1234" );
		$o_account->setSettlementAccountName( "test account name" );
		$o_account->setSettlementAccountNumber( "123456789" );
		$o_account->setSettlementAccountRoutingNumber( "000000000" );
		$o_account->setSettlementSameAsFeeAccount( true );
	
		$o_merch_app->setAccount( $o_account );
	
		$o_location = new Location();
		$o_location->setAlternativeFax("111-111-1111");
		$o_location->setAnnualRevenue( 10000000 );
		$o_location->setContactEmail( "contact@gmail.com" );
		$o_location->setContactMobile( "222-222-2222" );
		$o_location->setContactName( "Roger Rabbit" );
		$o_location->setContactSameAsOwner( false );
		$o_location->setDescription( Location::$XS_DESCRIPTION_CONSULTING );
		$o_location->setEntityStartDate( date('Y-m-d') );
		$o_location->setEntityState( "AZ" );
		$o_location->setFax( "333-333-3333" );
		$o_location->setIndustry( Location::$XS_INDUSTRY_CONSULTING );
		$o_location->setLengthOfCurrentOwnership( "1" );
		$o_location->setOrganizationMission( "We take your business where it needs to go" );
		$o_location->setOwnership( Location::$XS_OWNERSHIP_PRIVATE );
		$o_location->setQuantityOfLocation( 1 );
		$o_location->setSalesAgentName( "John Doe" );
		$o_location->setTaxExempt( false );
		$o_location->setTotalCustomers( 1500 );
		$o_location->setYearIncorporated( "2013" );
		$o_location->setYearsInBusiness( 1 );
		$o_location->setDBAAddress( $o_dba_address );

		$o_contact = new PrincipalContact();
		$o_contact->setLastName( "Rabbit" );
		$o_contact->setFirstName( "Roger" );

		$o_mailing = new Address( Address::$XS_ADDRESS_NAME_MAILING );
		$o_mailing->setLine1( "789 Maple Street" );
		$o_mailing->setCity( "Toontown" );
		$o_mailing->setState( "AZ" );
		$o_mailing->setZipcode( "85284" );
		$o_mailing->setCountry( "USA" );
	
		$o_contact->setMailingAddress( $o_mailing );
		$o_contact->setPhoneNumber( "111-111-1111" );
		$o_contact->setMobilePhoneNumber( "222-222-2222" );
		$o_contact->setEmail( "test-princ-contact@test.com" );
		$o_contact->setTitle( "CEO" );
		$o_contact->setBirthdate( date('Y-m-d') );
		$o_contact->setAuthorizedUser( True );
		$o_contact->setAccountSigner( True );
		$o_contact->setSSN( "111-11-1111" );
		$o_contact->setIsPrimary( True );
		$o_contact->setFax( "111-111-1112" );
		$o_contact->setOwnershipPercentage( 100 );
		$o_contact->setContactType( PrincipalContact::$XS_CONTACT_TYPE_OWNER );
		$o_contact->setDriverLicenseId( "123 456 789" );
		$o_contact->setLicenseState( "AZ" );
		$o_location->addPrincipalContact( $o_contact );

		$o_ach_details = new ACHDetails();
		$o_ach_details->setAuthMethodConversionPercentage( 100 );
		$o_ach_details->setAverageTicketAmount( 1 );
		$o_ach_details->setChargebackFee( 1 );
		$o_ach_details->setCompanyNameDescriptor( "comp name descriptor" );
		$o_ach_details->setDescriptor( "descript" );
		$o_ach_details->setMaxDailyAmount( 1 );
		$o_ach_details->setMaxSingleTransactionAmount( 1 );
		$o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_BUSINESSES_ORGANIZATIONS );
		$o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_CONSUMER_INDIVIDUALS );
		$o_ach_details->setReturnFee( 1 );
		$o_ach_details->addSubmissionMethod( ACHDetails::$XS_SUBMISSION_METHOD_MANUALLY_VIA_VIRTUAL_TERMINAL );
		$o_ach_details->setTransactionFee( 1 );
		$o_ach_details->setUnauthReturn( 1 );
		$o_ach_details->setAverageMonthlyAmount( 1 );

		$o_location->setAchDetails( $o_ach_details );
		
		$o_merch_app->addLocation( $o_location );
		try {
			$o_merch_app = $o_client->submitApplication( $o_merch_app );
			echo "\n\nSUCCESS";
			echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
			echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
		} catch ( BaseCommerceClientException $e ) {
			var_dump($e->getMessage());
			var_dump($e->getTrace());
		}
    }
    
    function testBankCardOnly() {
		global $s_gateway_username, $s_gateway_password, $s_gateway_key;
    	
    	$o_client = new BaseCommerceClient($s_gateway_username, $s_gateway_password, $s_gateway_key);
    
	    $o_merch_app = new MerchantApplication();
    
	    $o_merch_app->setApiVersion("1.2");
	    $o_merch_app->setCode($s_gateway_username);
    
	    $o_account = new Account();
	    $o_account->setAccountName( "TEST Account" );
    	$o_account->setTestAccount( True );
	    $o_account->setAcceptACH( False );
	    $o_account->setAcceptBC( True );
    
		$o_legal_address = new Address( Address::$XS_ADDRESS_NAME_LEGAL );
		$o_legal_address->setLine1( "123 Oak Street" );
		$o_legal_address->setCity( "Toontown" );
		$o_legal_address->setState( "AZ" );
		$o_legal_address->setZipcode( "85284" );
		$o_legal_address->setCountry( "USA" );
	
		$o_account->setLegalAddress( $o_legal_address );
	
		$o_dba_address = new Address( Address::$XS_ADDRESS_NAME_DBA );
		$o_dba_address->setLine1( "456 Maple Street" );
		$o_dba_address->setCity( "Toontown" );
		$o_dba_address->setState( "AZ" );
		$o_dba_address->setZipcode( "85284" );
		$o_dba_address->setCountry( "USA" );
	
		$o_account->setAccountPhoneNumber( "123-456-7891" );
		$o_account->setReferralPartnerID( "a0Fi0000006Om7a" );
		$o_account->setCustomerServicePhoneNumber( "981-124-6431" );
		$o_account->setDBA("ACME Inc");
		$o_account->setEntityType( Account::$XS_ENTITY_TYPE_CORP );
		$o_account->setAssociationNumber( "1111" );
		$o_account->setCongaTemplateId( "1111" );
		$o_account->setEIN( "123456789" );
		$o_account->setIpAddressOfAppSubmission( "192.168.0.1" );
		$o_account->setWebsite( "www.google.com" );
		$o_account->setSettlementAccountBankName( "test bank name" );
		$o_account->setSettlementAccountBankPhone( "123-123-1234" );
		$o_account->setSettlementAccountName( "test account name" );
		$o_account->setSettlementAccountNumber( "123456789" );
		$o_account->setSettlementAccountRoutingNumber( "000000000" );
		$o_account->setSettlementSameAsFeeAccount( true );
	
		$o_merch_app->setAccount( $o_account );
	
		$o_location = new Location();
		$o_location->setAlternativeFax("111-111-1111");
		$o_location->setAnnualRevenue( 10000000 );
		$o_location->setContactEmail( "contact@gmail.com" );
		$o_location->setContactMobile( "222-222-2222" );
		$o_location->setContactName( "Roger Rabbit" );
		$o_location->setContactSameAsOwner( false );
		$o_location->setDescription( Location::$XS_DESCRIPTION_CONSULTING );
		$o_location->setEntityStartDate( date('Y-m-d') );
		$o_location->setEntityState( "AZ" );
		$o_location->setFax( "333-333-3333" );
		$o_location->setIndustry( Location::$XS_INDUSTRY_CONSULTING );
		$o_location->setLengthOfCurrentOwnership( "1" );
		$o_location->setOrganizationMission( "We take your business where it needs to go" );
		$o_location->setOwnership( Location::$XS_OWNERSHIP_PRIVATE );
		$o_location->setQuantityOfLocation( 1 );
		$o_location->setSalesAgentName( "John Doe" );
		$o_location->setTaxExempt( false );
		$o_location->setTotalCustomers( 1500 );
		$o_location->setYearIncorporated( "2013" );
		$o_location->setYearsInBusiness( 1 );
		$o_location->setDBAAddress( $o_dba_address );

		$o_contact = new PrincipalContact();
		$o_contact->setLastName( "Rabbit" );
		$o_contact->setFirstName( "Roger" );

		$o_mailing = new Address( Address::$XS_ADDRESS_NAME_MAILING );
		$o_mailing->setLine1( "789 Maple Street" );
		$o_mailing->setCity( "Toontown" );
		$o_mailing->setState( "AZ" );
		$o_mailing->setZipcode( "85284" );
		$o_mailing->setCountry( "USA" );
	
		$o_contact->setMailingAddress( $o_mailing );
		$o_contact->setPhoneNumber( "111-111-1111" );
		$o_contact->setMobilePhoneNumber( "222-222-2222" );
		$o_contact->setEmail( "test-princ-contact@test.com" );
		$o_contact->setTitle( "CEO" );
		$o_contact->setBirthdate( date('Y-m-d') );
		$o_contact->setAuthorizedUser( True );
		$o_contact->setAccountSigner( True );
		$o_contact->setSSN( "111-11-1111" );
		$o_contact->setIsPrimary( True );
		$o_contact->setFax( "111-111-1112" );
		$o_contact->setOwnershipPercentage( 100 );
		$o_contact->setContactType( PrincipalContact::$XS_CONTACT_TYPE_OWNER );
		$o_contact->setDriverLicenseId( "123 456 789" );
		$o_contact->setLicenseState( "AZ" );
		$o_location->addPrincipalContact( $o_contact );
		
		$o_bc_details = new BankCardDetails();
		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT );
		$o_bc_details->setAuthorizationFee( 1 );
		$o_bc_details->setAverageMonthlyVolume( 1 );
		$o_bc_details->setAverageTicket( 1 );
		$o_bc_details->setCardPresentPercentage( 80 );
		$o_bc_details->setFlatRate( 1 );
		$o_bc_details->setInternetPercentage( 10 );
		$o_bc_details->setMailOrderPercentage( 1 );
		$o_bc_details->setTelephoneOrderPercentage( 9 );
		$o_bc_details->setMaxMonthlyVolume( 1 );
		$o_bc_details->setMaxTicket( 1 );
		$o_bc_details->setTransactionFee( 1 );
	
		$o_location->setBankCardDetails( $o_bc_details );
	
		$o_merch_app->addLocation( $o_location );
		try {
			$o_merch_app = $o_client->submitApplication( $o_merch_app );
			echo "\n\nSUCCESS";
			echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
			echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
		} catch ( BaseCommerceClientException $e ) {
			var_dump($e->getMessage());
			var_dump($e->getTrace());
		}
    }
    
    function testTwoSameTerminals() {
		global $s_gateway_username, $s_gateway_password, $s_gateway_key;
    	
    	$o_client = new BaseCommerceClient($s_gateway_username, $s_gateway_password, $s_gateway_key);
    
	    $o_merch_app = new MerchantApplication();
    
	    $o_merch_app->setApiVersion("1.2");
	    $o_merch_app->setCode($s_gateway_username);
    
	    $o_account = new Account();
	    $o_account->setAccountName( "UNIQUE ACCOUNT" );
    	$o_account->setTestAccount( True );
	    $o_account->setAcceptACH( False );
	    $o_account->setAcceptBC( False );
    
		$o_legal_address = new Address( Address::$XS_ADDRESS_NAME_LEGAL );
		$o_legal_address->setLine1( "123 Oak Street" );
		$o_legal_address->setCity( "Toontown" );
		$o_legal_address->setState( "AZ" );
		$o_legal_address->setZipcode( "85284" );
		$o_legal_address->setCountry( "USA" );
	
		$o_account->setLegalAddress( $o_legal_address );
	
		$o_dba_address = new Address( Address::$XS_ADDRESS_NAME_DBA );
		$o_dba_address->setLine1( "456 Maple Street" );
		$o_dba_address->setCity( "Toontown" );
		$o_dba_address->setState( "AZ" );
		$o_dba_address->setZipcode( "85284" );
		$o_dba_address->setCountry( "USA" );
	
		$o_account->setAccountPhoneNumber( "123-456-7891" );
		$o_account->setReferralPartnerID( "a0Fi0000006Om7a" );
		$o_account->setCustomerServicePhoneNumber( "981-124-6431" );
		$o_account->setDBA("ACME Inc");
		$o_account->setEntityType( Account::$XS_ENTITY_TYPE_CORP );
		$o_account->setAssociationNumber( "1111" );
		$o_account->setCongaTemplateId( "1111" );
		$o_account->setEIN( "123456789" );
		$o_account->setIpAddressOfAppSubmission( "192.168.0.1" );
		$o_account->setWebsite( "www.google.com" );
		$o_account->setSettlementAccountBankName( "test bank name" );
		$o_account->setSettlementAccountBankPhone( "123-123-1234" );
		$o_account->setSettlementAccountName( "test account name" );
		$o_account->setSettlementAccountNumber( "123456789" );
		$o_account->setSettlementAccountRoutingNumber( "000000000" );
		$o_account->setSettlementSameAsFeeAccount( true );
	
		$o_merch_app->setAccount( $o_account );
	
		$o_location = new Location();
		$o_location->setAlternativeFax("111-111-1111");
		$o_location->setAnnualRevenue( 10000000 );
		$o_location->setContactEmail( "contact@gmail.com" );
		$o_location->setContactMobile( "222-222-2222" );
		$o_location->setContactName( "Roger Rabbit" );
		$o_location->setContactSameAsOwner( false );
		$o_location->setDescription( Location::$XS_DESCRIPTION_CONSULTING );
		$o_location->setEntityStartDate( date('Y-m-d') );
		$o_location->setEntityState( "AZ" );
		$o_location->setFax( "333-333-3333" );
		$o_location->setIndustry( Location::$XS_INDUSTRY_CONSULTING );
		$o_location->setLengthOfCurrentOwnership( "1" );
		$o_location->setOrganizationMission( "We take your business where it needs to go" );
		$o_location->setOwnership( Location::$XS_OWNERSHIP_PRIVATE );
		$o_location->setQuantityOfLocation( 1 );
		$o_location->setSalesAgentName( "John Doe" );
		$o_location->setTaxExempt( false );
		$o_location->setTotalCustomers( 1500 );
		$o_location->setYearIncorporated( "2013" );
		$o_location->setYearsInBusiness( 1 );
		$o_location->setDBAAddress( $o_dba_address );

		$o_contact = new PrincipalContact();
		$o_contact->setLastName( "Rabbit" );
		$o_contact->setFirstName( "Roger" );

		$o_mailing = new Address( Address::$XS_ADDRESS_NAME_MAILING );
		$o_mailing->setLine1( "789 Maple Street" );
		$o_mailing->setCity( "Toontown" );
		$o_mailing->setState( "AZ" );
		$o_mailing->setZipcode( "85284" );
		$o_mailing->setCountry( "USA" );
	
		$o_contact->setMailingAddress( $o_mailing );
		$o_contact->setPhoneNumber( "111-111-1111" );
		$o_contact->setMobilePhoneNumber( "222-222-2222" );
		$o_contact->setEmail( "test-princ-contact@test.com" );
		$o_contact->setTitle( "CEO" );
		$o_contact->setBirthdate( date('Y-m-d') );
		$o_contact->setAuthorizedUser( True );
		$o_contact->setAccountSigner( True );
		$o_contact->setSSN( "111-11-1111" );
		$o_contact->setIsPrimary( True );
		$o_contact->setFax( "111-111-1112" );
		$o_contact->setOwnershipPercentage( 100 );
		$o_contact->setContactType( PrincipalContact::$XS_CONTACT_TYPE_OWNER );
		$o_contact->setDriverLicenseId( "123 456 789" );
		$o_contact->setLicenseState( "AZ" );
		$o_location->addPrincipalContact( $o_contact );
		
		$o_terminal = new Terminal();
		$o_terminal->setAutoBatchTerminal(True);
		$o_terminal->setAutoBatchTime(Terminal::$XS_AUTO_BATCH_TIME_9_30_PM);
		$o_terminal->setAutoBatchTimeZone(Terminal::$XS_AUTO_BATCH_TIME_ZONE_ET);
		$o_terminal->setSignatureFloorLimit(Terminal::$XS_SIGNATURE_FLOOR_LIMIT_NONE);
		$o_terminal->setModelType( Terminal::$XS_MODEL_TYPE_VX510_RESTAURANT );

		$o_terminal->setQuantity( 2 );
		$o_terminal->setBilling( Terminal::$XS_BiLLING_MERCHANT );
		$o_terminal->setConnectionType( Terminal::$XS_CONNECTION_TYPE_ETHERNET );
		$o_terminal->setImplementation( Terminal::$XS_IMPLEMENTATION_NEW_BC);
		$o_terminal->setShippingAddress( $o_dba_address );
		$o_location->addTerminal( $o_terminal );
		
		$o_bc_details = new BankCardDetails();
		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT );
		$o_bc_details->setAuthorizationFee( 1 );
		$o_bc_details->setAverageMonthlyVolume( 1 );
		$o_bc_details->setAverageTicket( 1 );
		$o_bc_details->setCardPresentPercentage( 80 );
		$o_bc_details->setFlatRate( 1 );
		$o_bc_details->setInternetPercentage( 10 );
		$o_bc_details->setMailOrderPercentage( 1 );
		$o_bc_details->setTelephoneOrderPercentage( 9 );
		$o_bc_details->setMaxMonthlyVolume( 1 );
		$o_bc_details->setMaxTicket( 1 );
		$o_bc_details->setTransactionFee( 1 );
	
		$o_location->setBankCardDetails( $o_bc_details );
	
		$o_merch_app->addLocation( $o_location );
		try {
			$o_merch_app = $o_client->submitApplication( $o_merch_app );
			echo "\n\nSUCCESS";
			echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
			echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
		} catch ( BaseCommerceClientException $e ) {
			var_dump($e->getMessage());
			var_dump($e->getTrace());
		}
    }
    
    function testTwoDifferentTerminals() {
		global $s_gateway_username, $s_gateway_password, $s_gateway_key;
    	
    	$o_client = new BaseCommerceClient($s_gateway_username, $s_gateway_password, $s_gateway_key);
    
	    $o_merch_app = new MerchantApplication();
    
	    $o_merch_app->setApiVersion("1.2");
	    $o_merch_app->setCode($s_gateway_username);
    
	    $o_account = new Account();
	    $o_account->setAccountName( "TEST Account" );
    	$o_account->setTestAccount( True );
	    $o_account->setAcceptACH( False );
	    $o_account->setAcceptBC( False );
    
		$o_legal_address = new Address( Address::$XS_ADDRESS_NAME_LEGAL );
		$o_legal_address->setLine1( "123 Oak Street" );
		$o_legal_address->setCity( "Toontown" );
		$o_legal_address->setState( "AZ" );
		$o_legal_address->setZipcode( "85284" );
		$o_legal_address->setCountry( "USA" );
	
		$o_account->setLegalAddress( $o_legal_address );
	
		$o_dba_address = new Address( Address::$XS_ADDRESS_NAME_DBA );
		$o_dba_address->setLine1( "456 Maple Street" );
		$o_dba_address->setCity( "Toontown" );
		$o_dba_address->setState( "AZ" );
		$o_dba_address->setZipcode( "85284" );
		$o_dba_address->setCountry( "USA" );
	
		$o_account->setAccountPhoneNumber( "123-456-7891" );
		$o_account->setReferralPartnerID( "a0Fi0000006Om7a" );
		$o_account->setCustomerServicePhoneNumber( "981-124-6431" );
		$o_account->setDBA("ACME Inc");
		$o_account->setEntityType( Account::$XS_ENTITY_TYPE_CORP );
		$o_account->setAssociationNumber( "1111" );
		$o_account->setCongaTemplateId( "1111" );
		$o_account->setEIN( "123456789" );
		$o_account->setIpAddressOfAppSubmission( "192.168.0.1" );
		$o_account->setWebsite( "www.google.com" );
		$o_account->setSettlementAccountBankName( "test bank name" );
		$o_account->setSettlementAccountBankPhone( "123-123-1234" );
		$o_account->setSettlementAccountName( "test account name" );
		$o_account->setSettlementAccountNumber( "123456789" );
		$o_account->setSettlementAccountRoutingNumber( "000000000" );
		$o_account->setSettlementSameAsFeeAccount( true );
	
		$o_merch_app->setAccount( $o_account );
	
		$o_location = new Location();
		$o_location->setAlternativeFax("111-111-1111");
		$o_location->setAnnualRevenue( 10000000 );
		$o_location->setContactEmail( "contact@gmail.com" );
		$o_location->setContactMobile( "222-222-2222" );
		$o_location->setContactName( "Roger Rabbit" );
		$o_location->setContactSameAsOwner( false );
		$o_location->setDescription( Location::$XS_DESCRIPTION_CONSULTING );
		$o_location->setEntityStartDate( date('Y-m-d') );
		$o_location->setEntityState( "AZ" );
		$o_location->setFax( "333-333-3333" );
		$o_location->setIndustry( Location::$XS_INDUSTRY_CONSULTING );
		$o_location->setLengthOfCurrentOwnership( "1" );
		$o_location->setOrganizationMission( "We take your business where it needs to go" );
		$o_location->setOwnership( Location::$XS_OWNERSHIP_PRIVATE );
		$o_location->setQuantityOfLocation( 1 );
		$o_location->setSalesAgentName( "John Doe" );
		$o_location->setTaxExempt( false );
		$o_location->setTotalCustomers( 1500 );
		$o_location->setYearIncorporated( "2013" );
		$o_location->setYearsInBusiness( 1 );
		$o_location->setDBAAddress( $o_dba_address );

		$o_contact = new PrincipalContact();
		$o_contact->setLastName( "Rabbit" );
		$o_contact->setFirstName( "Roger" );

		$o_mailing = new Address( Address::$XS_ADDRESS_NAME_MAILING );
		$o_mailing->setLine1( "789 Maple Street" );
		$o_mailing->setCity( "Toontown" );
		$o_mailing->setState( "AZ" );
		$o_mailing->setZipcode( "85284" );
		$o_mailing->setCountry( "USA" );
	
		$o_contact->setMailingAddress( $o_mailing );
		$o_contact->setPhoneNumber( "111-111-1111" );
		$o_contact->setMobilePhoneNumber( "222-222-2222" );
		$o_contact->setEmail( "test-princ-contact@test.com" );
		$o_contact->setTitle( "CEO" );
		$o_contact->setBirthdate( date('Y-m-d') );
		$o_contact->setAuthorizedUser( True );
		$o_contact->setAccountSigner( True );
		$o_contact->setSSN( "111-11-1111" );
		$o_contact->setIsPrimary( True );
		$o_contact->setFax( "111-111-1112" );
		$o_contact->setOwnershipPercentage( 100 );
		$o_contact->setContactType( PrincipalContact::$XS_CONTACT_TYPE_OWNER );
		$o_contact->setDriverLicenseId( "123 456 789" );
		$o_contact->setLicenseState( "AZ" );
		$o_location->addPrincipalContact( $o_contact );
		
		$o_terminal = new Terminal();
		$o_terminal->setAutoBatchTerminal(True);
		$o_terminal->setAutoBatchTime(Terminal::$XS_AUTO_BATCH_TIME_9_30_PM);
		$o_terminal->setAutoBatchTimeZone(Terminal::$XS_AUTO_BATCH_TIME_ZONE_ET);
		$o_terminal->setSignatureFloorLimit(Terminal::$XS_SIGNATURE_FLOOR_LIMIT_NONE);
		$o_terminal->setModelType( Terminal::$XS_MODEL_TYPE_VX510_RESTAURANT );

		$o_terminal->setQuantity( 1 );
		$o_terminal->setBilling( Terminal::$XS_BiLLING_MERCHANT );
		$o_terminal->setConnectionType( Terminal::$XS_CONNECTION_TYPE_ETHERNET );
		$o_terminal->setImplementation( Terminal::$XS_IMPLEMENTATION_NEW_BC);
		$o_terminal->setShippingAddress( $o_dba_address );
		$o_location->addTerminal( $o_terminal );
		
		$o_terminal = new Terminal();
		$o_terminal->setAutoBatchTerminal(True);
		$o_terminal->setAutoBatchTime(Terminal::$XS_AUTO_BATCH_TIME_9_30_PM);
		$o_terminal->setAutoBatchTimeZone(Terminal::$XS_AUTO_BATCH_TIME_ZONE_ET);
		$o_terminal->setSignatureFloorLimit(Terminal::$XS_SIGNATURE_FLOOR_LIMIT_NONE);
		$o_terminal->setModelType( Terminal::$XS_MODEL_TYPE_ICT220_RESTAURANT );

		$o_terminal->setQuantity( 1 );
		$o_terminal->setBilling( Terminal::$XS_BiLLING_MERCHANT );
		$o_terminal->setConnectionType( Terminal::$XS_CONNECTION_TYPE_DUAL );
		$o_terminal->setImplementation( Terminal::$XS_IMPLEMENTATION_NEW_BC);
		$o_terminal->setShippingAddress( $o_dba_address );
		$o_location->addTerminal( $o_terminal );
		
		$o_bc_details = new BankCardDetails();
		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT );
		$o_bc_details->setAuthorizationFee( 1 );
		$o_bc_details->setAverageMonthlyVolume( 1 );
		$o_bc_details->setAverageTicket( 1 );
		$o_bc_details->setCardPresentPercentage( 80 );
		$o_bc_details->setFlatRate( 1 );
		$o_bc_details->setInternetPercentage( 10 );
		$o_bc_details->setMailOrderPercentage( 1 );
		$o_bc_details->setTelephoneOrderPercentage( 9 );
		$o_bc_details->setMaxMonthlyVolume( 1 );
		$o_bc_details->setMaxTicket( 1 );
		$o_bc_details->setTransactionFee( 1 );
	
		$o_location->setBankCardDetails( $o_bc_details );
	
		$o_merch_app->addLocation( $o_location );
		try {
			$o_merch_app = $o_client->submitApplication( $o_merch_app );
			echo "\n\nSUCCESS";
			echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
			echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
		} catch ( BaseCommerceClientException $e ) {
			var_dump($e->getMessage());
			var_dump($e->getTrace());
		}
    }
    
    function testInternetObject() {
		global $s_gateway_username, $s_gateway_password, $s_gateway_key;
    	
    	$o_client = new BaseCommerceClient($s_gateway_username, $s_gateway_password, $s_gateway_key);
    
	    $o_merch_app = new MerchantApplication();
    
	    $o_merch_app->setApiVersion("1.2");
	    $o_merch_app->setCode($s_gateway_username);
    
	    $o_account = new Account();
	    $o_account->setAccountName( "TEST Account" );
    	$o_account->setTestAccount( True );
	    $o_account->setAcceptACH( False );
	    $o_account->setAcceptBC( False );
    
		$o_legal_address = new Address( Address::$XS_ADDRESS_NAME_LEGAL );
		$o_legal_address->setLine1( "123 Oak Street" );
		$o_legal_address->setCity( "Toontown" );
		$o_legal_address->setState( "AZ" );
		$o_legal_address->setZipcode( "85284" );
		$o_legal_address->setCountry( "USA" );
	
		$o_account->setLegalAddress( $o_legal_address );
	
		$o_dba_address = new Address( Address::$XS_ADDRESS_NAME_DBA );
		$o_dba_address->setLine1( "456 Maple Street" );
		$o_dba_address->setCity( "Toontown" );
		$o_dba_address->setState( "AZ" );
		$o_dba_address->setZipcode( "85284" );
		$o_dba_address->setCountry( "USA" );
	
		$o_account->setAccountPhoneNumber( "123-456-7891" );
		$o_account->setReferralPartnerID( "a0Fi0000006Om7a" );
		$o_account->setCustomerServicePhoneNumber( "981-124-6431" );
		$o_account->setDBA("ACME Inc");
		$o_account->setEntityType( Account::$XS_ENTITY_TYPE_CORP );
		$o_account->setAssociationNumber( "1111" );
		$o_account->setCongaTemplateId( "1111" );
		$o_account->setEIN( "123456789" );
		$o_account->setIpAddressOfAppSubmission( "192.168.0.1" );
		$o_account->setWebsite( "www.google.com" );
		$o_account->setSettlementAccountBankName( "test bank name" );
		$o_account->setSettlementAccountBankPhone( "123-123-1234" );
		$o_account->setSettlementAccountName( "test account name" );
		$o_account->setSettlementAccountNumber( "123456789" );
		$o_account->setSettlementAccountRoutingNumber( "000000000" );
		$o_account->setSettlementSameAsFeeAccount( true );
	
		$o_merch_app->setAccount( $o_account );
	
		$o_location = new Location();
		$o_location->setAlternativeFax("111-111-1111");
		$o_location->setAnnualRevenue( 10000000 );
		$o_location->setContactEmail( "contact@gmail.com" );
		$o_location->setContactMobile( "222-222-2222" );
		$o_location->setContactName( "Roger Rabbit" );
		$o_location->setContactSameAsOwner( false );
		$o_location->setDescription( Location::$XS_DESCRIPTION_CONSULTING );
		$o_location->setEntityStartDate( date('Y-m-d') );
		$o_location->setEntityState( "AZ" );
		$o_location->setFax( "333-333-3333" );
		$o_location->setIndustry( Location::$XS_INDUSTRY_CONSULTING );
		$o_location->setLengthOfCurrentOwnership( "1" );
		$o_location->setOrganizationMission( "We take your business where it needs to go" );
		$o_location->setOwnership( Location::$XS_OWNERSHIP_PRIVATE );
		$o_location->setQuantityOfLocation( 1 );
		$o_location->setSalesAgentName( "John Doe" );
		$o_location->setTaxExempt( false );
		$o_location->setTotalCustomers( 1500 );
		$o_location->setYearIncorporated( "2013" );
		$o_location->setYearsInBusiness( 1 );
		$o_location->setDBAAddress( $o_dba_address );

		$o_contact = new PrincipalContact();
		$o_contact->setLastName( "Rabbit" );
		$o_contact->setFirstName( "Roger" );

		$o_mailing = new Address( Address::$XS_ADDRESS_NAME_MAILING );
		$o_mailing->setLine1( "789 Maple Street" );
		$o_mailing->setCity( "Toontown" );
		$o_mailing->setState( "AZ" );
		$o_mailing->setZipcode( "85284" );
		$o_mailing->setCountry( "USA" );
	
		$o_contact->setMailingAddress( $o_mailing );
		$o_contact->setPhoneNumber( "111-111-1111" );
		$o_contact->setMobilePhoneNumber( "222-222-2222" );
		$o_contact->setEmail( "test-princ-contact@test.com" );
		$o_contact->setTitle( "CEO" );
		$o_contact->setBirthdate( date('Y-m-d') );
		$o_contact->setAuthorizedUser( True );
		$o_contact->setAccountSigner( True );
		$o_contact->setSSN( "111-11-1111" );
		$o_contact->setIsPrimary( True );
		$o_contact->setFax( "111-111-1112" );
		$o_contact->setOwnershipPercentage( 100 );
		$o_contact->setContactType( PrincipalContact::$XS_CONTACT_TYPE_OWNER );
		$o_contact->setDriverLicenseId( "123 456 789" );
		$o_contact->setLicenseState( "AZ" );
		$o_location->addPrincipalContact( $o_contact );
		
		$o_bc_details = new BankCardDetails();
		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT );
		$o_bc_details->setAuthorizationFee( 1 );
		$o_bc_details->setAverageMonthlyVolume( 1 );
		$o_bc_details->setAverageTicket( 1 );
		$o_bc_details->setCardPresentPercentage( 80 );
		$o_bc_details->setFlatRate( 1 );
		$o_bc_details->setInternetPercentage( 10 );
		$o_bc_details->setMailOrderPercentage( 1 );
		$o_bc_details->setTelephoneOrderPercentage( 9 );
		$o_bc_details->setMaxMonthlyVolume( 1 );
		$o_bc_details->setMaxTicket( 1 );
		$o_bc_details->setTransactionFee( 1 );
	
		$o_location->setBankCardDetails( $o_bc_details );
		
		$o_internet = new Internet();
		$o_internet->setFulfillmentVendorUtilized( True );
		$o_internet->setPercentOfSalesToNonUsCardHolders( 0 );
		$o_internet->setWebsiteIpAddress( "255.255.255.0" );
		$o_internet->setApplicantOwnsWebDomainAndContent( False );
		$o_internet->setGatewaySoftwareName( "Base Commerce" );
		$o_internet->setGatewaySoftwareVendor( "Base Commerce" );
		$o_internet->setGatewaySoftwareVersion( "1" );

		$o_location->setInternet( $o_internet );
		
		$o_merch_app->addLocation( $o_location );
		try {
			$o_merch_app = $o_client->submitApplication( $o_merch_app );
			echo "\n\nSUCCESS";
			echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
			echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
		} catch ( BaseCommerceClientException $e ) {
			var_dump($e->getMessage());
			var_dump($e->getTrace());
		}
    }
    
    function testMOTOObject() {
		global $s_gateway_username, $s_gateway_password, $s_gateway_key;
    	
    	$o_client = new BaseCommerceClient($s_gateway_username, $s_gateway_password, $s_gateway_key);
    
	    $o_merch_app = new MerchantApplication();
    
	    $o_merch_app->setApiVersion("1.2");
	    $o_merch_app->setCode($s_gateway_username);
    
	    $o_account = new Account();
	    $o_account->setAccountName( "TEST Account" );
    	$o_account->setTestAccount( True );
	    $o_account->setAcceptACH( False );
	    $o_account->setAcceptBC( False );
    
		$o_legal_address = new Address( Address::$XS_ADDRESS_NAME_LEGAL );
		$o_legal_address->setLine1( "123 Oak Street" );
		$o_legal_address->setCity( "Toontown" );
		$o_legal_address->setState( "AZ" );
		$o_legal_address->setZipcode( "85284" );
		$o_legal_address->setCountry( "USA" );
	
		$o_account->setLegalAddress( $o_legal_address );
	
		$o_dba_address = new Address( Address::$XS_ADDRESS_NAME_DBA );
		$o_dba_address->setLine1( "456 Maple Street" );
		$o_dba_address->setCity( "Toontown" );
		$o_dba_address->setState( "AZ" );
		$o_dba_address->setZipcode( "85284" );
		$o_dba_address->setCountry( "USA" );
	
		$o_account->setAccountPhoneNumber( "123-456-7891" );
		$o_account->setReferralPartnerID( "a0Fi0000006Om7a" );
		$o_account->setCustomerServicePhoneNumber( "981-124-6431" );
		$o_account->setDBA("ACME Inc");
		$o_account->setEntityType( Account::$XS_ENTITY_TYPE_CORP );
		$o_account->setAssociationNumber( "1111" );
		$o_account->setCongaTemplateId( "1111" );
		$o_account->setEIN( "123456789" );
		$o_account->setIpAddressOfAppSubmission( "192.168.0.1" );
		$o_account->setWebsite( "www.google.com" );
		$o_account->setSettlementAccountBankName( "test bank name" );
		$o_account->setSettlementAccountBankPhone( "123-123-1234" );
		$o_account->setSettlementAccountName( "test account name" );
		$o_account->setSettlementAccountNumber( "123456789" );
		$o_account->setSettlementAccountRoutingNumber( "000000000" );
		$o_account->setSettlementSameAsFeeAccount( true );
	
		$o_merch_app->setAccount( $o_account );
	
		$o_location = new Location();
		$o_location->setAlternativeFax("111-111-1111");
		$o_location->setAnnualRevenue( 10000000 );
		$o_location->setContactEmail( "contact@gmail.com" );
		$o_location->setContactMobile( "222-222-2222" );
		$o_location->setContactName( "Roger Rabbit" );
		$o_location->setContactSameAsOwner( false );
		$o_location->setDescription( Location::$XS_DESCRIPTION_CONSULTING );
		$o_location->setEntityStartDate( date('Y-m-d') );
		$o_location->setEntityState( "AZ" );
		$o_location->setFax( "333-333-3333" );
		$o_location->setIndustry( Location::$XS_INDUSTRY_CONSULTING );
		$o_location->setLengthOfCurrentOwnership( "1" );
		$o_location->setOrganizationMission( "We take your business where it needs to go" );
		$o_location->setOwnership( Location::$XS_OWNERSHIP_PRIVATE );
		$o_location->setQuantityOfLocation( 1 );
		$o_location->setSalesAgentName( "John Doe" );
		$o_location->setTaxExempt( false );
		$o_location->setTotalCustomers( 1500 );
		$o_location->setYearIncorporated( "2013" );
		$o_location->setYearsInBusiness( 1 );
		$o_location->setDBAAddress( $o_dba_address );

		$o_contact = new PrincipalContact();
		$o_contact->setLastName( "Rabbit" );
		$o_contact->setFirstName( "Roger" );

		$o_mailing = new Address( Address::$XS_ADDRESS_NAME_MAILING );
		$o_mailing->setLine1( "789 Maple Street" );
		$o_mailing->setCity( "Toontown" );
		$o_mailing->setState( "AZ" );
		$o_mailing->setZipcode( "85284" );
		$o_mailing->setCountry( "USA" );
	
		$o_contact->setMailingAddress( $o_mailing );
		$o_contact->setPhoneNumber( "111-111-1111" );
		$o_contact->setMobilePhoneNumber( "222-222-2222" );
		$o_contact->setEmail( "test-princ-contact@test.com" );
		$o_contact->setTitle( "CEO" );
		$o_contact->setBirthdate( date('Y-m-d') );
		$o_contact->setAuthorizedUser( True );
		$o_contact->setAccountSigner( True );
		$o_contact->setSSN( "111-11-1111" );
		$o_contact->setIsPrimary( True );
		$o_contact->setFax( "111-111-1112" );
		$o_contact->setOwnershipPercentage( 100 );
		$o_contact->setContactType( PrincipalContact::$XS_CONTACT_TYPE_OWNER );
		$o_contact->setDriverLicenseId( "123 456 789" );
		$o_contact->setLicenseState( "AZ" );
		$o_location->addPrincipalContact( $o_contact );
		
		$o_bc_details = new BankCardDetails();
		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT );
		$o_bc_details->setAuthorizationFee( 1 );
		$o_bc_details->setAverageMonthlyVolume( 1 );
		$o_bc_details->setAverageTicket( 1 );
		$o_bc_details->setCardPresentPercentage( 80 );
		$o_bc_details->setFlatRate( 1 );
		$o_bc_details->setInternetPercentage( 10 );
		$o_bc_details->setMailOrderPercentage( 1 );
		$o_bc_details->setTelephoneOrderPercentage( 9 );
		$o_bc_details->setMaxMonthlyVolume( 1 );
		$o_bc_details->setMaxTicket( 1 );
		$o_bc_details->setTransactionFee( 1 );
	
		$o_location->setBankCardDetails( $o_bc_details );
		
		$o_moto = new MOTO();
		$o_moto->setOrderForms( MOTO::$XS_ORDER_FORMS_DESTROYED );
		$o_moto->addProcessingMethod( MOTO::$XS_PROCESSING_METHOD_VIRTUAL_TERMINAL_GATEWAY );
		$o_moto->setOutboundTelemarketingIsConducted( True );
		$o_moto->addOutboundMarketing( MOTO::$XS_OUTBOUND_MARKETING_CATALOG);
		$o_moto->addOutboundMarketing( MOTO::$XS_OUTBOUND_MARKETING_ENVELOPE );
		
		$o_location->setMoto( $o_moto );
		
		$o_merch_app->addLocation( $o_location );
		try {
			$o_merch_app = $o_client->submitApplication( $o_merch_app );
			echo "\n\nSUCCESS";
			echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
			echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
		} catch ( BaseCommerceClientException $e ) {
			var_dump($e->getMessage());
			var_dump($e->getTrace());
		}
    }
    
    function testPOSObject() {
		global $s_gateway_username, $s_gateway_password, $s_gateway_key;
    	
    	$o_client = new BaseCommerceClient($s_gateway_username, $s_gateway_password, $s_gateway_key);
    
	    $o_merch_app = new MerchantApplication();
    
	    $o_merch_app->setApiVersion("1.2");
	    $o_merch_app->setCode($s_gateway_username);
    
	    $o_account = new Account();
	    $o_account->setAccountName( "TEST Account" );
    	$o_account->setTestAccount( True );
	    $o_account->setAcceptACH( False );
	    $o_account->setAcceptBC( False );
    
		$o_legal_address = new Address( Address::$XS_ADDRESS_NAME_LEGAL );
		$o_legal_address->setLine1( "123 Oak Street" );
		$o_legal_address->setCity( "Toontown" );
		$o_legal_address->setState( "AZ" );
		$o_legal_address->setZipcode( "85284" );
		$o_legal_address->setCountry( "USA" );
	
		$o_account->setLegalAddress( $o_legal_address );
	
		$o_dba_address = new Address( Address::$XS_ADDRESS_NAME_DBA );
		$o_dba_address->setLine1( "456 Maple Street" );
		$o_dba_address->setCity( "Toontown" );
		$o_dba_address->setState( "AZ" );
		$o_dba_address->setZipcode( "85284" );
		$o_dba_address->setCountry( "USA" );
	
		$o_account->setAccountPhoneNumber( "123-456-7891" );
		$o_account->setReferralPartnerID( "a0Fi0000006Om7a" );
		$o_account->setCustomerServicePhoneNumber( "981-124-6431" );
		$o_account->setDBA("ACME Inc");
		$o_account->setEntityType( Account::$XS_ENTITY_TYPE_CORP );
		$o_account->setAssociationNumber( "1111" );
		$o_account->setCongaTemplateId( "1111" );
		$o_account->setEIN( "123456789" );
		$o_account->setIpAddressOfAppSubmission( "192.168.0.1" );
		$o_account->setWebsite( "www.google.com" );
		$o_account->setSettlementAccountBankName( "test bank name" );
		$o_account->setSettlementAccountBankPhone( "123-123-1234" );
		$o_account->setSettlementAccountName( "test account name" );
		$o_account->setSettlementAccountNumber( "123456789" );
		$o_account->setSettlementAccountRoutingNumber( "000000000" );
		$o_account->setSettlementSameAsFeeAccount( true );
	
		$o_merch_app->setAccount( $o_account );
	
		$o_location = new Location();
		$o_location->setAlternativeFax("111-111-1111");
		$o_location->setAnnualRevenue( 10000000 );
		$o_location->setContactEmail( "contact@gmail.com" );
		$o_location->setContactMobile( "222-222-2222" );
		$o_location->setContactName( "Roger Rabbit" );
		$o_location->setContactSameAsOwner( false );
		$o_location->setDescription( Location::$XS_DESCRIPTION_CONSULTING );
		$o_location->setEntityStartDate( date('Y-m-d') );
		$o_location->setEntityState( "AZ" );
		$o_location->setFax( "333-333-3333" );
		$o_location->setIndustry( Location::$XS_INDUSTRY_CONSULTING );
		$o_location->setLengthOfCurrentOwnership( "1" );
		$o_location->setOrganizationMission( "We take your business where it needs to go" );
		$o_location->setOwnership( Location::$XS_OWNERSHIP_PRIVATE );
		$o_location->setQuantityOfLocation( 1 );
		$o_location->setSalesAgentName( "John Doe" );
		$o_location->setTaxExempt( false );
		$o_location->setTotalCustomers( 1500 );
		$o_location->setYearIncorporated( "2013" );
		$o_location->setYearsInBusiness( 1 );
		$o_location->setDBAAddress( $o_dba_address );

		$o_contact = new PrincipalContact();
		$o_contact->setLastName( "Rabbit" );
		$o_contact->setFirstName( "Roger" );

		$o_mailing = new Address( Address::$XS_ADDRESS_NAME_MAILING );
		$o_mailing->setLine1( "789 Maple Street" );
		$o_mailing->setCity( "Toontown" );
		$o_mailing->setState( "AZ" );
		$o_mailing->setZipcode( "85284" );
		$o_mailing->setCountry( "USA" );
	
		$o_contact->setMailingAddress( $o_mailing );
		$o_contact->setPhoneNumber( "111-111-1111" );
		$o_contact->setMobilePhoneNumber( "222-222-2222" );
		$o_contact->setEmail( "test-princ-contact@test.com" );
		$o_contact->setTitle( "CEO" );
		$o_contact->setBirthdate( date('Y-m-d') );
		$o_contact->setAuthorizedUser( True );
		$o_contact->setAccountSigner( True );
		$o_contact->setSSN( "111-11-1111" );
		$o_contact->setIsPrimary( True );
		$o_contact->setFax( "111-111-1112" );
		$o_contact->setOwnershipPercentage( 100 );
		$o_contact->setContactType( PrincipalContact::$XS_CONTACT_TYPE_OWNER );
		$o_contact->setDriverLicenseId( "123 456 789" );
		$o_contact->setLicenseState( "AZ" );
		$o_location->addPrincipalContact( $o_contact );
		
		$o_bc_details = new BankCardDetails();
		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT );
		$o_bc_details->setAuthorizationFee( 1 );
		$o_bc_details->setAverageMonthlyVolume( 1 );
		$o_bc_details->setAverageTicket( 1 );
		$o_bc_details->setCardPresentPercentage( 80 );
		$o_bc_details->setFlatRate( 1 );
		$o_bc_details->setInternetPercentage( 10 );
		$o_bc_details->setMailOrderPercentage( 1 );
		$o_bc_details->setTelephoneOrderPercentage( 9 );
		$o_bc_details->setMaxMonthlyVolume( 1 );
		$o_bc_details->setMaxTicket( 1 );
		$o_bc_details->setTransactionFee( 1 );
	
		$o_location->setBankCardDetails( $o_bc_details );
		
		$o_pos = new POS();
		$o_pos->setVendorName( "Base Commerce" );
		$o_pos->setVendorContactName( "John Smith" );
		$o_pos->setVendorPhoneNumber( "11111112345" );
		$o_pos->setMake( "Cloud Commerce" );
		$o_pos->setModel( "Cloud Commerce" );
		$o_pos->setVersion( "1.0" );

		$o_location->setPos( $o_pos );
		
		$o_merch_app->addLocation( $o_location );
		try {
			$o_merch_app = $o_client->submitApplication( $o_merch_app );
			echo "\n\nSUCCESS";
			echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
			echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
		} catch ( BaseCommerceClientException $e ) {
			var_dump($e->getMessage());
			var_dump($e->getTrace());
		}
    }
    
    function testFiveCards() {
    	global $s_gateway_username, $s_gateway_password, $s_gateway_key;
    	
    	$o_client = new BaseCommerceClient($s_gateway_username, $s_gateway_password, $s_gateway_key);
        $o_client->setSandbox(true);
	    $o_merch_app = new MerchantApplication();
    
	    $o_merch_app->setApiVersion("1.2");
	    //$o_merch_app->setCode($s_gateway_username);
    
	    $o_account = new Account();
	    $o_account->setAccountName( "Accepts Amex Only Account" );
    	$o_account->setTestAccount( True );
	    $o_account->setAcceptACH( False );
	    $o_account->setAcceptBC( True );
    
		$o_legal_address = new Address( Address::$XS_ADDRESS_NAME_LEGAL );
		$o_legal_address->setLine1( "123 Oak Street" );
		$o_legal_address->setCity( "Toontown" );
		$o_legal_address->setState( "AZ" );
		$o_legal_address->setZipcode( "85284" );
		$o_legal_address->setCountry( "USA" );
	
		$o_account->setLegalAddress( $o_legal_address );
	
		$o_dba_address = new Address( Address::$XS_ADDRESS_NAME_DBA );
		$o_dba_address->setLine1( "456 Maple Street" );
		$o_dba_address->setCity( "Toontown" );
		$o_dba_address->setState( "AZ" );
		$o_dba_address->setZipcode( "85284" );
		$o_dba_address->setCountry( "USA" );
	
		$o_account->setAccountPhoneNumber( "123-456-7891" );
		$o_account->setReferralPartnerID( "a0Fi0000006Om7a" );
		$o_account->setCustomerServicePhoneNumber( "981-124-6431" );
		$o_account->setDBA("ACME Inc");
                $o_account->setDdaAccountName("test dda name");
                $o_account->setDdaAccountNumber( "test dda number");
                $o_account->setDdaBankName( "test bank name");
                $o_account->setDdaRoutingNumber("test routing number");
		$o_account->setEntityType( Account::$XS_ENTITY_TYPE_CORP );
		$o_account->setAssociationNumber( "1111" );
		$o_account->setCongaTemplateId( "1111" );
		$o_account->setEIN( "123456789" );
		$o_account->setIpAddressOfAppSubmission( "192.168.0.1" );
		$o_account->setWebsite( "www.google.com" );
		$o_account->setSettlementAccountBankName( "test bank name" );
		$o_account->setSettlementAccountBankPhone( "123-123-1234" );
		$o_account->setSettlementAccountName( "test account name" );
		$o_account->setSettlementAccountNumber( "123456789" );
		$o_account->setSettlementAccountRoutingNumber( "000000000" );
		$o_account->setSettlementSameAsFeeAccount( true );
	
		$o_merch_app->setAccount( $o_account );
	
		$o_location = new Location();
		$o_location->setAlternativeFax("111-111-1111");
		$o_location->setAnnualRevenue( 10000000 );
		$o_location->setContactEmail( "contact@gmail.com" );
		$o_location->setContactMobile( "222-222-2222" );
		$o_location->setContactName( "Roger Rabbit" );
		$o_location->setContactSameAsOwner( false );
		$o_location->setDescription( Location::$XS_DESCRIPTION_CONSULTING );
		$o_location->setEntityStartDate( date('Y-m-d') );
		$o_location->setEntityState( "AZ" );
		$o_location->setFax( "333-333-3333" );
		$o_location->setIndustry( Location::$XS_INDUSTRY_CONSULTING );
		$o_location->setLengthOfCurrentOwnership( "1" );
		$o_location->setOrganizationMission( "We take your business where it needs to go" );
		$o_location->setOwnership( Location::$XS_OWNERSHIP_PRIVATE );
		$o_location->setQuantityOfLocation( 1 );
		$o_location->setSalesAgentName( "John Doe" );
		$o_location->setTaxExempt( false );
		$o_location->setTotalCustomers( 1500 );
		$o_location->setYearIncorporated( "2013" );
		$o_location->setYearsInBusiness( 1 );
		$o_location->setDBAAddress( $o_dba_address );

		$o_contact = new PrincipalContact();
		$o_contact->setLastName( "Rabbit" );
		$o_contact->setFirstName( "Roger" );

		$o_mailing = new Address( Address::$XS_ADDRESS_NAME_MAILING );
		$o_mailing->setLine1( "789 Maple Street" );
		$o_mailing->setCity( "Toontown" );
		$o_mailing->setState( "AZ" );
		$o_mailing->setZipcode( "85284" );
		$o_mailing->setCountry( "USA" );
	
		$o_contact->setMailingAddress( $o_mailing );
		$o_contact->setPhoneNumber( "111-111-1111" );
		$o_contact->setMobilePhoneNumber( "222-222-2222" );
		$o_contact->setEmail( "test-princ-contact@test.com" );
		$o_contact->setTitle( "CEO" );
		$o_contact->setBirthdate( date('Y-m-d') );
		$o_contact->setAuthorizedUser( True );
		$o_contact->setAccountSigner( True );
		$o_contact->setSSN( "111-11-1111" );
		$o_contact->setIsPrimary( True );
		$o_contact->setFax( "111-111-1112" );
		$o_contact->setOwnershipPercentage( 100 );
		$o_contact->setContactType( PrincipalContact::$XS_CONTACT_TYPE_OWNER );
		$o_contact->setDriverLicenseId( "123 456 789" );
		$o_contact->setLicenseState( "AZ" );
		$o_location->addPrincipalContact( $o_contact );
		
		$o_bc_details = new BankCardDetails();
//		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT );
//		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_MASTERCARD_CREDIT );
//		$o_bc_details->addCreditRequested( BankCardDetails::$XS_CREDIT_REQUESTED_VISA_CREDIT );
		$o_bc_details->addCreditRequested( BankCardDetails::$XS_OTHER_BRANDS_REQUESTED_AMERICAN_EXPRESS );
		$o_bc_details->setAuthorizationFee( 1 );
		$o_bc_details->setAverageMonthlyVolume( 1 );
		$o_bc_details->setAverageTicket( 1 );
		$o_bc_details->setCardPresentPercentage( 80 );
		$o_bc_details->setFlatRate( 1 );
		$o_bc_details->setInternetPercentage( 10 );
		$o_bc_details->setMailOrderPercentage( 1 );
		$o_bc_details->setTelephoneOrderPercentage( 9 );
		$o_bc_details->setMaxMonthlyVolume( 1 );
		$o_bc_details->setMaxTicket( 1 );
		$o_bc_details->setTransactionFee( 1 );
                $o_bc_details->addOtherBrandsRequested( BankCardDetails::$XS_OTHER_BRANDS_REQUESTED_AMERICAN_EXPRESS );
                $o_bc_details->setAmexAverageTicketAmount( 100 );
	
		$o_location->setBankCardDetails( $o_bc_details );
	
		$o_merch_app->addLocation( $o_location );
		try {
			$o_merch_app = $o_client->submitApplication( $o_merch_app );
			echo "\n\nSUCCESS";
			echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
			echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
//                        var_dump($o_merch_app);
		} catch ( BaseCommerceClientException $e ) {
                    echo "\n\nERROR";
			var_dump($e->getMessage());
//			var_dump($e->getTrace());
		}
    }
?>