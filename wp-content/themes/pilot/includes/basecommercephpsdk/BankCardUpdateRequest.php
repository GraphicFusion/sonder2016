<?php

/**
 * Description of BankCardUpdateRequest
 *
 */
class BankCardUpdateRequest {
    
    static $XS_BCUR_STATUS_FULFILLED = "FULFILLED";
    static $XS_BCUR_STATUS_FAILED = "FAILED";
    static $XS_BCUR_STATUS_PENDING = "PENDING";
    
    private $is_status;
    private $io_bank_card_update;
    private $io_messages;
    private $in_update_request_id=0;


    
        
    /**
     * 
     * Default Constructor.
     */
    function __construct() {
        $this->io_messages = array();        
    }
    
    
    /**
     *  Returns the update_request_id
     *  @return the update_request_id
     */
    public function getUpdateRequestID() {
        return $this->in_update_request_id;
    }

    /**
     *  sets the in_update_request_ID 
     *  @param vn_update_request_id the in_update_request_ID set 
     */
    public function setUpdateRequestID($vn_update_request_id) {
        $this->in_update_request_id = $vn_update_request_id;
    }
    
    /**
     * Returns the status
     * @return the status
     */
    public function getStatus() {
        return $this->is_status;
    }
    
    /**
     * sets the status
     * @param type $vs_status the status
     */
    public function setStatus( $vs_status ) {
        $this->is_status = $vs_status;
    }
    
    /**
     * returns true if the BankCardUpdateRequest's status is the same as the passed in status, false if different
     * @return true if the BankCardUpdateRequest's status is the same as the passed in status, false if different
     * @param type $vs_status the status being compared against
     */
    public function isStatus( $vs_status ) {
        return $this->is_status ==  $vs_status;
    }
    
    /**
     * returns the bank card update object
     * @return type BankCardUpdate object
     */
    public function getBankCardUpdate() {
        return $this->io_bank_card_update;
    }
    
    /**
     * sets the BankCardUpdate object
     * @param type $vo_bank_card_update the BankCardUpdate
     */
    public function setBankCardUpdate( $vo_bank_card_update ) {
        $this->io_bank_card_update = $vo_bank_card_update;
    }
    
    /**
     * returns an array of error messages
     * @return array of the error messages
     */
    public function getMessages() {
        return $this->io_messages;
    }
    
    /**
     * adds a message to the message array
     * @param type $vs_error message to be added to the messages array
     */
    public function addMessage( $vs_error ) {
        array_push( $this->io_messages, $vs_error );
    }
    
    /**
     * builds a BankCardUpdateRequest object from the passed in JSON object
     * @param type $vo_data the JSON object
     * @return \BankCardUpdateRequest a BankCardUpdateRequest object
     */
    static function buildFromJSON( $vo_data ) {

        $o_instance = new BankCardUpdateRequest();
        
        if(array_key_exists( 'bank_card_update_request_id' , $vo_data)) {  $o_instance->setUpdateRequestID( $vo_data['bank_card_update_request_id'] ); }
        
        if(array_key_exists( 'bank_card_update_request_status', $vo_data) ) {
            
            if( gettype( $vo_data['bank_card_update_request_status'] ) === "array" ){
                
                $o_status = (array)$vo_data['bank_card_update_request_status'];
                
                if( array_key_exists( 'bank_card_update_request_status_name', $o_status ) ){
                    
                    $o_instance->setStatus( $o_status['bank_card_update_request_status_name'] );
                }
                else{
                    $o_instance->setStatus( $vo_data[ 'bank_card_update_request_status'] );
                }
            }
        }

        if(array_key_exists( 'bank_card_update', $vo_data) ) { $o_instance->setBankCardUpdate( BankCardUpdate::buildFromJSON( $vo_data['bank_card_update'] ) ); }

        return $o_instance;
        
    }
    
    /**
     * returns a JSON representation of the BankCardUpdateRequest
     * @return type JSON object
     */
    public function getJSON() {
        
        $o_array = array();
        
         if ($this->in_update_request_id!=0) {
             $o_array['bank_card_update_request_id'] = $this->in_update_request_id;
        }
        
        if( !is_null( $this->is_status ) ) {
            $o_array['bank_card_update_request_status'] = $this->is_status;
        }
        if( !is_null( $this->io_bank_card_update ) ) {
            $o_array['bank_card_update'] = $this->io_bank_card_update->getJSON();
        }
        
        if( !is_null( $this->io_messages ) ) {
            $o_array[ 'messages' ]  =  $this->io_messages;         
        }
        
        return json_encode( $o_array );
        
    }
    
    
}
