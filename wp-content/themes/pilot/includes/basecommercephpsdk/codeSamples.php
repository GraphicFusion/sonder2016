<?php

include_once 'BankCardTransaction.php';
include_once 'BankAccountTransaction.php';
include_once 'BaseCommerceClient.php';
include_once 'BankCard.php';
include_once 'BankAccount.php';
include_once 'PushNotification.php';
include_once 'JSONPushNotification.php';
include_once 'MerchantApplication.php';
include_once 'RecurringTransaction.php';
include_once 'BankCardUpdate.php';
include_once 'BankCardUpdateRequest.php';
include_once 'SettlementBatch.php';
include_once 'SplitFunding.php';
include_once 'CryptInfo.php';

class codeSamples {

    public $s_username = "0000026";
    public $s_password = "JPUbJjtDTvPdn6sNeXQA";
    public $s_key = "51B0945DE3A80E3E157FFB166494DA73F132BF1F68BA5B7C";

    
    /**
     * Test method for getBankAccountTransactionByMerchantTransactionID.
     * @throws BaseCommerceClientException
     */
    public function getBankAccountTransactionByMerchantTransactionID() {
        
        $o_bcpc = new BaseCommerceClient( $this->s_username, $this->s_password, $this->s_key );
        $o_bcpc->setSandbox( true );
        
        $o_merchant_transaction_id = "1";
        $o_bat = new BankAccountTransaction();
        $o_bat_list = $o_bcpc->getBankAccountTransactionByMerchantTransactionID($o_merchant_transaction_id);
        
        foreach ($o_bat_list as $o_bat) {
            var_dump($o_bat->getMessages());
            var_dump($o_bat->getBankAccountTransactionId());
        }
    }
    
    
    
    /**
     * Test method for getBankCardTransactionByMerchantTransactionID.
     * @throws BaseCommerceClientException
     */
    public function getBankCardTransactionByMerchantTransactionID() {
        $o_bcpc = new BaseCommerceClient( $this->s_username, $this->s_password, $this->s_key );
        $o_bcpc->setSandbox( true );
        
        $o_merchant_transaction_id = "3";
        $o_bat = new BankCardTransaction();
        $o_bct_list = $o_bcpc->getBankCardTransactionByMerchantTransactionID($o_merchant_transaction_id);
        
       // var_dump($o_bct_list);
                     
       foreach ($o_bct_list as $o_bat) {
           
           foreach( $o_bat->getMessages() as $val )
           {
               if($val!=null)
                    var_dump($val);
           }
           
        }
        
       
    }
    
    
    
    /**
     * Test method for manual batch close.
     * @throws BaseCommerceClientException
     */
    public function testCreateBankCardTransactionSettlementBatch() {

        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $o_settlement_batch = new SettlementBatch();
        $o_settlement_batch = $o_bcpc->createBankCardTransactionSettlementBatch();

        var_dump($o_settlement_batch->getBCTSaleAmount());
        var_dump($o_settlement_batch->getBCTCreditCount());
        var_dump($o_settlement_batch->getBCTSaleCount());
    }

    // Cancle a recurring transaction
    public function cancelRecurringTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $o_returned_rt = $o_bcpc->cancelRecurringTransaction(3);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getMessages());
    }

    public function processRecurringTransaction($vs_account_type, $vs_frequency, $vs_start_date, $vs_end_date, $vb_new_acccount) {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$$vs_frequency);
        $o_recurring_transaction->setStartDate(( new DateTime($vs_start_date))->format('m/d/Y'));
        if ($vs_end_date != null) {
            $o_recurring_transaction->setEndDate(( new DateTime($vs_end_date))->format('m/d/Y'));
        }
        $o_recurring_transaction->setAmount(69.00);

        // Create a BankCard and add it to the recurring transaction
        //Create a new BankCard object
        if ($vs_account_type == 'card') {
            $o_bc = new BankCard();

            if ($vb_new_acccount == false) {
                $o_bc->setToken("089dd53b3840ca76d0e4841bcf5febb504276462c72a8f835e6635939388ddd9");
            } else if ($vb_new_acccount == true) {
                $o_address = new Address();
                $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
                $o_address->setLine1("123 Some Address");
                $o_address->setCity("Tempe");
                $o_address->setState("AZ");
                $o_address->setZipcode("12345");

                //Create a new BankCard object
                $o_bc = new BankCard();
                $o_bc->setBillingAddress($o_address);
                $o_bc->setExpirationMonth("02");
                $o_bc->setExpirationYear("2017");
                $o_bc->setName("John Smith");
                $o_bc->setNumber("4111111111111111");
            }

            $o_recurring_transaction->setBankCard($o_bc);
        } else if ($vs_account_type == 'account') {

            $o_recurring_transaction->setMethod(BankAccountTransaction::$XS_BAT_METHOD_CCD);
            $o_recurring_transaction->setType(BankAccountTransaction::$XS_BAT_TYPE_DEBIT);
            $o_ba = new BankAccount();

            if ($vb_new_acccount == false) {
                $o_ba->setToken("0b602a0aceec1058fa9011087fa765be4d5fc3a299dc4b45e0fa2a251b93f830");
            } else if ($vb_new_acccount == true) {
                $o_address = new Address();
                $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
                $o_address->setLine1("123 Some Address");
                $o_address->setCity("Tempe");
                $o_address->setState("AZ");
                $o_address->setZipcode("12345");

                //Create a new BankCard object
                $o_ba = new BankAccount();
                $o_ba->setBillingAddress($o_address);
                $o_ba->setName("Bradly Rosno");
                $o_ba->setAccountNumber("123123123");
                $o_ba->setAccountType(BankAccount::$XS_BA_TYPE_CHECKING);
                $o_ba->setRoutingNumber("111000025");
            }

            $o_recurring_transaction->setBankAccount($o_ba);
        }
        var_dump($o_recurring_transaction->getJSON());
        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getMessages());
        if ($vs_account_type == 'card')
            var_dump($o_returned_rt->getBankCard()->getToken());
        else if ($vs_account_type == 'account')
            var_dump($o_returned_rt->getBankAccount()->getToken());
    }

    // Add a recurring transaction
    public function processRecurringTransactionAnnuallyWithBankCardTokenAndEndDate() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$XS_FREQUENCY_ANNUALLY);
        $o_recurring_transaction->setStartDate(( new DateTime('6/16/2015'))->format('m/d/Y'));
        $o_recurring_transaction->setEndDate(( new DateTime('6/16/2016'))->format('m/d/Y'));
        $o_recurring_transaction->setAmount(98.00);

        // Create a BankCard and add it to the recurring transaction
        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setToken("71fc638e3acaeceec6f059744a3f5039729a1332416ced2d1c408f43d7f56c9c");

        $o_recurring_transaction->setBankCard($o_bc);

        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getBankCard()->getToken());
        var_dump($o_returned_rt->getMessages());
    }

    // Add a recurring transaction
    public function processRecurringTransactionAnnuallyWithNewBankCardAndEndDate() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$XS_FREQUENCY_ANNUALLY);
        $o_recurring_transaction->setStartDate(( new DateTime('6/16/2015'))->format('m/d/Y'));
        $o_recurring_transaction->setEndDate(( new DateTime('6/16/2016'))->format('m/d/Y'));
        $o_recurring_transaction->setAmount(99.00);

        // Create a BankCard and add it to the recurring transaction
        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("02");
        $o_bc->setExpirationYear("2017");
        $o_bc->setName("John Smith");
        $o_bc->setNumber("4111111111111111");

        $o_recurring_transaction->setBankCard($o_bc);

        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getBankCard()->getToken());
        var_dump($o_returned_rt->getMessages());
    }

    // Add a recurring transaction
    public function processRecurringTransactionAnnuallyWithNewBankCardAndNoEndDate() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$XS_FREQUENCY_ANNUALLY);
        $o_recurring_transaction->setStartDate(( new DateTime('6/15/2015'))->format('m/d/Y'));
//        $o_recurring_transaction->setEndDate( date('m/d/Y') ); // If not set, recurring transaction is considered continuous ( runs forever )
        $o_recurring_transaction->setAmount(100.00);

        // Create a BankCard and add it to the recurring transaction
        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("02");
        $o_bc->setExpirationYear("2017");
        $o_bc->setName("John Smith");
        $o_bc->setNumber("4111111111111111");

        $o_recurring_transaction->setBankCard($o_bc);

        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getBankCard()->getToken());
        var_dump($o_returned_rt->getMessages());
    }

    // Add a recurring transaction
    public function processRecurringTransactionQuarterlyWithNewBankCardAndNoEndDate() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$XS_FREQUENCY_QUARTERLY);
        $o_recurring_transaction->setStartDate(( new DateTime('6/15/2015'))->format('m/d/Y'));
//        $o_recurring_transaction->setEndDate( date('m/d/Y') ); // If not set, recurring transaction is considered continuous ( runs forever )
        $o_recurring_transaction->setAmount(100.00);

        // Create a BankCard and add it to the recurring transaction
        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("02");
        $o_bc->setExpirationYear("2017");
        $o_bc->setName("John Smith");
        $o_bc->setNumber("4111111111111111");

        $o_recurring_transaction->setBankCard($o_bc);

        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getBankCard()->getToken());
        var_dump($o_returned_rt->getMessages());
    }

    // Add a recurring transaction
    public function processRecurringTransactionMonthlyWithNewBankCardAndNoEndDate() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$XS_FREQUENCY_MONTHLY);
        $o_recurring_transaction->setStartDate(( new DateTime('6/15/2015'))->format('m/d/Y'));
//        $o_recurring_transaction->setEndDate( date('m/d/Y') ); // If not set, recurring transaction is considered continuous ( runs forever )
        $o_recurring_transaction->setAmount(100.00);

        // Create a BankCard and add it to the recurring transaction
        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("02");
        $o_bc->setExpirationYear("2017");
        $o_bc->setName("John Smith");
        $o_bc->setNumber("4111111111111111");

        $o_recurring_transaction->setBankCard($o_bc);

        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getBankCard()->getToken());
        var_dump($o_returned_rt->getMessages());
    }

    // Add a recurring transaction
    public function processRecurringTransactionBiweeklyWithNewBankCardAndNoEndDate() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$XS_FREQUENCY_BIWEEKLY);
        $o_recurring_transaction->setStartDate(( new DateTime('6/15/2015'))->format('m/d/Y'));
//        $o_recurring_transaction->setEndDate( date('m/d/Y') ); // If not set, recurring transaction is considered continuous ( runs forever )
        $o_recurring_transaction->setAmount(100.00);

        // Create a BankCard and add it to the recurring transaction
        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("02");
        $o_bc->setExpirationYear("2017");
        $o_bc->setName("John Smith");
        $o_bc->setNumber("4111111111111111");

        $o_recurring_transaction->setBankCard($o_bc);

        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getBankCard()->getToken());
        var_dump($o_returned_rt->getMessages());
    }

    // Add a recurring transaction
    public function processRecurringTransactionWeeklyWithNewBankCardAndNoEndDate() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$XS_FREQUENCY_WEEKLY);
        $o_recurring_transaction->setStartDate(( new DateTime('6/15/2015'))->format('m/d/Y'));
//        $o_recurring_transaction->setEndDate( date('m/d/Y') ); // If not set, recurring transaction is considered continuous ( runs forever )
        $o_recurring_transaction->setAmount(100.00);

        // Create a BankCard and add it to the recurring transaction
        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("02");
        $o_bc->setExpirationYear("2017");
        $o_bc->setName("John Smith");
        $o_bc->setNumber("4111111111111111");

        $o_recurring_transaction->setBankCard($o_bc);

        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getBankCard()->getToken());
        var_dump($o_returned_rt->getMessages());
    }

    // Add a recurring transaction
    public function processRecurringTransactionDailyWithNewBankCardAndNoEndDate() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$XS_FREQUENCY_DAILY);
        $o_recurring_transaction->setStartDate(( new DateTime('6/15/2015'))->format('m/d/Y'));
//        $o_recurring_transaction->setEndDate( date('m/d/Y') ); // If not set, recurring transaction is considered continuous ( runs forever )
        $o_recurring_transaction->setAmount(100.00);

        // Create a BankCard and add it to the recurring transaction
        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("02");
        $o_bc->setExpirationYear("2017");
        $o_bc->setName("John Smith");
        $o_bc->setNumber("4111111111111111");

        $o_recurring_transaction->setBankCard($o_bc);

        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getBankCard()->getToken());
        var_dump($o_returned_rt->getMessages());
    }

    //Process Bank Card Transaction
    public function processBankCardTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //create a new BankCardTransaction object and add all values to it where the type will be of type XS_SALE
        $o_bct = new BankCardTransaction();
        $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_SALE);
        $o_bct->setCardName("John Doe");
        $o_bct->setCardNumber("4111111111111111");
        $o_bct->setCardExpirationMonth("09");
        $o_bct->setCardExpirationYear("2018");
        $o_bct->setCardCVV2("800");
        $o_bct->setAmount(1.33);

        //create billing Address object to set on the BankCardTransaction
        $o_billing_address = new Address();
        $o_billing_address->setCountry("US");
        $o_billing_address->setCity("Scottsdale");
        $o_billing_address->setLine1("111 East 2nd Street");
        $o_billing_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_billing_address->setState("AZ");
        $o_billing_address->setZipcode("85254");

        $o_bct->setBillingAddress($o_billing_address);

        //Actually process the BankCardTransaction        
        $o_bct = $o_bcpc->processBankCardTransaction($o_bct);

        //check status after the transaction is processed 
        if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_FAILED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bct->getMessages());
        } else if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_DECLINED)) {
            //Transaction Declined, look at response code and response message
            var_dump($o_bct->getResponseCode());
            var_dump($o_bct->getResponseMessage());
        } else if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_CAPTURED)) {
            //Transaction went through successfully
            var_dump($o_bct->getTransactionID());
        } else {
            var_dump($o_bct->getStatus());
            var_dump("what");
        }
    }

    public function setPushNotificationURL() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        var_dump($o_bcpc->setPushNotificationURL("www.rob.com"));
    }

    //Process a Void Transaction
    public function voidBankCardTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //create a new BankCardTransaction object and add all values to it, set the type to XS_VOID
        $o_bct = new BankCardTransaction();
        $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_VOID);
        //set the transaction ID on the BankCardTransaction object to the old BankCardTransaction that you want voided
        $o_bct->setTransactionID(943);

        //Actually process the BankCardTransaction
        $o_bct = $o_bcpc->processBankCardTransaction($o_bct);

        //check status after the transaction is processed 
        if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_FAILED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bct->getMessages());
        } else if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_VOIDED)) {
            //Transaction Successful   
            var_dump($o_bct);
        }
    }

    //Process a Refund Transaction
    public function refundBankCardTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //create a new BankCardTransaction object and add all values to it, set the type to XS_REFUND
        $o_bct = new BankCardTransaction();
        $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_REFUND);
        //set the transaction ID on the BankCardTransaction object to the old BankCardTransaction that you want refunded
        $o_bct->setTransactionID(906);
        //set the amount you want refunded which cannot exceed the original transactions amount
        $o_bct->setAmount(1.11);

        //Actually process the BankCardTransaction
        $o_bct = $o_bcpc->processBankCardTransaction($o_bct);

        //check status after the transaction is processed 
        if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_FAILED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bct->getMessages());
        } else if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_DECLINED)) {
            //Transaction Declined, look at response code and response message
            var_dump($o_bct->getResponseCode());
            var_dump($o_bct->getResponseMessage());
        } else if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_CAPTURED)) {
            //Transaction went through successfully
            var_dump($o_bct->getTransactionID());
        }
    }

    //Process an ACH Transaction
    public function processACHTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //create a new BankAccountTransaction object
        $o_bat = new BankAccountTransaction();
        $o_bat->setAccountName("John Doe");
        $o_bat->setRoutingNumber("111000025");
        $o_bat->setAccountNumber("12312312312");
        $o_bat->setAccountType(BankAccountTransaction::$XS_BAT_ACCOUNT_TYPE_CHECKING);
        $o_bat->setMethod(BankAccountTransaction::$XS_BAT_METHOD_TEL);
        $o_bat->setType(BankAccountTransaction::$XS_BAT_TYPE_DEBIT);
        $o_bat->setEffectiveDate(date('m/d/Y H:i:s'));
        var_dump($o_bat->getEffectiveDate());

        $o_bat->setRoutingNumber( "253170376" );
        $o_bat->setAccountNumber( "12312312312" );        
        $o_bat->setAccountType( BankAccountTransaction::$XS_BAT_ACCOUNT_TYPE_CHECKING );
        $o_bat->setMethod( BankAccountTransaction::$XS_BAT_METHOD_TEL );
        $o_bat->setType( BankAccountTransaction::$XS_BAT_TYPE_DEBIT );
        $o_bat->setEffectiveDate( date('m/d/Y H:i:s') );
  //      var_dump( $o_bat->getEffectiveDate());
//        $o_bat->setEffectiveDate( date('m-d-Y H:i:s') );
//        $o_bat->setCheckNumber(231456);
//        var_dump( $o_bat->getEffectiveDate());
//        var_dump( date('m-d-Y H:i:s'));
        $o_bat->setAmount(1.55);

//        //actually process the BankAccountTransaction
        $o_bat = $o_bcpc->processBankAccountTransaction($o_bat);

        if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_FAILED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bat->getMessages());
        } else if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_CREATED)) {
            //Transaction went through successfully
            var_dump($o_bat->getBankAccountTransactionId());
        }
        var_dump($o_bat->getEffectiveDate());
    }

    //add a bank card to the vault
    public function addBankCardToVault() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("02");
        $o_bc->setExpirationYear("2015");
        $o_bc->setName("John Smith");
        $o_bc->setNumber("4111111111111111");

        //add the bank card to the vault
        $o_bc = $o_bcpc->addBankCard($o_bc);
        var_dump($o_bc);
        if ($o_bc->isStatus(BankCard::$XS_BC_STATUS_FAILED)) {
            //the add Failed, look at messages array for reason(s) why
            var_dump($o_bc->getMessages());
        } else if ($o_bc->isStatus(BankCard::$XS_BC_STATUS_ACTIVE)) {
            //Card added successfully
            var_dump($o_bc->getToken());
        }
    }

    //add a bank account to the vault
    public function addBankAccountToVault() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //Create the BankAccount object and set the values
        $o_bank_account = new BankAccount();
        $o_bank_account->setName("John Doe");
        $o_bank_account->setAlias("Johns Savings account");
        $o_bank_account->setAccountNumber("1111111119");
        $o_bank_account->setRoutingNumber("011000028");
        $o_bank_account->setAccountType(BankAccount::$XS_BA_TYPE_SAVINGS);

        //Actually add the BankAccount to the vault
        $o_bank_account = $o_bcpc->addBankAccount($o_bank_account);
        var_dump($o_bank_account);
        if ($o_bank_account->isStatus(BankAccount::$XS_BA_STATUS_FAILED)) {
            //the add Failed, look at messages array for reason(s) why
            var_dump($o_bank_account->getMessages());
        } else if ($o_bank_account->isStatus(BankAccount::$XS_BA_STATUS_ACTIVE)) {
            //Account added successfully
            var_dump($o_bank_account->getToken());
        }
    }
    
       //add a bank account with MICR data
    public function addBankAccountwithMICRData() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //Create the BankAccount object and set the values
        $o_bank_account = new BankAccount();
        $o_bank_account->setName("John Doe");
        $o_bank_account->setAlias("Johns Savings account");
        $o_bank_account->setMICRData("U2317555995UT011000015T"); 
        $o_bank_account->setAccountType(BankAccount::$XS_BA_TYPE_SAVINGS);

        //Actually add the BankAccount to the vault
        $o_bank_account = $o_bcpc->addBankAccount($o_bank_account);
        var_dump($o_bank_account);
        if ($o_bank_account->isStatus(BankAccount::$XS_BA_STATUS_FAILED)) {
            //the add Failed, look at messages array for reason(s) why
            var_dump($o_bank_account->getMessages());
        } else if ($o_bank_account->isStatus(BankAccount::$XS_BA_STATUS_ACTIVE)) {
            //Account added successfully
            var_dump($o_bank_account->getToken());
        }
    }

    //process bank card transaction using vault record
    public function processBankCardVaultRecord() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //Create the BankCardTransction then set the token on it as the token from the BankCard in the vault's token
        $o_bct = new BankCardTransaction();
        $o_bct->setToken("570fec770ea150119f9d01eab461b9e570a7725688039e1f08d2703033358157");
        $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_SALE);
        $o_bct->setAmount(1.15);
        //Then actually process the BankCard
        $o_bct = $o_bcpc->processBankCardTransaction($o_bct);

        //check status after the transaction is processed 
        if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_FAILED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bct->getMessages());
        } else if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_DECLINED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bct->getMessages());
        } else if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_CAPTURED)) {
            //Transaction went through successfully
            var_dump($o_bct->getTransactionID());
        }
    }

    //process bank account transaction using vault record
    public function processBankAccountVaultRecord() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //Create a BankAccountTransaction object and set the token on it as the token of the account from the vault
        $o_bat = new BankAccountTransaction();
        $o_bat->setMethod(BankAccountTransaction::$XS_BAT_METHOD_CCD);
        $o_bat->setType(BankAccountTransaction::$XS_BAT_TYPE_DEBIT);
        $o_bat->setEffectiveDate(date('m/d/Y H:i:s'));
        $o_bat->setAmount(2.22);
        $o_bat->setToken("dcb8be979c8a887f531206502c13f0eb40265ec4aeec1b4242c04c19f2b3af78");

        //actually process the BankAccountTransaction
        $o_bat = $o_bcpc->processBankAccountTransaction($o_bat);

        if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_FAILED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bat->getMessages());
        } else if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_CREATED)) {
            //Transaction went through successfully
            var_dump($o_bat->getBankAccountTransactionId());
        }
    }

    //get a previous Bank Card transaction
    public function getBankCardTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //Create the BankCardTransaction object
        $o_bct = new BankCardTransaction();

        //Call a getBankCardTransaction and pass it the transaction ID of the BankCardTransaction you want returned
        $o_bct = $o_bcpc->getBankCardTransaction(1);

        if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_FAILED)) {
            // Transaction failed, look at messages for reasons why
            var_dump("1");
            var_dump($o_bct->getMessages());
        } else {
            // Transaction retrieved, look at status of the transaction 
            var_dump("hello");
            var_dump($o_bct->getStatus());
        }
    }

    //get a previous bank account transaction 
    public function getBankAccountTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //Create the BankAccount Transaction object
        $o_bat = new BankAccountTransaction();

        //Call a getBankAccountTransaction and pass it the transaction ID of the BankAccountTransaction you want returned
        $o_bat = $o_bcpc->getBankAccountTransaction(457);

        if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_FAILED)) {
            // Transaction failed, look at messages for reasons why
            var_dump($o_bat->getMessages());
        } else {
            // Transaction retrieved, look at status of the transaction 
            var_dump($o_bat->getStatus());
            var_dump($o_bat->getEffectiveDate());
            var_dump($o_bat->getSettlementDate());
            var_dump($o_bat->getJSON());
        }
    }

    //Split Funding for BCT
    public function testBCTSplitFunding() {
        $o_bct = new BankCardTransaction();
        $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_SALE);
        $o_bct->setAmount(1);
        $o_bct->setCardName("Sample Bank Card Transaction");
        $o_bct->setCardNumber("4111111111111111");
        $o_bct->setCardExpirationMonth("08");
        $o_bct->setCardExpirationYear("2016");
        $o_bct->setMerchantTransactionID("557");

        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        $o_bct->setBillingAddress($o_address);


        $io_funding_instruction = array();
        $sf1 = new SplitFunding();
        $sf1->setBankAccountToken("7388430d262469efac9563027bb239edbf3c167a4a958ab128a53f9af3b185f7");
        $sf1->setAmount(10);

        $sf2 = new SplitFunding();
        $sf2->setBankAccountToken("7388430d262469efac9563027bb239edbf3c167a4a958ab128a53f9af3b185f7");
        $sf2->setAmount(20);

        $sf3 = new SplitFunding();
        $sf3->setBankAccountToken("7388430d262469efac9563027bb239edbf3c167a4a958ab128a53f9af3b185f7");
        $sf3->setAmount(30);

        $o_bct->addFundingInstruction($sf1);
        $o_bct->addFundingInstruction($sf2);
        $o_bct->addFundingInstruction($sf3);

        $o_client = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_client->setSandbox(true);

        $o_bct = $o_client->processBankCardTransaction($o_bct);
        
        
        foreach ($o_bct->getFundingInstruction() as $value){
            print($value->getAmount());
            print("\n");
            print($value->getBankAccountToken());
            print("\n");
            print($value->getType());
            print("\n");
        }

        if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_FAILED)) {
           var_dump($o_bct->getMessages());
        } else if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_DECLINED)) {
            var_dump($o_bct->getResponseMessage());
        } else if ($o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_CAPTURED)) {
            var_dump($o_bct->getTransactionID());
        }
        echo "card number =  " . $o_bct->getCardNumber();
    }

    //Split Funding for Bank Account Transaction
    public function testBATSplitFunding() {
        $o_bat = new BankAccountTransaction();
        $o_bat->setAccountName("John Doe");
        $o_bat->setRoutingNumber("111000025");
        $o_bat->setAccountNumber("12312312312");
        $o_bat->setAccountType(BankAccountTransaction::$XS_BAT_ACCOUNT_TYPE_CHECKING);
        $o_bat->setMethod(BankAccountTransaction::$XS_BAT_METHOD_TEL);
        $o_bat->setType(BankAccountTransaction::$XS_BAT_TYPE_DEBIT);
        $o_bat->setEffectiveDate(date('m/d/Y H:i:s'));
        $o_bat->setMerchantTransactionID("557");
        $o_bat->setAmount(11.55);

        $sf1 = new SplitFunding();
        $sf1->setBankAccountToken("7388430d262469efac9563027bb239edbf3c167a4a958ab128a53f9af3b185f7");
        $sf1->setAmount(10);

        $sf2 = new SplitFunding();
        $sf2->setBankAccountToken("7388430d262469efac9563027bb239edbf3c167a4a958ab128a53f9af3b185f7");
        $sf2->setAmount(2.78787);

        $sf3 = new SplitFunding();
        $sf3->setBankAccountToken("7388430d262469efac9563027bb239edbf3c167a4a958ab128a53f9af3b185f7");
        $sf3->setAmount(3.778789);

        $o_bat->addFundingInstruction($sf1);
        $o_bat->addFundingInstruction($sf2);
        $o_bat->addFundingInstruction($sf3);



        $o_client = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_client->setSandbox(true);

        $o_bat = $o_client->processBankAccountTransaction($o_bat);

        foreach ($o_bat->getFundingInstruction() as $value){
            print($value->getAmount());
            print("\n");
            print($value->getBankAccountToken());
            print("\n");
            print($value->getType());
            print("\n");
        }
        
        
        if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_FAILED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bat->getMessages());
        } else if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_CREATED)) {
            //Transaction went through successfully
            var_dump($o_bat->getBankAccountTransactionId());
        }
        echo "Account number =  " . $o_bat->getAccountNumber();
    }

    //reverse an ACH Transaction
    public function reverseACHTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //create a new BankAccountTransaction object to be reversed
        $o_bat = new BankAccountTransaction();
        $o_bat->setBankAccountTransactionId(146);
        $o_bat->setType(BankAccountTransaction::$XS_BAT_TYPE_REVERSAL);

        //actually process the BankAccountTransaction
        $o_bat = $o_bcpc->processBankAccountTransaction($o_bat);

        if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_FAILED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bat->getMessages());
        } else if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_CREATED)) {
            //Transaction went through successfully
            var_dump($o_bat->getBankAccountTransactionId());
        }
    }

    //cancel an ACH Transaction
    public function cancelACHTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //create a new BankAccountTransaction object
        $o_bat = new BankAccountTransaction();
        $o_bat->setBankAccountTransactionId(1121);
        $o_bat->setType(BankAccountTransaction::$XS_BAT_TYPE_CANCEL);

        //actually process the BankAccountTransaction
        $o_bat = $o_bcpc->processBankAccountTransaction($o_bat);

        if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_FAILED)) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump($o_bat->getMessages());
        } else if ($o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_CANCELED)) {
            //Transaction was successfully canceled
            var_dump($o_bat->getBankAccountTransactionId());
        }
    }

    public function testPingPong() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $o_bcpc->sendPing();

        echo $o_bcpc->getLastSessionId();
    }

    public function testPingPushNotification() {


        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);


        $o_pn = new PushNotification();
        $o_pn2 = new PushNotification();

        $o_pn->setPING();

        var_dump($o_pn->getJSON());
        var_dump($o_pn2->getJSON());

        $o_pn2 = PushNotification::buildFromJSON($o_pn->getJSON());

        var_dump($o_pn2->getJSON());
    }

    public function testJSONPushNotification() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);


        $triple_des = new TripleDESService($this->s_key);

        $o_pn = new PushNotification();
        $o_pn2 = new JSONPushNotification();
//        $o_pn3 = new JSONPushNotification();
        $o_pn4 = new JSONPushNotification();

        $o_array = array();
        $o_array["test"] = "testyuh";
        $o_array["test2"] = "testyuh2";

        $o_array2 = array();
        $o_array2["test"] = "testyuh";
        $o_array2["test2"] = "testyuh2";
//        $o_array2["push_notification_type"] = "JSON";
//        var_dump( $o_pn4->isNotificationType( PushNotification::$XS_PN_TYPE_JSON) );
//        var_dump( $o_pn4->getNotificationType() );
//        var_dump( $o_pn4->getJSON() );
//        var_dump( $o_pn->getJSON() );
//        $o_pn = PushNotification::buildFromJSON( $o_array2 );
//        var_dump( $o_pn );
//        $o_pn2 = PushNotification::buildFromJSON( $o_array );
//        var_dump( $o_pn2 );
//        $o_pn2 = JSONPushNotification::buildFromJSON( $o_array );
//        var_dump( $o_pn2 );

        $o_pn4 = JSONPushNotification::buildFromJSON($o_array);
        var_dump($o_pn4->isNotificationType(PushNotification::$XS_PN_TYPE_JSON));
        var_dump($o_pn4->getJSON());
        $o_pn3 = $o_bcpc->handlePushNotification($triple_des->encrypt(json_encode($o_pn4->getJSON())));
        var_dump($o_pn3);
        var_dump($o_pn3->getJSON());
        var_dump($o_pn3->getNotificationType());
    }

    public function processRequest() {

        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $o_json = array();
        $o_json['request_type'] = "sometype";
        $o_json['another_field'] = "another field";

        var_dump($o_bcpc->processRequest($o_json));
    }

    //update a bank card to the vault
    //Added by Dipanjan Ghosh (dipanjan.ghosh@basecommerce.com)
    public function updateBankCardToVault() {

        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key, NULL);
        $o_bcpc->setSandbox(true);

        //create the billing Address object

        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("Indralok");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");


        //Create a new BankCard object

        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("09");
        $o_bc->setExpirationYear("2019");

        //$o_bc->setNumber("2222222222");

        $o_bc->setToken("fa788e9f22cf0b439f40383a322efc3682def0b17896b1aa9425c88efbf82379");

        //update the bank card to the vault
        $o_bc = $o_bcpc->updateBankCard($o_bc);

        if ($o_bc->isStatus(BankCard::$XS_BC_STATUS_FAILED)) {
            //the update Failed, look at messages array for reason(s) 
            var_dump($o_bc->getMessages());
        } else if ($o_bc->isStatus(BankCard::$XS_BC_STATUS_ACTIVE)) {
            //Card updated successfully
            var_dump($o_bc->getToken());
        } else {
            var_dump($o_bc->getMessages());
        }
    }

    /**
     * test function to show push notification id is working
     */
    public function testPushNotificationID() {

        $o_pn = new PushNotification();
        $o_pn->setID(33);
        $o_pn->setPING();

        var_dump(PushNotification::buildFromJSON($o_pn->getJSON())->getJSON());
    }

    public function testBankCardUpdate() {

        $o_bcu = new BankCardUpdate();
        $o_bcu->setOriginalNumber("4111111111111111");
        $o_bcu->setOriginalToken("ljnlnlkdmslkfsadlkf");
        $o_bcu->setOriginalExpirationMonth("04");
        $o_bcu->setOriginalExpirationYear("2015");
        $o_bcu->setUpdatedNumber("5555555555555555");
        $o_bcu->setUpdatedExpirationMonth("05");
        $o_bcu->setUpdatedExpirationYear("2016");
        $o_bcu->setStatus(BankCardUpdate::$XS_REQUEST_STATUS_NEW);
        $o_bcu->setCreationDate(date('m/d/Y H:i:s'));
        $o_bcu->setUpdatedDate(date('m/d/Y H:i:s'));
        $o_bcu->setLatestRequestDate(date('m/d/Y H:i:s'));

        var_dump($o_bcu->getJSON());


        $o_bcu2 = new BankCardUpdate();

        $o_bcu2 = BankCardUpdate::buildFromJSON($o_bcu->getJSON());

        var_dump($o_bcu2);
    }

    public function testBankCardUpdateRequest() {

        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);


        $o_bcu = new BankCardUpdate();
//        $o_bcu->setOriginalNumber("4111111111111111");
        $o_bcu->setOriginalToken("78dc7478b902492204374dfe88734da1d27133bde215b23d3ea8dce58a862ad1");
//        $o_bcu->setOriginalExpirationMonth("04");
//        $o_bcu->setOriginalExpirationYear("2015");
//        $o_bcu->setUpdatedNumber("5555555555555555");
//        $o_bcu->setUpdatedExpirationMonth("05");
//        $o_bcu->setUpdatedExpirationYear("2016");
//        $o_bcu->setStatus( BankCardUpdate::$XS_REQUEST_STATUS_NEW );
//        $o_bcu->setCreationDate( date('m/d/Y H:i:s') );
//        $o_bcu->setUpdatedDate( date('m/d/Y H:i:s') );
//        $o_bcu->setLatestRequestDate( date('m/d/Y H:i:s') );


        $o_bcur = new BankCardUpdateRequest();

        $o_bcur->setStatus(BankCardUpdateRequest::$XS_REQUEST_STATUS_PENDING);
        $o_bcur->setBankCardUpdate($o_bcu);
//        var_dump( $o_bcur->getBankCardUpdate() );
//        var_dump( $o_bcur->getJSON() );
//        echo "\n ------------------------------------------------------------------------------------------------------ \n";
        $o_bcur = $o_bcpc->processBankCardUpdateRequest($o_bcur);
        var_dump($o_bcur->getJSON());
//        echo "\n ------------------------------------------------------------------------------------------------------ \n \n";
//        var_dump( $o_bcur->isStatus( BankCardUpdateRequest::$XS_REQUEST_STATUS_PENDING ) );
//        var_dump( $o_bcur->isStatus( BankCardUpdateRequest::$XS_REQUEST_STATUS_FAILED ) );
//        var_dump( $o_bcur->getJSON() );
//        
//        $o_bcur2 = new BankCardUpdateRequest();
//        var_dump( $o_bcur2->getJSON() );
//        $o_bcur2 = BankCardUpdateRequest::buildFromJSON(  $o_bcur->getJSON() );
//        echo "\n\n code samples: \n\n";
//        var_dump( $o_bcur2->getJSON() );
//        echo "\n\n\n o_bcur2 $o_bcur2-> \n";
    }

    /**
     * deletes a bank card based off the bank cards token
     */
    public function deleteBankCard() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $s_token = "519ac989649c726371125c1383a0bde7fdbcb55850caac50acbc75db4149f695";

        $o_bc = $o_bcpc->deleteBankCard($s_token);

        if ($o_bc->isStatus(BankCard::$XS_BC_STATUS_DELETED)) {
            var_dump("it worked");
        } else {
            var_dump($o_bc->getMessages());
        }
    }

    /**
     * deletes a bank account based off the bank accounts token
     */
    public function deleteBankAccount() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $s_token = "842aae87c2aa9f40aacfb7df9cc305d2d060d884aab4bc4839cc646c2f80ba78";

        $o_ba = $o_bcpc->deleteBankAccount($s_token);

        if ($o_ba->isStatus(BankAccount::$XS_BA_STATUS_DELETED)) {
            var_dump("it worked");
        } else {
            var_dump($o_ba->getMessages());
        }
    }

    function testMerchApp() {

        $o_client = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_client->setSandbox(true);

        $o_merch_app = new MerchantApplication();

        $o_merch_app->setApiVersion("1.2");

        $o_account = new Account();
        $o_account->setAccountName("TEST Account");
        $o_account->setTestAccount(True);
        $o_account->setAcceptACH(false);
        $o_account->setAcceptBC(True);

        $o_legal_address = new Address(Address::$XS_ADDRESS_NAME_LEGAL);

        $o_account->setLegalAddress($o_legal_address);

        $o_dba_address = new Address(Address::$XS_ADDRESS_NAME_DBA);
        $o_dba_address->setLine1("456 Maple Street");
        $o_dba_address->setCity("Toontown");
        $o_dba_address->setState("AZ");
        $o_dba_address->setZipcode("85284");
        $o_dba_address->setCountry("USA");

        $o_account->setAccountPhoneNumber("123-456-7891");
        $o_account->setReferralPartnerID("a0Fi0000006Om7a");
        $o_account->setCustomerServicePhoneNumber("981-124-6431");
        $o_account->setDBA("ACME Inc");
        $o_account->setEntityType(Account::$XS_ENTITY_TYPE_CORP);
        $o_account->setAssociationNumber("1111");
        $o_account->setCongaTemplateId("1111");
        $o_account->setEIN("123456789");
        $o_account->setIpAddressOfAppSubmission("192.168.0.1");
        $o_account->setWebsite("www.google.com");
        $o_account->setSettlementAccountBankName("test bank name");
        $o_account->setSettlementAccountBankPhone("123-123-1234");
        $o_account->setSettlementAccountName("test account name");
        $o_account->setSettlementAccountNumber("123456789");
        $o_account->setSettlementAccountRoutingNumber("000000000");
        $o_account->setSettlementSameAsFeeAccount(true);

        $o_merch_app->setAccount($o_account);

        $o_location = new Location();
        $o_location->setAlternativeFax("111-111-1111");
        $o_location->setAnnualRevenue(10000000);
        $o_location->setContactEmail("contact@gmail.com");
        $o_location->setContactMobile("222-222-2222");
        $o_location->setContactName("Roger Rabbit");
        $o_location->setContactSameAsOwner(false);
        $o_location->setDescription(Location::$XS_DESCRIPTION_CONSULTING);
        $o_location->setEntityStartDate(date('m/d/y h:i:s'));
        $o_location->setEntityState("AZ");
        $o_location->setFax("333-333-3333");
        $o_location->setIndustry(Location::$XS_INDUSTRY_CONSULTING);
        $o_location->setLengthOfCurrentOwnership("1");
        $o_location->setOrganizationMission("We take your business where it needs to go");
        $o_location->setOwnership(Location::$XS_OWNERSHIP_PRIVATE);
        $o_location->setQuantityOfLocation(1);
        $o_location->setSalesAgentName("John Doe");
        $o_location->setTaxExempt(false);
        $o_location->setTotalCustomers(1500);
        $o_location->setYearIncorporated("2013");
        $o_location->setYearsInBusiness(1);
        $o_location->setDBAAddress($o_dba_address);

        $o_contact = new PrincipalContact();
        $o_contact->setLastName("Rabbit");
        $o_contact->setFirstName("Roger");

        $o_mailing = new Address(Address::$XS_ADDRESS_NAME_MAILING);
        $o_mailing->setLine1("789 Maple Street");
        $o_mailing->setCity("Toontown");
        $o_mailing->setState("AZ");
        $o_mailing->setZipcode("85284");
        $o_mailing->setCountry("USA");

        $o_contact->setMailingAddress($o_mailing);
        $o_contact->setPhoneNumber("111-111-1111");
        $o_contact->setMobilePhoneNumber("222-222-2222");
        $o_contact->setEmail("test-princ-contact@test.com");
        $o_contact->setTitle("CEO");
        $o_contact->setBirthdate(date('m/d/y h:i:s'));
        $o_contact->setAuthorizedUser(True);
        $o_contact->setAccountSigner(True);
        $o_contact->setSSN("111-11-1111");
        $o_contact->setIsPrimary(True);
        $o_contact->setFax("111-111-1112");
        $o_contact->setOwnershipPercentage(100);
        $o_contact->setContactType(PrincipalContact::$XS_CONTACT_TYPE_OWNER);
        $o_contact->setDriverLicenseId("123 456 789");
        $o_contact->setLicenseState("AZ");
        $o_location->addPrincipalContact($o_contact);

        $o_ach_details = new ACHDetails();
        $o_ach_details->setAuthMethodConversionPercentage(100);
        $o_ach_details->setAverageTicketAmount(1);
        $o_ach_details->setChargebackFee(1);
        $o_ach_details->setCompanyNameDescriptor("comp name descriptor");
        $o_ach_details->setDescriptor("descript");
        $o_ach_details->setMaxDailyAmount(1);
        $o_ach_details->setMaxSingleTransactionAmount(1);
        $o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_BUSINESSES_ORGANIZATIONS);
        $o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_CONSUMER_INDIVIDUALS);
        $o_ach_details->setReturnFee(1);
        $o_ach_details->addSubmissionMethod(ACHDetails::$XS_SUBMISSION_METHOD_MANUALLY_VIA_VIRTUAL_TERMINAL);
        $o_ach_details->setTransactionFee(1);
        $o_ach_details->setUnauthReturn(1);
        $o_ach_details->setAverageMonthlyAmount(1);

        $o_location->setAchDetails($o_ach_details);

        $o_bc_details = new BankCardDetails();
        $o_bc_details->addCreditRequested(BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT);
        $o_bc_details->setAuthorizationFee(1);
        $o_bc_details->setAverageMonthlyVolume(1);
        $o_bc_details->setAverageTicket(1);
        $o_bc_details->setCardPresentPercentage(80);
        $o_bc_details->setInternetPercentage(10);
        $o_bc_details->setMailOrderPercentage(1);
        $o_bc_details->setTelephoneOrderPercentage(9);
        $o_bc_details->setMaxMonthlyVolume(1);
        $o_bc_details->setMaxTicket(1);
        $o_bc_details->setTransactionFee(1);
		
		$o_bc_details->setBundledSingleRate(5);
		$o_bc_details->setPTPDiscountPerItem(10);
		$o_bc_details->setAmexPTPDiscountPerItem(15);
		$o_bc_details->setEarlyTerminationFee(20);

        $o_location->setBankCardDetails($o_bc_details);

//        $o_merch_app->addLocation( $o_location );
        $o_merch_app->addLocation($o_location);
        try {
            $o_merch_applications = $o_client->submitApplication($o_merch_app);
//                        print_r($o_merch_applications);
            for ($n_index = 0, $n_size = count($o_merch_applications); $n_index < $n_size; $n_index++) {
                $o_merch_app = $o_merch_applications[$n_index];
                echo "\n\nSUCCESS";
                echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
                echo "\n\n";
                echo "\n\n o_merch_app id = " . $o_merch_app->getId();
                echo "\n\n";
                echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
                echo "\n\n";
                echo "\n\n o_merch_app salesforce ids = " . print_r($o_merch_app->getSalesforceID());
                echo "\n\n";
            }
        } catch (BaseCommerceClientException $e) {
            echo "ERROR";
            var_dump($e->getMessage());
            //var_dump($e->getTrace());
        }
    }

    /**
     * Update a bank account to the vault
     */
    public function updateBankAccountToVault() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        //Create the BankAccount object and set the values
        $o_bank_account = new BankAccount();

        $o_user_address = new Address();
        $o_user_address->setCity("Kolkata1");
        $o_user_address->setCountry("India");
        $o_user_address->setLine1("D4/18 Indralok Housing");
        $o_user_address->setLine2("Estate 2");
        $o_user_address->setZipcode("700002");
        $o_user_address->setName("Address_1");


        $o_bank_account->setBillingAddress($o_user_address);
        $o_bank_account->setRoutingNumber("1111111111111111");
        //$o_bank_account->setAccountNumber("1111111111111111");
        $o_bank_account->setToken("7de1edc61c0a37db89560835bf19352ce80e4c029271d6e8ed8f57fe1e614aeb");

        $o_bank_account->setAccountType(BankAccount::$XS_BA_TYPE_SAVINGS);

        // Update the BankAccount to the vault
        $o_bank_account = $o_bcpc->updateBankAccount($o_bank_account);
        var_dump($o_bank_account);
        if ($o_bank_account->isStatus(BankAccount::$XS_BA_STATUS_FAILED)) {
            //the add Failed, look at messages array for reason(s) why
            var_dump($o_bank_account->getMessages());
        } else if ($o_bank_account->isStatus(BankAccount::$XS_BA_STATUS_ACTIVE)) {
            //Account updated successfully
            var_dump($o_bank_account->getBillingAddress());
            var_dump($o_bank_account->getRoutingNumber());
            var_dump($o_bank_account->getAccountNumber());
        } else {
            var_dump($o_bank_account->getMessages());
        }
    }

    /**
     * Create a Merchant with AutoBoarding..
     */
    function testBothACHAndBankCardforAutoBoard() {

        // Add Partner User info.
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);


        $o_merch_app = new MerchantApplication();
        $o_merch_app->setApiVersion("1.2");
        $o_merch_app->setCode($this->s_username);
        $o_merch_app->setAutoBoard(true);

        // Create new account
        $o_account = new Account();
        $o_account->setAccountName("TEST Account -1");
        $o_account->setTestAccount(True);
        $o_account->setAcceptACH(True);
        $o_account->setAcceptBC(True);


        // Custom Fields
        $o_account->setCongaTemplateId("1111");
        $o_account->setReferralPartnerID("a0Fi0000006Om7a");
        $o_account->setIpAddressOfAppSubmission("192.168.0.2");


        // Legal Address
        $o_legal_address = new Address(Address::$XS_ADDRESS_NAME_LEGAL);
        $o_legal_address->setLine1("12345 Oak Street ok ok ");
        $o_legal_address->setCity("Toon town");
        $o_legal_address->setState("AZ");
        $o_legal_address->setZipcode("94404");
        $o_legal_address->setCountry("US");

        $o_account->setLegalAddress($o_legal_address);


        // DBA Address.
        $o_dba_address = new Address(Address::$XS_ADDRESS_NAME_DBA);
        $o_dba_address->setLine1("456 Maple Street");
        $o_dba_address->setCity("XYZ");
        $o_dba_address->setState("AZ");
        $o_dba_address->setZipcode("94404");
        $o_dba_address->setCountry("US");

        $o_account->setAccountPhoneNumber("123-456-7890");

        $o_account->setCustomerServicePhoneNumber("981-124-6431");
        $o_account->setDBA("ACME Inc");

        $o_account->setEntityType(Account::$XS_ENTITY_TYPE_CORP);
        $o_account->setAssociationNumber("1111");

        $o_account->setEIN("313456789");

        $o_account->setWebsite("www.google.com");
        $o_account->setSettlementAccountBankName("test bank name");
        $o_account->setSettlementAccountBankPhone("123-123-1234");
        $o_account->setSettlementAccountName("test account name");
        $o_account->setSettlementAccountNumber("123456789");
        $o_account->setSettlementAccountRoutingNumber("000000000");
        $o_account->setSettlementSameAsFeeAccount(true);


        $o_merch_app->setAccount($o_account);


        // Add Location 
        $o_location = new Location();
        $o_location->setAlternativeFax("111-111-1111");
        $o_location->setAnnualRevenue(10000000);
        $o_location->setContactEmail("contact@gmail.com");
        $o_location->setContactMobile("222-222-2222");
        $o_location->setContactName("Roger Rabbit");
        $o_location->setContactSameAsOwner(false);
        $o_location->setDescription(Location::$XS_DESCRIPTION_CONSULTING);
        $o_location->setEntityStartDate('12-12-2012 12:12:12');
        $o_location->setEntityState("AZ");
        $o_location->setFax("333-333-3333");
        $o_location->setIndustry(Location::$XS_INDUSTRY_CONSULTING);
        $o_location->setLengthOfCurrentOwnership("1");
        $o_location->setOrganizationMission("We take your business where it needs to go");
        $o_location->setOwnership(Location::$XS_OWNERSHIP_PRIVATE);
        $o_location->setQuantityOfLocation(1);
        $o_location->setSalesAgentName("John Doe");
        $o_location->setTaxExempt(false);
        $o_location->setTotalCustomers(1500);
        $o_location->setYearIncorporated("2015");
        $o_location->setYearsInBusiness(1);
        $o_location->setDBAAddress($o_dba_address);
        $o_location->setChargebackFax("888-888-8888");
        $o_location->setMCC("8011");
        $o_location->setDBAAddress($o_dba_address);

        // Principle Contact.. 
        $o_contact = new PrincipalContact();
        $o_contact->setLastName("Rabbit");
        $o_contact->setFirstName("Roger");

        //Mailing Address
        $o_mailing = new Address(Address::$XS_ADDRESS_NAME_MAILING);
        $o_mailing->setLine1("789 Maple Street");
        $o_mailing->setCity("Toontown");
        $o_mailing->setState("AZ");
        $o_mailing->setZipcode("85284");
        $o_mailing->setCountry("USA");

        $o_contact->setMailingAddress($o_mailing);
        $o_contact->setPhoneNumber("111-111-1111");
        $o_contact->setMobilePhoneNumber("222-222-2222");
        $o_contact->setEmail("test-princ-contact@test.com");
        $o_contact->setTitle("CEO");
        $o_contact->setBirthdate('12-12-2012 12:12:12');
        $o_contact->setAuthorizedUser(True);
        $o_contact->setAccountSigner(True);
        $o_contact->setSSN("111-11-1111");
        $o_contact->setIsPrimary(True);
        $o_contact->setFax("111-111-1112");

        // $o_contact->setOwnershipPercentage( 100 );
        $o_contact->setContactType(PrincipalContact::$XS_CONTACT_TYPE_OWNER);
        $o_contact->setDriverLicenseId("123 456 789");
        $o_contact->setLicenseState("AZ");
        $o_location->addPrincipalContact($o_contact);

        // ACH 
        $o_ach_details = new ACHDetails();

        //$o_ach_details->setAuthMethodConversionPercentage( 100 );
        // ACH should be 100 % 
        // $o_ach_details->setAuthMethodConversionPercentage (100);

        $o_ach_details->setAuthMethodWrittenFaxedPercentage(20);
        $o_ach_details->setAuthMethodWrittenInPersonPercentage(55);
        $o_ach_details->setAuthMethodVerbalPercentage(5);
        $o_ach_details->setAuthMethodOnlinePercentage(10);
        $o_ach_details->setAuthMethodConversionPercentage(10);


        $o_ach_details->setAverageTicketAmount(1);
        $o_ach_details->setChargebackFee(1);
        $o_ach_details->setCompanyNameDescriptor("comp name descriptor");
        $o_ach_details->setDescriptor("descript");
        $o_ach_details->setMaxDailyAmount(1);
        $o_ach_details->setMaxSingleTransactionAmount(1);
        $o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_BUSINESSES_ORGANIZATIONS);
        $o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_CONSUMER_INDIVIDUALS);
        $o_ach_details->setReturnFee(1);
        $o_ach_details->addSubmissionMethod(ACHDetails::$XS_SUBMISSION_METHOD_MANUALLY_VIA_VIRTUAL_TERMINAL);
        $o_ach_details->setTransactionFee(1);
        $o_ach_details->setUnauthReturn(1);
        $o_ach_details->setAverageMonthlyAmount(1);

        $o_location->setAchDetails($o_ach_details);

        // Bank Card..
        $o_bc_details = new BankCardDetails();
        $o_bc_details->addCreditRequested(BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT);
        $o_bc_details->setAuthorizationFee(1);
        $o_bc_details->setAverageMonthlyVolume(1);
        $o_bc_details->setAverageTicket(1);

        // Total should be 100%.. 
        $o_bc_details->setCardPresentPercentage(80);
        $o_bc_details->setInternetPercentage(10);
        $o_bc_details->setMailOrderPercentage(1);
        $o_bc_details->setTelephoneOrderPercentage(9);

        // Card Custom Fields..
        /*  $o_bc_details->addDebitBrandsRequested("Bank of Somalia");

          $o_bc_details->addDebitSignatureCardsRequested("Bank of AAA");
          $o_bc_details->addOtherBrandsRequested("VISA");

          $o_bc_details->addCreditRequested("MASTER");
          $o_bc_details->addCreditSignatureCardsRequested("AMEX");
         */

        //$o_bc_details->setFlatRate( 1 );        
        $o_bc_details->setMaxMonthlyVolume(1);
        $o_bc_details->setMaxTicket(1);
        $o_bc_details->setTransactionFee(1);

        //$o_bc_details->set

        $o_location->setBankCardDetails($o_bc_details);

        $o_merch_app->addLocation($o_location);
        //$o_merch_app->addLocation( $o_location );
        try {
            $o_merch_applications = $o_bcpc->submitApplication($o_merch_app);
            //                        print_r($o_merch_applications);
            for ($n_index = 0, $n_size = count($o_merch_applications); $n_index < $n_size; $n_index++) {
                $o_merch_app = $o_merch_applications[$n_index];
                echo "\n\nSUCCESS";
                echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
                echo "\n\n";
                echo "\n\n o_merch_app id = " . $o_merch_app->getId();
                echo "\n\n";
                echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
                echo "\n\n";
                echo "\n\n o_merch_app salesforce ids = " . print_r($o_merch_app->getSalesforceID());
            }
        } catch (BaseCommerceClientException $e) {
            echo "ERROR";
            var_dump($e->getMessage());
            //var_dump($e->getTrace());
        }
    }

    /**
     * Create a Merchant with non AutoBoarding..
     */
    function testBothACHAndBankCardforNonAutoBoard() {
        // Add Partner User info.
		var_dump("Inside testBothACHAndBankCardforNonAutoBoard");
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $o_merch_app = new MerchantApplication();
        $o_merch_app->setApiVersion("1.2");
        $o_merch_app->setCode($this->s_username);

        // Create new account
        $o_account = new Account();
        $o_account->setAccountName("TEST Account-2");
        $o_account->setTestAccount(True);
        $o_account->setAcceptACH(True);
        $o_account->setAcceptBC(True);

        // Custom Fields
        $o_account->setCongaTemplateId("1111");
        $o_account->setReferralPartnerID("a0Fi0000006Om7a");
        $o_account->setIpAddressOfAppSubmission("192.168.0.2");

        // Legal Address
        $o_legal_address = new Address(Address::$XS_ADDRESS_NAME_LEGAL);
        $o_legal_address->setLine1("123 Oak Street");
        $o_legal_address->setCity("Toontown");
        $o_legal_address->setState("AZ");
        $o_legal_address->setZipcode("85284");
        $o_legal_address->setCountry("US");

        $o_account->setLegalAddress($o_legal_address);

        // DBA Address.
        $o_dba_address = new Address(Address::$XS_ADDRESS_NAME_DBA);
        $o_dba_address->setLine1("456 Maple Street");
        $o_dba_address->setCity("Toontown");
        $o_dba_address->setState("AZ");
        $o_dba_address->setZipcode("85284");
        $o_dba_address->setCountry("US");


        $o_account->setAccountPhoneNumber("123-456-7891");

        $o_account->setCustomerServicePhoneNumber("981-124-6431");
        $o_account->setDBA("ACME Inc");

        $o_account->setEntityType(Account::$XS_ENTITY_TYPE_CORP);
        $o_account->setAssociationNumber("1111");

        $o_account->setEIN("313456789");

        $o_account->setWebsite("www.google.com");
        $o_account->setSettlementAccountBankName("test bank name");
        $o_account->setSettlementAccountBankPhone("123-123-1234");
        $o_account->setSettlementAccountName("test account name");
        $o_account->setSettlementAccountNumber("123456789");
        $o_account->setSettlementAccountRoutingNumber("000000000");
        $o_account->setSettlementSameAsFeeAccount(true);


        $o_merch_app->setAccount($o_account);


        // Add Location 
        $o_location = new Location();
        $o_location->setAlternativeFax("111-111-1111");
        $o_location->setAnnualRevenue(10000000);
        $o_location->setContactEmail("contact@gmail.com");
        $o_location->setContactMobile("222-222-2222");
        $o_location->setContactName("Roger Rabbit");
        $o_location->setContactSameAsOwner(false);
        $o_location->setDescription(Location::$XS_DESCRIPTION_CONSULTING);
        $o_location->setEntityStartDate('12-12-2012 12:12:12');
        $o_location->setEntityState("AZ");
        $o_location->setFax("333-333-3333");
        $o_location->setIndustry(Location::$XS_INDUSTRY_CONSULTING);
        $o_location->setLengthOfCurrentOwnership("1");
        $o_location->setOrganizationMission("We take your business where it needs to go");
        $o_location->setOwnership(Location::$XS_OWNERSHIP_PRIVATE);
        $o_location->setQuantityOfLocation(1);
        $o_location->setSalesAgentName("John Doe");
        $o_location->setTaxExempt(false);
        $o_location->setTotalCustomers(1500);
        $o_location->setYearIncorporated("2015");
        $o_location->setYearsInBusiness(1);
        $o_location->setDBAAddress($o_dba_address);
        $o_location->setChargebackFax("888-888-8888");
        $o_location->setMCC("8011");
        $o_location->setDBAAddress($o_dba_address);

        // Principle Contact.. 
        $o_contact = new PrincipalContact();
        $o_contact->setLastName("Rabbit");
        $o_contact->setFirstName("Roger");

        //Mailing Address
        $o_mailing = new Address(Address::$XS_ADDRESS_NAME_MAILING);
        $o_mailing->setLine1("789 Maple Street");
        $o_mailing->setCity("Toontown");
        $o_mailing->setState("AZ");
        $o_mailing->setZipcode("85284");
        $o_mailing->setCountry("USA");

        $o_contact->setMailingAddress($o_mailing);
        $o_contact->setPhoneNumber("111-111-1111");
        $o_contact->setMobilePhoneNumber("222-222-2222");
        $o_contact->setEmail("test-princ-contact@test.com");
        $o_contact->setTitle("CEO");
        $o_contact->setBirthdate('12-12-2012 12:12:12');
        $o_contact->setAuthorizedUser(True);
        $o_contact->setAccountSigner(True);
        $o_contact->setSSN("111-11-1111");
        $o_contact->setIsPrimary(True);
        $o_contact->setFax("111-111-1112");

        // $o_contact->setOwnershipPercentage( 100 );
        $o_contact->setContactType(PrincipalContact::$XS_CONTACT_TYPE_OWNER);
        $o_contact->setDriverLicenseId("123 456 789");
        $o_contact->setLicenseState("AZ");
        $o_location->addPrincipalContact($o_contact);

        // ACH 
        $o_ach_details = new ACHDetails();
        //$o_ach_details->setAuthMethodConversionPercentage( 100 );
        $o_ach_details->setAverageTicketAmount(1);
        $o_ach_details->setChargebackFee(1);
        $o_ach_details->setCompanyNameDescriptor("comp name descriptor");
        $o_ach_details->setDescriptor("descript");
        $o_ach_details->setMaxDailyAmount(1);
        $o_ach_details->setMaxSingleTransactionAmount(1);
        $o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_BUSINESSES_ORGANIZATIONS);
        $o_ach_details->addPaymentToFrom(ACHDetails::$XS_PAYMENT_TO_FROM_CONSUMER_INDIVIDUALS);
        $o_ach_details->setReturnFee(1);
        $o_ach_details->addSubmissionMethod(ACHDetails::$XS_SUBMISSION_METHOD_MANUALLY_VIA_VIRTUAL_TERMINAL);
        $o_ach_details->setTransactionFee(1);
        $o_ach_details->setUnauthReturn(1);
        $o_ach_details->setAverageMonthlyAmount(1);
		$o_ach_details->setPaymentUrl("www.payme.com");
		
        $o_location->setAchDetails( $o_ach_details );
        // Bank Card..
        $o_bc_details = new BankCardDetails();
        $o_bc_details->addCreditRequested(BankCardDetails::$XS_CREDIT_REQUESTED_DISCOVER_CREDIT);
        $o_bc_details->setAuthorizationFee(1);
        $o_bc_details->setAverageMonthlyVolume(1);
        $o_bc_details->setAverageTicket(1);

        // Total should be 100%.. 
        $o_bc_details->setCardPresentPercentage(80);
        $o_bc_details->setInternetPercentage(10);
        $o_bc_details->setMailOrderPercentage(1);
        $o_bc_details->setTelephoneOrderPercentage(9);
        // Card Custom Fields..
        $o_bc_details->addDebitBrandsRequested("Bank of Somalia");
        $o_bc_details->addDebitSignatureCardsRequested("Bank of AAA");

        //$o_bc_details->setFlatRate( 1 );        
        $o_bc_details->setMaxMonthlyVolume(1);
        $o_bc_details->setMaxTicket(1);
        $o_bc_details->setTransactionFee(1);
		$o_bc_details->setBundledSingleRate(1);
		$o_bc_details->setPTPDiscountPerItem(-0.1);
		$o_bc_details->setAmexPTPDiscountPerItem(-0.1);
		$o_bc_details->setEarlyTerminationFee(-0.1);
		
        $o_location->setBankCardDetails($o_bc_details);
		
        $o_merch_app->addLocation($o_location);
        //$o_merch_app->addLocation( $o_location );
        try {
            $o_merch_applications = $o_bcpc->submitApplication($o_merch_app);
            //                        print_r($o_merch_applications);
            for ($n_index = 0, $n_size = count($o_merch_applications); $n_index < $n_size; $n_index++) {
                $o_merch_app = $o_merch_applications[$n_index];
                echo "\n\nSUCCESS";
                echo "\n\n o_merch_app response code = " . $o_merch_app->getResponseCode();
                echo "\n\n";
                echo "\n\n o_merch_app id = " . $o_merch_app->getId();
                echo "\n\n";
                echo "\n\n o_merch_app response messages = " . print_r($o_merch_app->getResponseMessages());
                echo "\n\n";
                echo "\n\n o_merch_app salesforce ids = " . print_r($o_merch_app->getSalesforceID());
            }
        } catch (BaseCommerceClientException $e) {
            echo "ERROR";
            var_dump($e->getMessage());
            //var_dump($e->getTrace());
        }
    }
    
    /**
     * Create to test the generate Cipher crypt
     */
    function testGenerateCipherCrypt() {

        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $o_crypt_info = new CryptInfo();
       
        $o_crypt_info = $o_bcpc->generateCipherCrypt($o_crypt_info);
        echo " KSN :";
        echo $o_crypt_info->getKsn();
        
    }
    
    /**
     * Create to test the encryption of Cipher crypt
     */
    function testEncryptCipherCrypt() {

        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $o_Cipher_text = "";
        $o_crypt_info = new CryptInfo();
        $o_crypt_info->setKsn("e8f258dc-138e-4c2b-8346-d9ec2c597c79");
        $o_crypt_info->setPlainText("Hello Test");
       
        $o_Cipher_text = $o_bcpc->encryptCipherCrypt($o_crypt_info);
        if(gettype($o_Cipher_text) == "string")
            echo "Exception :".$o_Cipher_text;
        else {
            echo "Cipher Text :";
            echo $o_Cipher_text->getCipherText();
        }
        
    }
    
    /**
     * Create to test the decryption of Cipher crypt
     */
    function testDecryptCipherCrypt() {

        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        $o_Cipher_text = "";
        $o_crypt_info = new CryptInfo();
        $o_crypt_info->setKsn("3455daab-7e66-4797-8bb4-8c1e92b6394a");
        $o_crypt_info->setCipherText("85E1887A6604E28E7244493B3CD6350B");
       
        $o_Cipher_text = $o_bcpc->decryptCipherCrypt($o_crypt_info);
        if(gettype($o_Cipher_text) == "string")
            echo "Exception :".$o_Cipher_text;
        else {
            echo "Plain Text :";
            echo $o_Cipher_text->getPlainText();
        }

    }
    
    /*
    * Create a Bank Account Recurring Transaction with New Bank Account and no 
    * end date.
    */
    function processRecurringTransactionDailyWithNewBankAccountAndNoEndDate() {
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient($this->s_username, $this->s_password, $this->s_key);
        $o_bcpc->setSandbox(true);

        // Create a new RecurringTransaction and set all the appropriate fields
        $o_recurring_transaction = new RecurringTransaction();

        $o_recurring_transaction->setFrequency(RecurringTransaction::$XS_FREQUENCY_DAILY);
        $o_recurring_transaction->setStartDate(( new DateTime('6/15/2015'))->format('m/d/Y'));
//        $o_recurring_transaction->setEndDate( date('m/d/Y') ); // If not set, recurring transaction is considered continuous ( runs forever )
        $o_recurring_transaction->setAmount(100.00);

        // Create a BankCard and add it to the recurring transaction
        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_ba = new BankAccount();
        $o_ba->setBillingAddress($o_address);
        $o_ba->setName("Bradly Rosno");
        $o_ba->setAccountNumber("123123123");
        $o_ba->setAccountType(BankAccount::$XS_BA_TYPE_CHECKING);
        $o_ba->setRoutingNumber("011000015");

        $o_recurring_transaction->setBankAccount($o_ba);

        $o_returned_rt = $o_bcpc->processRecurringTransaction($o_recurring_transaction);

        var_dump($o_returned_rt->getRecurringTransactionID());
        var_dump($o_returned_rt->getStatus());
        var_dump($o_returned_rt->getJSON());
        
    }
}