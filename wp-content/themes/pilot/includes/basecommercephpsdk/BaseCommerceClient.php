<?php

include 'TripleDESService.php';
include 'BaseCommerceClientException.php';
include 'SettlementBatch.php';
include 'CryptInfo.php';

/**
 * Description of BaseCommerceClient
 * BaseCommerceClient is an entity that handles all of the client side work for creating and sending the request, 
 * as well as formatting and returning the response to the user.
 *
 * © Base Commerce
 */
class BaseCommerceClient {

    private $is_url = 'https://gateway.basecommerce.com';
    private $is_sandbox_url = 'https://gateway.basecommercesandbox.com';
    
    private $is_gateway_username;
    private $is_gateway_password;
    private $is_key;
    private $is_last_session_id;
    private $ib_sandbox;

    /**
     * 
     * Default Constructor
     * 
     */
    function __construct($vs_gateway_username, $vs_gateway_password, $vs_key) {

        $this->is_gateway_username = $vs_gateway_username;
        $this->is_gateway_password = $vs_gateway_password;
        $this->is_key = $vs_key;
        $this->ib_sandbox = FALSE;
    }

    /**
     * Sets the bank card transaction to be processed through the sandbox environment.
     * 
     * @param $vb_sandbox true if it should be processed through the sandbox, false otherwise.
     */
    public function setSandbox($vb_sandbox) {
        $this->ib_sandbox = $vb_sandbox;
    }

    /**
     * Returns true if the bank card transaction is to be processed through the sandbox environment.
     * 
     * @return true if it should be processed through the sandbox, false otherwise.
     */
    public function isSandbox() {
        return $this->ib_sandbox;
    }

    /**
     * Returns the last session id generated during a client method call
     * 
     * @return the last session id
     */
    public function getLastSessionId() {
        return $this->is_last_session_id;
    }

    /**
     * 
     * Returns a Bank Card Transaction given a specific id.
     * 
     * @param type $vn_bct_id     the id of a bank card transaction
     * @return the bank card transaction if one exists for the given id
     * @throws BaseCommerceClientException if an invalid bank card transaction id was give, invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function getBankCardTransaction($vn_bct_id) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $vn_bct_id;

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_getBankCardTransactionV4', $s_query, $triple_des, 0);

        $o_bct = BankCardTransaction::buildFromJSON($response["bank_card_transaction"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {
                $o_bct->addMessage($s_error);
            }
        }

        return $o_bct;
    }

    /**
     * 
     * Processes the transaction using the specified BankCardTransaction.
     * Returns an updated BankCardTransaction containing the response information.
     * 
     * @param vo_bct    the bank card transaction
     * @return  the updated bank card transaction
     * @throws BaseCommerceClientException if invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function addBankCard(BankCard $vo_bc) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vo_bc->getJSON());

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_addBankCardV4', $s_query, $triple_des, 0);

        $vo_bc = BankCard::buildFromJSON($response["bank_card"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {
                $vo_bc->addMessage($s_error);
            }
        }

        return $vo_bc;
    }

    /**
     * 
     * Processes the transaction using the specified BankCardTransaction.
     * Returns an updated BankCardTransaction containing the response information.
     * 
     * @param vo_bct    the bank card transaction
     * @return  the updated bank card transaction
     * @throws BaseCommerceClientException if invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function processBankCardTransaction(BankCardTransaction $vo_bct) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vo_bct->getJSON());

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_processBankCardTransactionV4', $s_query, $triple_des, 0);

        $vo_bct = BankCardTransaction::buildFromJSON($response["bank_card_transaction"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {
                $vo_bct->addMessage($s_error);
            }
        }
        return $vo_bct;
    }

    /**
     * 
     * Processes the Recurring Transaction
     * Returns an updated RecurringTransaction containing the response information.
     * 
     * @param vo_recurring_transaction    the Recurring transaction
     * @return  the updated RecurringTransaction object
     * @throws BaseCommerceClientException if invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function processRecurringTransaction(RecurringTransaction $vo_recurring_transaction) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vo_recurring_transaction->getJSON());

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_processRecurringTransaction', $s_query, $triple_des, 0);

        $o_returned_recurring_transaction = RecurringTransaction::buildFromJSON($response["recurring_transaction"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {
                $o_returned_recurring_transaction->addMessage($s_error);
            }
        }
        return $o_returned_recurring_transaction;
    }

    /**
     * 
     * Cancel a Recurring Transaction that belongs to the id passed in
     * Returns an updated RecurringTransaction containing the updated information.
     * 
     * @param $vn_recurring_transaction_id the id of a RecurringTransaction
     * @return  the updated RecurringTransaction object
     * @throws BaseCommerceClientException if invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function cancelRecurringTransaction($vn_recurring_transaction_id) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vn_recurring_transaction_id);

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_cancelRecurringTransaction', $s_query, $triple_des, 0);

        $o_returned_recurring_transaction = RecurringTransaction::buildFromJSON($response["recurring_transaction"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {
                $o_returned_recurring_transaction->addMessage($s_error);
            }
        }
        return $o_returned_recurring_transaction;
    }

    /**
     * 
     * Returns a Bank Account Transaction given a specific id.
     * 
     * @param type $vn_bat_id     the id of a bank account transaction
     * @return the bank account transaction if one exists for the given id
     * @throws BaseCommerceClientException if an invalid bank card transaction id was give, invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function getBankAccountTransaction($vn_bat_id) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $vn_bat_id;

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_getBankAccountTransactionV4', $s_query, $triple_des, 0);

        $o_bat = BankAccountTransaction::buildFromJSON($response["bank_account_transaction"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {
                $o_bat->addMessage($s_error);
            }
        }

        return $o_bat;
    }

    /**
     * 
     * Add a bank account to the system.
     * 
     * @param vo_bat    the bank account transaction
     * @return  the updated bank account transaction
     * @throws BaseCommerceClientException if invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function addBankAccount(BankAccount $vo_ba) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vo_ba->getJSON());

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_addBankAccountV4', $s_query, $triple_des, 0);

        $vo_ba = BankAccount::buildFromJSON($response["bank_account"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {
                $vo_ba->addMessage($s_error);
            }
        }

        return $vo_ba;
    }

    /**
     * 
     * Processes the transaction using the specified BankAccountTransaction.
     * Returns an updated BankAccountTransaction containing the response information.
     * 
     * @param vo_bct    the bank account transaction
     * @return  the updated bank account transaction
     * @throws BaseCommerceClientException if invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function processBankAccountTransaction(BankAccountTransaction $vo_bat) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vo_bat->getJSON());

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_processBankAccountTransactionV4', $s_query, $triple_des, 0);

        $vo_bat = BankAccountTransaction::buildFromJSON($response["bank_account_transaction"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {
                $vo_bat->addMessage($s_error);
            }
        }

        return $vo_bat;
    }

    /**
     * Submits a merchant application
     * 
     * @param MerchantApplication $vo_merch_app the merchant application
     * @return array of the merchant application
     * @throws BaseCommerceClientException if invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function submitApplication(MerchantApplication $vo_merch_app) {

        $o_triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $o_triple_des->encrypt($vo_merch_app->getJSON());

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=newSubmitApplication', $s_query, $o_triple_des, 0);

        $o_merchant_applications = array();

        for ($n_index = 0, $n_size = count($response); $n_index < $n_size; $n_index++) {
            array_push($o_merchant_applications, MerchantApplication::buildFromJSON($response[$n_index]));
        }

        return $o_merchant_applications;
    }

    /**
     * 
     * Private helper function to send / recieve posts to the server.  
     * @param $url The url to attempt to post to 
     * @param $data The payload to send to the url
     * @param $triple_des  A triple DES object to decrypt messages from the server
     * @param $optional_headers  Optional headers
     * @return The decrypted response from the server
     * @throws BaseCommerceClientException    If anything goes wrong
     */
    private function do_post_request($url, $data, $triple_des, $vn_400_retry_counter, $optional_headers = null) {

        $s_url = "";
        if ($this->ib_sandbox) {
            $s_url = $this->is_sandbox_url . $url;
        } else {
            $s_url = $this->is_url . $url;
        }
            $params = array( 'http' => array(
               'method' => 'POST',
               'content' => $data,
               'header' => "Content-type: application/x-www-form-urlencoded") );



        if ($optional_headers !== null) {
            $params['http']['header'] = $optional_headers;
        }
        $ctx = stream_context_create($params);
        

        ini_set('user_agent', 'BaseCommerceClientPHP/5.1.7');

        $fp = fopen($s_url, 'rb', false, $ctx);
        if (!$fp) {
            $lasterror = error_get_last();
            $error = $lasterror['message'];
            if (strpos($error, '403') !== false) {
                throw new BaseCommerceClientException('Invalid Credentials.', '403');
            } else if (strpos($error, '500') !== false) {
                throw new BaseCommerceClientException('Internal Server Error. Please contact tech support.', '500');
            } else if (strpos($error, '404') != false) {
                throw new BaseCommerceClientException('Invalid url or host is offline.', '404');
            } else if (strpos($error, '400') != false) {
                if ($vn_400_retry_counter < 10) {
                    sleep(3);
                    return $this->do_post_request($url, $data, $triple_des, $vn_400_retry_counter + 1);
                } else {
                    throw new BaseCommerceClientException('Error Connecting to BaseCommerce', '400');
                }
            }
            throw new BaseCommerceClientException($error);
        }

        $response = stream_get_contents($fp);

        // adapted from http://us.php.net/manual/en/function.stream-get-meta-data.php
        $meta = stream_get_meta_data($fp);
        foreach (array_keys($meta) as $h) {
            $v = $meta[$h];
            if (is_array($v)) {
                foreach (array_keys($v) as $hh) {
                    $vv = $v[$hh];
                    if (is_string($vv) && substr_count($vv, 'JSESSIONID')) {
                        $this->is_last_session_id = substr($vv, strpos($vv, '=') + 1, 24);
                    }
                }
            }
        }

        if ($response === false) {
            $lasterror = error_get_last();
            
            $error = $lasterror['message'];
            if (strpos($error, '403') !== false) {
                throw new BaseCommerceClientException('Invalid Credentials.', '403');
            } else if (strpos($error, '500') !== false) {
                throw new BaseCommerceClientException('Internal Server Error. Please contact tech support.', '500');
            } else if (strpos($error, '404') !== false) {
                throw new BaseCommerceClientException('Invalid url or host is offline.', '404');
            } else if (strpos($error, '400') != false) {
                if ($vn_400_retry_counter < 10) {
                    sleep(3);
                    return $this->do_post_request($url, $data, $triple_des, $vn_400_retry_counter + 1);
                } else {
                    throw new BaseCommerceClientException('Error Connecting to BaseCommerce', '400');
                }
            }
            throw new BaseCommerceClientException($error);
        }
        $decrypted_response = $triple_des->decrypt($response);

        $decrypted_response = trim($decrypted_response, "\x00..\x1F");

        $decrypted_response = json_decode($decrypted_response, TRUE);

        return $decrypted_response;
    }

    /**
     * creates a push notification from the passed in string
     * 
     * @param type $vs_json the passed in string
     * @return the push notification
     */
    public function handlePushNotification($vs_json) {
        $triple_des = new TripleDESService($this->is_key);
        $o_json = $triple_des->decrypt($vs_json);
        $o_json = trim($o_json, "\x00..\x1F");
        $o_json = json_decode($o_json, TRUE);

        if (array_key_exists("push_notification_type", $o_json) && $o_json["push_notification_type"] == PushNotification::$XS_PN_TYPE_JSON) {
            $o_return = JSONPushNotification::buildFromJSON($o_json);
        } else {
            $o_return = PushNotification::buildFromJSON($o_json);
        }
        return $o_return;
    }

    /**
     * Sets the merchants push notification url
     * @param type $vs_url the url
     * @return a JSON object with success if it worked and exception if not
     */
    public function setPushNotificationURL($vs_url) {

        $o_triple_des = new TripleDESService($this->is_key);
        $o_return = "";

        $o_payload = array();
        $o_payload['url'] = $vs_url;

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $o_triple_des->encrypt(json_encode($o_payload));

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_setPushNotificationURLV4', $s_query, $o_triple_des, 0);

        if (array_key_exists("success", $response)) {
            $o_return = $response["success"];
        } else {
            $o_temp = $response["exception"];
            $o_return = $o_temp["exception"];
        }

        return $o_return;
    }

    /**
     * sends a ping JSON object to ping the server
     * @return true if everything worked, false if not
     * @throws BaseCommerceClientException if an underlying error occurs
     */
    public function sendPing() {

        $b_return = FALSE;

        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_payload_array = array();
        $o_payload_array['PING'] = "Ping Ping";

        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt(json_encode($o_payload_array));

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_PingPong', $s_query, $triple_des, 0);

        if (array_key_exists("success", $response)) {
            $b_return = TRUE;
        }

        return $b_return;
    }

    /**
     * Processes a generic Request.
     * @param type $vs_json the request json
     * @return a json object string that holds the return data
     */
    public function processRequest($vs_json) {

        $o_triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $o_triple_des->encrypt(json_encode($vs_json));

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_processRequest', $s_query, $o_triple_des, 0);

        return $response;
    }

    /**

     * Updates the specified bank card Expiration date and billing address.
     * Returns an updated BankCardTransaction containing the response information.
     * @param vo_bct    the bank card transaction
     * @return  the updated bank card transaction
     * @throws BaseCommerceClientException if invalid credentials were given or 
     * if there was an internal server error. Please contact tech support 
     * if there is an internal server error.
     */
    public function updateBankCard(BankCard $vo_bc) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vo_bc->getJSON());

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_updateBankCardV4', $s_query, $triple_des, NULL);

        if (array_key_exists("bank_card", $response) && !is_null($response['bank_card'])) {
            $vo_bc = BankCard::buildFromJSON($response["bank_card"]);
        }

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {
            foreach ($response['exception'] as $s_key => $s_error) {
                $vo_bc->addMessage($s_error);
            }
        }
        return $vo_bc;
    }

    /* sends a BankCardUpdateRequest object to the API
     * @param BankCardUpdateRequest $vo_bcur the BankCArdUpdateRequet object
     * @return the updated BankCardUpdateRequest
     */

    public function processBankCardUpdateRequest(BankCardUpdateRequest $vo_bcur) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;

        $o_query['payload'] = $triple_des->encrypt($vo_bcur->getJSON());

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_processBankCardUpdateRequest', $s_query, $triple_des, 0);

        $vo_bcur = BankCardUpdateRequest::buildFromJSON($response["bank_card_update_request"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {

                $vo_bcur->addMessage($s_error);
            }
        }

        return $vo_bcur;
    }

    /**
     * 
     * deletes a bank card from the system
     * 
     * @param vs_token the bank cards token
     * @return  the updated deleted bank card
     * @throws BaseCommerceClientException if invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function deleteBankCard($vs_token) {

        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vs_token);

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_deleteBankCardV4', $s_query, $triple_des, 0);

        $vo_bc = BankCard::buildFromJSON($response["bank_card"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {

                $vo_bc->addMessage($s_error);
            }
        }

        return $vo_bc;
    }

    /**
     * 
     * deletes a bank account from the system
     * @param vs_token the bank accounts token
     * @return  the updated deleted bank account
     * @throws BaseCommerceClientException if invalid credentials were given or if there was an internal server error. Please contact tech support if there is an internal server error.
     */
    public function deleteBankAccount($vs_token) {

        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vs_token);

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_deleteBankAccountV4', $s_query, $triple_des, 0);

        $vo_ba = BankAccount::buildFromJSON($response["bank_account"]);

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {

            foreach ($response['exception'] as $s_key => $s_error) {
                $vo_ba->addMessage($s_error);
            }
        }

        return $vo_ba;
    }

    /**
     * 
     * Update a bank account to the system.
     * Only Billing address and Routing no can be updated.
     * @param vo_ba the bank account transaction
     * @return  the updated bank account
     * @throws BaseCommerceClientException if invalid credentials were given 
     * or if there was an internal server error. 
     * Please contact tech support if there is an internal server error.
     */
    public function updateBankAccount(BankAccount $vo_ba) {
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();

        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($vo_ba->getJSON());

        $s_query = json_encode($o_query);


        $response = $this->do_post_request('/pcms/?f=API_updateBankAccountV4', $s_query, $triple_des, 0);


        if (array_key_exists("bank_account", $response) && !is_null($response['bank_account'])) {
            $vo_ba = BankAccount::buildFromJSON($response["bank_account"]);
        }

        if (array_key_exists("exception", $response) && !is_null($response['exception'])) {
            foreach ($response['exception'] as $s_key => $s_error) {
                $vo_ba->addMessage($s_error);
            }
        }
        return $vo_ba;
    }

    /**
     * Create bank card transaction settlement batch
     * Returns the new settlement batch  
     * 
     * @return the settlement batch object
     * @throws BaseCommerceClientException if invalid credentials were given or 
     * if there was an internal server error. Please contact tech support 
     * if there is an internal server error.
     */
    public function createBankCardTransactionSettlementBatch() {
        $vo_bs = new SettlementBatch();
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_payload_array = array();
        $o_payload_array[''] = "";

        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt(json_encode($o_payload_array));

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_createBankCardTransactionSettlementBatch', $s_query, $triple_des, 0);

        if (array_key_exists("settlement_batch", $response) && !is_null($response['settlement_batch'])) {
            $vo_bs = $vo_bs->buildFromJSON($response["settlement_batch"]);
        }
        return $vo_bs;
    }

    /**
     * Returns BankAccountTransactions that match the given Merchant Transaction ID
     * @return array of BATs
     * @throws BaseCommerceClientException if invalid credentials were given or 
     * if there was an internal server error. Please contact tech support 
     * if there is an internal server error.
     */
    public function getBankAccountTransactionByMerchantTransactionID($merchant_txn_id) {

        $triple_des = new TripleDESService($this->is_key);
        $o_query = array();

        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($merchant_txn_id);

        $s_query = json_encode($o_query);
        
        $o_bat = new BankAccountTransaction();
        $o_bat_list = array();
    
        $o_response_array = $this->do_post_request('/pcms/?f=API_getBankAccountTransactionsByMerchantTransactionID', $s_query, $triple_des, 0);

        for ( $n_index = 0; $n_index < count($o_response_array); $n_index++) {
                
            $bat_json = $o_response_array[ $n_index ];
            
            if (array_key_exists("exception", $bat_json)) {
                       foreach ($bat_json as $s_key => $s_error) {
                           $o_bat->addMessage($s_error);  
                        }
                    }else{
                        $o_bat = BankAccountTransaction::buildFromJSON($bat_json);
                    }
            
            
            array_push( $o_bat_list , $o_bat );
        }
        
        
        return  $o_bat_list;
    }

    /**
     * Returns BankCardTransactions that match the given Merchant Transaction ID
     * @return array of BCTs
     * @throws BaseCommerceClientException if invalid credentials were given or 
     * if there was an internal server error. Please contact tech support 
     * if there is an internal server error.
     */
    public function getBankCardTransactionByMerchantTransactionID($merchant_txn_id) {
        $triple_des = new TripleDESService($this->is_key);
        $o_query = array();

        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;
        $o_query['payload'] = $triple_des->encrypt($merchant_txn_id);

        $s_query = json_encode($o_query);
        $o_bct_list = array();
        
        $o_bct = new BankCardTransaction();
        
        $o_response_array = $this->do_post_request('/pcms/?f=API_getBankCardTransactionsByMerchantTransactionID', $s_query, $triple_des, 0);
        
        for ( $n_index = 0; $n_index < count($o_response_array); $n_index++) {
                $bct_json = $o_response_array[ $n_index ];
                
                if (array_key_exists("exception", $bct_json)) {
                       foreach ($bct_json as $s_key => $s_error) {
                           $o_bct->addMessage($s_error);  
                        }
                    }else{
                        $o_bct = BankCardTransaction::buildFromJSON($bct_json);
                    }
                  array_push( $o_bct_list , $o_bct);
        }
        return  $o_bct_list;
    }
    
    /*
     * Returns the object of CryptInfo
     * This method helps to encrypt the plain text using ksn
     * 
    */ 
    
     public function encryptCipherCrypt(CryptInfo $vo_eci){
       
        $triple_des = new TripleDESService($this->is_key);
        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;

        if($vo_eci->getJSON() != "[]"){
            $o_query['payload'] = $triple_des->encrypt($vo_eci->getJSON());    
        }else{
            $o_query['payload'] = $triple_des->encrypt('{}');
        }
        
        $s_query = json_encode($o_query);
        
        $response = $this->do_post_request('/pcms/?f=API_encryptCipherCrypt', $s_query, $triple_des, 0);
        
        if(array_key_exists('exception', $response)  && !is_null($response['exception'])) 
               foreach ($response['exception'] as $s_key => $s_error) {
                $vo_eci->addMessage($s_error);
            }
        else
                $vo_eci = CryptInfo::buildFromJSON($response);

        return $vo_eci;
    }
    
    /*
     * Returns the object of CryptInfo
     * This method helps to decrypt the cypher text using ksn  
    */ 
    
    public function  decryptCipherCrypt(CryptInfo $vo_dci){
        
         $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;

        if($vo_dci->getJSON() != "[]"){
            $o_query['payload'] = $triple_des->encrypt($vo_dci->getJSON());    
        }else{
            $o_query['payload'] = $triple_des->encrypt('{}');
        }

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_decryptCipherCrypt', $s_query, $triple_des, 0);
        
       if(array_key_exists('exception', $response)  && !is_null($response['exception'])) 
               foreach ($response['exception'] as $s_key => $s_error) {
                $vo_dci->addMessage($s_error);
            }
        else
                $vo_dci = CryptInfo::buildFromJSON($response);

        return $vo_dci;
    }
    
    /*
     * This method helps to generate the ksn 
     * 
     * 
    */ 
    
    public function generateCipherCrypt($vo_gci){
        
        $triple_des = new TripleDESService($this->is_key);

        $o_query = array();
        $o_query['gateway_username'] = $this->is_gateway_username;
        $o_query['gateway_password'] = $this->is_gateway_password;

        $o_query['payload'] = $triple_des->encrypt($vo_gci->getJSON());

        $s_query = json_encode($o_query);

        $response = $this->do_post_request('/pcms/?f=API_generateKeyCipherCrypt', $s_query, $triple_des, 0);

        $vo_gci = CryptInfo::buildFromJSON($response);
        
        return $vo_gci;
    }
 
}
?>
