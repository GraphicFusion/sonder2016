<?php

/**
 * FundingInstructions is a simple re-usable abstract class.
 */

abstract class FundingInstructions {
    
     
    /**
    * buildFromJSON is a simple re-usable abstract method.
    */
   public static function buildFromJSON($vo_json) {
       
       $o_return = null;
       $s_type = null;
       
       if(array_key_exists("funding_instruction_type", $vo_json) ) {
           
           if( gettype( $vo_json['funding_instruction_type'] ) === "array" ){
               
                $o_type = (array)$vo_json['funding_instruction_type'];
                
                if (array_key_exists( 'funding_instruction_type_name', $o_type ) ) {
                    
                    $s_type = $o_type['funding_instruction_type_name'];
                    
                }
               
           } else {
               
               $s_type = $vo_json['funding_instruction_type'];
               
           }
           
       }
       
       if ( $s_type != null && $s_type == "SPLIT_FUNDING" ) {
           
           $o_return = SplitFunding::buildFromJSON($vo_json);
           
       }
       
       return $o_return;
    }
    
      /**
    * getJSON is a simple re-usable abstract method.
    */
    abstract public function getJSON();
}

