<?php


include_once 'BankCardTransaction.php';
include_once 'BankAccountTransaction.php';
include_once 'BaseCommerceClient.php';
include_once 'BankCard.php';
include_once 'BankAccount.php';
include_once 'SettlementBatch.php';

class codeSamples {
    
    //Process Bank Card Transaction
    public function processBankCardTransaction() {
        
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );

        //create a new BankCardTransaction object and add all values to it where the type will be of type XS_SALE
        $o_bct = new BankCardTransaction();
        $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_SALE);
        $o_bct->setCardName("John Doe");
        $o_bct->setCardNumber("4111111111111111");
        $o_bct->setCardExpirationMonth("09");
        $o_bct->setCardExpirationYear("2015");
        $o_bct->setCardCVV2( "800" );
        $o_bct->setAmount(1.33); 

        //create billing Address object to set on the BankCardTransaction
        $o_billing_address = new Address();
        $o_billing_address->setCountry("US");
        $o_billing_address->setCity("Scottsdale");
        $o_billing_address->setLine1("111 East 2nd Street");
        $o_billing_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_billing_address->setState("AZ");
        $o_billing_address->setZipcode("85254");

        $o_bct->setBillingAddress($o_billing_address);

        //Actually process the BankCardTransaction
        $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );

        //check status after the transaction is processed 
        if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_FAILED ) ) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump( $o_bct->getMessages() );
        } else if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_DECLINED ) ) {
            //Transaction Declined, look at response code and response message
            var_dump( $o_bct->getResponseCode() );
            var_dump( $o_bct->getResponseMessage() );
        } else if( $o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_CAPTURED) ) {
            //Transaction went through successfully
            var_dump( $o_bct->getTransactionID() );
        } 

    }


    //Process a Void Transaction
    public function voidBankCardTransaction() {
        
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );

        //create a new BankCardTransaction object and add all values to it, set the type to XS_VOID
        $o_bct = new BankCardTransaction();
        $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_VOID);
        //set the transaction ID on the BankCardTransaction object to the old BankCardTransaction that you want voided
        $o_bct->setTransactionID(1016365);

        //Actually process the BankCardTransaction
        $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );

        //check status after the transaction is processed 
        if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_FAILED ) ) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump( $o_bct->getMessages() );
        } else if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_VOIDED ) ) {
            //Transaction Successful   
            var_dump($o_bct);
        }
    }



    //Process a Refund Transaction
    public function refundBankCardTransaction() {
        
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );

        //create a new BankCardTransaction object and add all values to it, set the type to XS_REFUND
        $o_bct = new BankCardTransaction();
        $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_REFUND);
        //set the transaction ID on the BankCardTransaction object to the old BankCardTransaction that you want refunded
        $o_bct->setTransactionID(1016364);
        //set the amount you want refunded which cannot exceed the original transactions amount
        $o_bct->setAmount(1.33);

        //Actually process the BankCardTransaction
        $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );

        //check status after the transaction is processed 
        if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_FAILED ) ) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump( $o_bct->getMessages() );
        } else if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_DECLINED ) ) {
            //Transaction Declined, look at response code and response message
            var_dump( $o_bct->getResponseCode() );
            var_dump( $o_bct->getResponseMessage() );
        } else if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_CAPTURED ) ) {
            //Transaction went through successfully
            var_dump( $o_bct->getTransactionID() );
        } 
    }


    //Process an ACH Transaction
    public function processACHTransaction() {
        
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );

        //create a new BankAccountTransaction object
        $o_bat = new BankAccountTransaction();
        $o_bat->setAccountName("John Doe");
        $o_bat->setRoutingNumber( "021000021" );
        $o_bat->setAccountNumber( "12312312312" );        
        $o_bat->setAccountType( BankAccountTransaction::$XS_BAT_ACCOUNT_TYPE_CHECKING );
        $o_bat->setMethod( BankAccountTransaction::$XS_BAT_METHOD_CCD );
        $o_bat->setType( BankAccountTransaction::$XS_BAT_TYPE_CREDIT );
        $o_bat->setEffectiveDate( date('d-m-Y H:i:s') );
        $o_bat->setAmount(1.55);

        //actually process the BankAccountTransaction
        $o_bat = $o_bcpc->processBankAccountTransaction( $o_bat );
        
        if( $o_bat->isStatus( BankAccountTransaction::$XS_BAT_STATUS_FAILED ) ) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump( $o_bat->getMessages() );
        } else if( $o_bat->isStatus( BankAccountTransaction::$XS_BAT_STATUS_CREATED ) ) {
            //Transaction went through successfully
            var_dump( $o_bat->getBankAccountTransactionId() );
        }

    }
    
    
    //add a bank card to the vault
    public function addBankCardToVault() {
    
        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );
    
        //create the billing Address object
        $o_address = new Address();
        $o_address->setName(Address::$XS_ADDRESS_NAME_BILLING);
        $o_address->setLine1("123 Some Address");
        $o_address->setCity("Tempe");
        $o_address->setState("AZ");
        $o_address->setZipcode("12345");

        //Create a new BankCard object
        $o_bc = new BankCard();
        $o_bc->setBillingAddress($o_address);
        $o_bc->setExpirationMonth("02");
        $o_bc->setExpirationYear("2015");
        $o_bc->setName("John Smith");
        $o_bc->setNumber("4111111111111111");
        
        //add the bank card to the vault
        $o_bc = $o_bcpc->addBankCard( $o_bc );
        
        if( $o_bc->isStatus(BankCard::$XS_BC_STATUS_FAILED ) ) {
            //the add Failed, look at messages array for reason(s) why
            var_dump( $o_bc->getMessages() );
        } else if( $o_bc->isStatus(BankCard::$XS_BC_STATUS_ACTIVE ) ) {
            //Card added successfully
            var_dump( $o_bc->getToken() );
        }
                
    }
    
    
    //add a bank account to the vault
    public function addBankAccountToVault() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );
        
        //Create the BankAccount object and set the values
        $o_bank_account = new BankAccount();
        $o_bank_account->setName("John Doe");
        $o_bank_account->setAlias("Johns Savings account");
        $o_bank_account->setAccountNumber("1111111111");
        $o_bank_account->setRoutingNumber("111000025");
        $o_bank_account->setAccountType(BankAccount::$XS_BA_TYPE_SAVINGS);

        //Actually add the BankAccount to the vault
        $o_bank_account = $o_bcpc->addBankAccount( $o_bank_account );
        
        if( $o_bank_account->isStatus(BankAccount::$XS_BA_STATUS_FAILED) ) {
            //the add Failed, look at messages array for reason(s) why
            var_dump( $o_bank_account->getMessages() );
        } else if( $o_bank_account->isStatus(BankAccount::$XS_BA_STATUS_ACTIVE ) ) {
            //Account added successfully
            var_dump( $o_bank_account->getToken() );
        }                
        
    }
    
    
    //process bank card transaction using vault record
    public function processBankCardVaultRecord() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );
        
        //Create the BankCardTransction then set the token on it as the token from the BankCard in the vault's token
        $o_bct = new BankCardTransaction();
        $o_bct->setToken("6873c23326df7da61464fe6414a14b7b24435522d7726333abd46a82f920bf2b");
        $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_SALE);
        $o_bct->setAmount(1.15);
        //Then actually process the BankCard
        $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );

        //check status after the transaction is processed 
        if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_FAILED ) ) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump( $o_bct->getMessages() );
        } else if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_DECLINED ) ) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump( $o_bct->getMessages() );
        } else if( $o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_CAPTURED) ) {
            //Transaction went through successfully
            var_dump( $o_bct->getTransactionID() );
        } 
        
        
    }
    
    
    //process bank account transaction using vault record
    public function processBankAccountVaultRecord() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );
        
        //Create a BankAccountTransaction object and set the token on it as the token of the account from the vault
        $o_bat = new BankAccountTransaction();
        $o_bat->setMethod( BankAccountTransaction::$XS_BAT_METHOD_CCD );
        $o_bat->setType( BankAccountTransaction::$XS_BAT_TYPE_CREDIT );
        $o_bat->setEffectiveDate( date('d-m-Y H:i:s') );
        $o_bat->setAmount(2.22);
        $o_bat->setToken("26ca7865a89781436c6a7e8be48998857ce25b4c06f7d49a39401d1b3bbbad85");

        //actually process the BankAccountTransaction
        $o_bat = $o_bcpc->processBankAccountTransaction( $o_bat );
        
        if( $o_bat->isStatus( BankAccountTransaction::$XS_BAT_STATUS_FAILED ) ) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump( $o_bat->getMessages() );
        } else if( $o_bat->isStatus( BankAccountTransaction::$XS_BAT_STATUS_CREATED ) ) {
            //Transaction went through successfully
            var_dump( $o_bat->getBankAccountTransactionId() );
        }
                        
    }
    
    
    //get a previous Bank Card transaction
    public function getBankCardTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );
        
        //Create the BankCardTransaction object
        $o_bct = new BankCardTransaction();
        
        //Call a getBankCardTransaction and pass it the transaction ID of the BankCardTransaction you want returned
        $o_bct = $o_bcpc->getBankCardTransaction(1016351);
        
        if( $o_bct->isStatus(BankAccountTransaction::$XS_BAT_STATUS_FAILED) ) {
            // Transaction failed, look at messages for reasons why
            var_dump( $o_bct->getMessages() );
        } else {
            // Transaction retrieved, look at status of the transaction 
            var_dump( $o_bct->getStatus() );
        }
        
    }
    
    
    //get a previous bank account transaction 
    public function getBankAccountTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );
        
        //Create the BankAccount Transaction object
        $o_bat = new BankAccountTransaction();
        
        //Call a getBankAccountTransaction and pass it the transaction ID of the BankAccountTransaction you want returned
        $o_bat = $o_bcpc->getBankAccountTransaction(107);
                
        if ( $o_bat->isStatus(BankAccountTransaction::$XS_BAT_STATUS_FAILED) ) {
            // Transaction failed, look at messages for reasons why
            var_dump( $o_bat->getMessages() );
        } else {
            // Transaction retrieved, look at status of the transaction 
            var_dump( $o_bat->getStatus() );
        }        
                
    }    
    
    //reverse an ACH Transaction
    public function reverseACHTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );

        //create a new BankAccountTransaction object to be reversed
        $o_bat = new BankAccountTransaction();
        $o_bat->setBankAccountTransactionId(146);        
        $o_bat->setType( BankAccountTransaction::$XS_BAT_TYPE_REVERSAL );

        //actually process the BankAccountTransaction
        $o_bat = $o_bcpc->processBankAccountTransaction( $o_bat );
                
        if( $o_bat->isStatus( BankAccountTransaction::$XS_BAT_STATUS_FAILED ) ) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump( $o_bat->getMessages() );
        } else if( $o_bat->isStatus( BankAccountTransaction::$XS_BAT_STATUS_CREATED ) ) {
            //Transaction went through successfully
            var_dump( $o_bat->getBankAccountTransactionId() );
        }
        

    }
    
    
    //cancel an ACH Transaction
    public function cancelACHTransaction() {

        //create BaseCommerceClient with your given username, password, and key and set sandbox to true
        $o_bcpc = new BaseCommerceClient( $s_username, $s_password, $s_key );
        $o_bcpc->setSandbox( true );

        //create a new BankAccountTransaction object
        $o_bat = new BankAccountTransaction();
        $o_bat->setBankAccountTransactionId(145);
        $o_bat->setType( BankAccountTransaction::$XS_BAT_TYPE_CANCEL );

        //actually process the BankAccountTransaction
        $o_bat = $o_bcpc->processBankAccountTransaction( $o_bat );
        
        if( $o_bat->isStatus( BankAccountTransaction::$XS_BAT_STATUS_FAILED ) ) {
            //Transaction Failed, look at messages array for reason(s) why
            var_dump( $o_bat->getMessages() );
        } else if( $o_bat->isStatus( BankAccountTransaction::$XS_BAT_STATUS_CANCELED ) ) {
            //Transaction was successfully canceled
            var_dump( $o_bat->getBankAccountTransactionId() );
        }
            
    }
    
    //tests settlement batch methods
    public function testSettlementBatch() {
        
        $o_sb = new SettlementBatch();
        
        $o_sb->setBATCreditAmount( 33.12 );
        $o_sb->setBATCreditCount( 33 );
        $o_sb->setBATDebitAmount( 12.12 );
        $o_sb->setBATDebitCount( 12 );
        $o_sb->setBCTCreditAmount( 44.44 );
        $o_sb->setBCTCreditCount( 44 );
        $o_sb->setBCTSaleAmount( 52.23 );
        $o_sb->setBCTSaleCount( 52 );
        
        $o_sb->addBankAccountTransactionID( 1 );
        $o_sb->addBankAccountTransactionID( 2 );
        $o_sb->addBankAccountTransactionID( 3 );
        $o_sb->addBankAccountTransactionID( 4 );
        $o_sb->addBankAccountTransactionID( 5 );
        $o_sb->addBankCardTransactionID( 11 );
        $o_sb->addBankCardTransactionID( 12 );
        $o_sb->addBankCardTransactionID( 13 );
        $o_sb->addBankCardTransactionID( 14 );
     
        var_dump( $o_sb->getJSON() );
        
    }
    
}


