<?php

/**
 * Contains fields for a RecurringTransaction
 *
 */
class RecurringTransaction {
    
    static $XS_FREQUENCY_ANNUALLY = "ANNUALLY";
    static $XS_FREQUENCY_QUARTERLY = "QUARTERLY";
    static $XS_FREQUENCY_MONTHLY = "MONTHLY";
    static $XS_FREQUENCY_BIWEEKLY = "BIWEEKLY";
    static $XS_FREQUENCY_WEEKLY = "WEEKLY";
    static $XS_FREQUENCY_DAILY = "DAILY";
    
    static $XS_RECURRING_STATUS_ENABLED = "RECURRINGENABLED";
    static $XS_RECURRING_STATUS_FAILED = "RECURRINGFAILED";
    static $XS_RECURRING_STATUS_DISABLED = "RECURRINGDISABLED";
    static $XS_RECURRING_STATUS_COMPLETED ="RECURRINGCOMPLETED";
    
    private $in_recurring_transaction_id;
    private $io_bank_card;
    private $io_bank_account;
    private $is_frequency;
    private $io_start_date;
    private $io_end_date;
    private $id_amount;
    private $io_messages;
    private $is_status;
    private $is_method;
    private $is_type;
    
    // BC-4246 : Variable added for Custom fields
    private $is_custom_field1;
    private $is_custom_field2;
    private $is_custom_field3;
    private $is_custom_field4;
    private $is_custom_field5;
    private $is_custom_field6;
    private $is_custom_field7;
    private $is_custom_field8;
    private $is_custom_field9;
    private $is_custom_field10;
    
    /**
     * Default Constructor.
     * 
     */
    function __construct() {
        $this->io_messages = array();
    }
    
    /**
     * Returns a array of error messages
     * 
     * @return array of messages
     */
    public function getMessages() {
        return $this->io_messages;
    }
    
    /**
     * Adds an error messages to he array
     * 
     * @param String $vs_message the message
     */
    public function addMessage( $vs_message ) {
        array_push( $this->io_messages, $vs_message );
    }
    
    /**
     * Returns the Recurring Transaction ID
     * 
     * @return int the Recurring Transaction ID
     */
    public function getRecurringTransactionID() {
        return $this->in_recurring_transaction_id;
    }
    
    /**
     * Set the Recurring Transaction ID
     * 
     * @param int $vn_recurring_transaction_id the id
     */
    public function setRecurringTransactionID( $vn_recurring_transaction_id ) {
        $this->in_recurring_transaction_id = $vn_recurring_transaction_id;
    }
    
    /**
     * Return the BankCard associated with the Recurring Transaction
     * 
     * @return BankCard the BankCard associated with the Recurring Transaction
     */
    public function getBankCard() {
        return $this->io_bank_card;
    }

    /**
     * Sets the BankCard associated with the Recurring Transaction
     * 
     * @param BankCard $vo_bank_card BankCard associated with the Recurring Transaction
     */
    public function setBankCard( $vo_bank_card ) {
        $this->io_bank_card = $vo_bank_card;
    }
    
    /**
     * Return the BankAccount associated with the Recurring Transaction
     * 
     * @return BankAccount the BankAccount associated with the Recurring Transaction
     */
    public function getBankAccount() {
        return $this->io_bank_account;
    }

    /**
     * Sets the BankAccount associated with the Recurring Transaction
     * 
     * @param BankAccount $vo_bank_account BankAccount associated with the Recurring Transaction
     */
    public function setBankAccount( $vo_bank_account ) {
        $this->io_bank_account = $vo_bank_account;
    }
    
    /**
     * Returns the frequency of the Recurring Transaction
     * Will always use of the following defined public static variables contained in this class.
     * $XS_FREQUENCY_ANNUALLY
     * $XS_FREQUENCY_QUARTERLY
     * $XS_FREQUENCY_MONTHLY
     * $XS_FREQUENCY_BIWEEKLY
     * $XS_FREQUENCY_WEEKLY
     * $XS_FREQUENCY_DAILY
     * 
     * @return String the frequency
     */
    public function getFrequency() {
        return $this->is_frequency;
    }
    
    /**
     * Sets the frequency of the Recurring Transaction
     * You should always use of the following defined public static variables contained in this class.
     * $XS_FREQUENCY_ANNUALLY
     * $XS_FREQUENCY_QUARTERLY
     * $XS_FREQUENCY_MONTHLY
     * $XS_FREQUENCY_BIWEEKLY
     * $XS_FREQUENCY_WEEKLY
     * $XS_FREQUENCY_DAILY
     * 
     * @param String $vs_frequency the frequency
     */
    public function setFrequency( $vs_frequency ) {
        $this->is_frequency = $vs_frequency;
    }
    
    /**
     * Returns the start date of the Recurring Transaction
     * 
     * @return Date the start date
     */
    public function getStartDate() {
        return $this->io_start_date;
    }
    
    /**
     * Sets the start date of the Recurring Transaction
     * 
     * @param Date $vo_start_date the start date
     */
    public function setStartDate( $vo_start_date ) {
        //if the string passed in is in format m/d/Y else assuming its in format m-d-Y
        if( PHP_VERSION < 5.3 ) {
            if( strpos($vo_start_date, '/') !== FALSE ) {
                $this->io_start_date = $this->date_create_from_format('m/d/Y', $vo_start_date )->format('m/d/Y');
            } else {
                $this->io_start_date = $this->date_create_from_format('m-d-Y', $vo_start_date )->format('m/d/Y');
            }
        } else {
            if( strpos($vo_start_date, '/') !== FALSE ) {
                $this->io_start_date = DateTime::createFromFormat('m/d/Y', $vo_start_date )->format('m/d/Y');
            } else {
                $this->io_start_date = DateTime::createFromFormat('m-d-Y', $vo_start_date )->format('m/d/Y');
            }
        }
    }
    
    /**
     * Returns the end date of the Recurring Transaction
     * 
     * @return Date the end date
     */
    public function getEndDate() {
        return $this->io_end_date;
    }
    
    /**
     * Sets the end date of the Recurring Transaction
     * 
     * @param Date $vo_end_date the end
     */
    public function setEndDate( $vo_end_date ) {
        //if the string passed in is in format m/d/Y else assuming its in format m-d-Y
        if( PHP_VERSION < 5.3 ) {
            if( strpos($vo_end_date, '/') !== FALSE ) {
                $this->io_end_date = $this->date_create_from_format('m/d/Y', $vo_end_date )->format('m/d/Y');
            } else {
                $this->io_end_date = $this->date_create_from_format('m-d-Y', $vo_end_date )->format('m/d/Y');
            }
        } else {
            if( strpos($vo_end_date, '/') !== FALSE ) {
                $this->io_end_date = DateTime::createFromFormat('m/d/Y', $vo_end_date )->format('m/d/Y');
            } else {
                $this->io_end_date = DateTime::createFromFormat('m-d-Y', $vo_end_date )->format('m/d/Y');
            }
        }
    }
    
    /**
     * Returns the amount that will be charged each time the Recurring Transaction is run
     * 
     * @return double the amount
     */
    public function getAmount() {
        return $this->id_amount;
    }
    
    /**
     * Sets the amount that will be charged each time the Recurring Transaction is run
     * 
     * @param double $vd_amount the amount
     */
    public function setAmount( $vd_amount ) {
        $this->id_amount = $vd_amount;
    }
    
    /**
     * Returns the status of the Recurring Transaction.
     * Will be one of the public static variables defined in this class
     * $XS_RECURRING_STATUS_ENABLED
     * $XS_RECURRING_STATUS_FAILED
     * $XS_RECURRING_STATUS_DISABLED
     * 
     * @return String the status
     */
    public function getStatus() {
        return $this->is_status;
    }
    
    /**
     * Sets the status of the Recurring Transaction.
     * Will be one of the public static variables defined in this class
     * $XS_RECURRING_STATUS_ENABLED
     * $XS_RECURRING_STATUS_FAILED
     * $XS_RECURRING_STATUS_DISABLED
     * 
     * @param String $vs_status the status
     */
    public function setStatus( $vs_status ) {
        $this->is_status = $vs_status;
    }
    
    /**
     * Returns the method of the Recurring Transaction. Only used when a BankAccount is attached to the RecurringTransaction.
     * 
     * @return String the method
     */
    public function getMethod() {
        return $this->is_method;
    }
    
    /**
     * Sets the method of the Recurring Transaction. Only used when a BankAccount is attached to the RecurringTransaction.
     * Should use one of the static defined variables on the BankAccountTransaction class for the Method value.
     * 
     * @param String $vs_method the method
     */
    public function setMethod( $vs_method ) {
        $this->is_method = $vs_method;
    }
    
    /**
     * Returns the type of the Recurring Transaction. Only used when a BankAccount is attached to the RecurringTransaction.
     * 
     * @return String the type
     */
    public function getType() {
        return $this->is_type;
    }
    
    /**
     * Sets the type of the Recurring Transaction. Only used when a BankAccount is attached to the RecurringTransaction.
     * Should use one of the static defined variables on the BankAccountTransaction class for the Type value.
     * 
     * @param String $vs_type the type
     */
    public function setType( $vs_type ) {
        $this->is_type = $vs_type;
    }
    
    /**
     * Sets the custom field 1 on the Recurring Transaction
     * @param $vs_custom_field1 custom field 1
     */
    public function setCustomField1($vs_custom_field1) {
        $this->is_custom_field1 = $vs_custom_field1;
    }

    /**
     * Returns the custom field 1 on the Recurring Transaction
     * @return the custom field 1
     */
    public function getCustomField1() {
        return $this->is_custom_field1;
    }
    
    /**
     * Sets the custom field 2 on the Recurring Transaction
     * @param $vs_custom_field2 custom field 2
     */
    public function setCustomField2($vs_custom_field2) {
        $this->is_custom_field2 = $vs_custom_field2;
    }

    /**
     * Returns the custom field 2 on the Recurring Transaction
     * @return the custom field 2
     */
    public function getCustomField2() {
        return $this->is_custom_field2;
    }
    
    /**
     * Sets the custom field 3 on the Recurring Transaction
     * @param $vs_custom_field3 custom field 3
     */
    public function setCustomField3($vs_custom_field3) {
        $this->is_custom_field3 = $vs_custom_field3;
    }

    /**
     * Returns the custom field 3 on the Recurring Transaction
     * @return the custom field 3
     */
    public function getCustomField3() {
        return $this->is_custom_field3;
    }
    
    /**
     * Sets the custom field 4 on the Recurring Transaction
     * @param $vs_custom_field4 custom field 4
     */
    public function setCustomField4($vs_custom_field4) {
        $this->is_custom_field4 = $vs_custom_field4;
    }

    /**
     * Returns the custom field 4 on the Recurring Transaction
     * @return the custom field 4
     */
    public function getCustomField4() {
        return $this->is_custom_field4;
    }
    
    /**
     * Sets the custom field 5 on the Recurring Transaction
     * @param $vs_custom_field5 custom field 5
     */
    public function setCustomField5($vs_custom_field5) {
        $this->is_custom_field5 = $vs_custom_field5;
    }

    /**
     * Returns the custom field 5 on the Recurring Transaction
     * @return the custom field 5
     */
    public function getCustomField5() {
        return $this->is_custom_field5;
    }
    
    /**
     * Sets the custom field 6 on the Recurring Transaction
     * @param $vs_custom_field6 custom field 6
     */
    public function setCustomField6($vs_custom_field6) {
        $this->is_custom_field6 = $vs_custom_field6;
    }

    /**
     * Returns the custom field 6 on the Recurring Transaction
     * @return the custom field 6
     */
    public function getCustomField6() {
        return $this->is_custom_field6;
    }
    
    /**
     * Sets the custom field 7 on the Recurring Transaction
     * @param $vs_custom_field7 custom field 7
     */
    public function setCustomField7($vs_custom_field7) {
        $this->is_custom_field7 = $vs_custom_field7;
    }

    /**
     * Returns the custom field 7 on the Recurring Transaction
     * @return the custom field 7
     */
    public function getCustomField7() {
        return $this->is_custom_field7;
    }
    
    /**
     * Sets the custom field 8 on the Recurring Transaction
     * @param $vs_custom_field8 custom field 8
     */
    public function setCustomField8($vs_custom_field8) {
        $this->is_custom_field8 = $vs_custom_field8;
    }

    /**
     * Returns the custom field 8 on the Recurring Transaction
     * @return the custom field 8
     */
    public function getCustomField8() {
        return $this->is_custom_field8;
    }
    
    /**
     * Sets the custom field 9 on the Recurring Transaction
     * @param $vs_custom_field9 custom field 9
     */
    public function setCustomField9($vs_custom_field9) {
        $this->is_custom_field9 = $vs_custom_field9;
    }

    /**
     * Returns the custom field 9 on the Recurring Transaction
     * @return the custom field 9
     */
    public function getCustomField9() {
        return $this->is_custom_field9;
    }
    
    /**
     * Sets the custom field 10 on the Recurring Transaction
     * @param $vs_custom_field10 custom field 10
     */
    public function setCustomField10($vs_custom_field10) {
        $this->is_custom_field10 = $vs_custom_field10;
    }

    /**
     * Returns the custom field 10 on the Recurring Transaction
     * @return the custom field 10
     */
    public function getCustomField10() {
        return $this->is_custom_field10;
    }
    
    /**
     * Builds a RecurringTransaction PHP object from the passed in json object.
     * 
     * @param json $vo_json - The json representation of the object to be built.
     * @return RecurringTransaction a RecurringTransactionobject with fields based on the input json object.
     */
    static function buildFromJSON( $vo_json ) {
        
        $o_recurring_transaction = new RecurringTransaction();
        
        if(array_key_exists( "recurring_transaction_id", $vo_json ) ) { $o_recurring_transaction->setRecurringTransactionID( $vo_json[ "recurring_transaction_id" ] ); }
        if(array_key_exists( "bank_card", $vo_json ) ) { $o_recurring_transaction->setBankCard( BankCard::buildFromJSON( $vo_json[ "bank_card" ] ) ); }
        if(array_key_exists( "bank_account", $vo_json ) ) { $o_recurring_transaction->setBankAccount( BankAccount::buildFromJSON( $vo_json[ "bank_account" ] ) ); }
        if(array_key_exists( "recurring_transaction_frequency", $vo_json ) ) { $o_recurring_transaction->setFrequency( $vo_json[ "recurring_transaction_frequency" ] ); }
        if(array_key_exists( "recurring_transaction_method", $vo_json ) ) { $o_recurring_transaction->setMethod( $vo_json[ "recurring_transaction_method" ] ); }
        if(array_key_exists( "recurring_transaction_type", $vo_json ) ) { $o_recurring_transaction->setType( $vo_json[ "recurring_transaction_type" ] ); }
        if(array_key_exists( "recurring_transaction_start_date", $vo_json ) ) { $o_recurring_transaction->setStartDate( date("m/d/Y", strtotime( $vo_json[ "recurring_transaction_start_date" ] ) ) ); }
        if(array_key_exists( "recurring_transaction_end_date", $vo_json ) ) { $o_recurring_transaction->setEndDate( date("m/d/Y", strtotime( $vo_json[ "recurring_transaction_end_date" ] ) ) ); }
        if(array_key_exists( "recurring_transaction_amount", $vo_json ) ) { $o_recurring_transaction->setAmount( $vo_json[ "recurring_transaction_amount" ] ); }
        
        if(array_key_exists( "recurring_transaction_status", $vo_json ) ) { 
            if( gettype( $vo_json[ 'recurring_transaction_status' ] ) === "array" ){
                $o_status = (array)$vo_json[ 'recurring_transaction_status' ];
                if (array_key_exists( 'transaction_status_name', $o_status ) ) {
                    $o_recurring_transaction->setStatus( $o_status[ 'transaction_status_name' ] );
                }
            }
            else {
                $o_recurring_transaction->setStatus( $vo_json[ 'recurring_transaction_status' ] );
            }
        }
        
        if( array_key_exists( 'messages', $vo_json  ) && !is_null( $vo_json[ 'messages' ] ) ) {
            foreach ( $vo_json['messages'] as $s_key => $s_msg ) {
                $o_recurring_transaction->addMessage( $s_msg );
            }
        }
        
        // BC-4246 : Added statements for custom fields
        if(array_key_exists("recurring_transaction_custom_field1", $vo_json) ) { $o_recurring_transaction->setCustomField1($vo_json["recurring_transaction_custom_field1"]); }
        if(array_key_exists("recurring_transaction_custom_field2", $vo_json) ) { $o_recurring_transaction->setCustomField2($vo_json["recurring_transaction_custom_field2"]); }
        if(array_key_exists("recurring_transaction_custom_field3", $vo_json) ) { $o_recurring_transaction->setCustomField3($vo_json["recurring_transaction_custom_field3"]); }
        if(array_key_exists("recurring_transaction_custom_field4", $vo_json) ) { $o_recurring_transaction->setCustomField4($vo_json["recurring_transaction_custom_field4"]); }
        if(array_key_exists("recurring_transaction_custom_field5", $vo_json) ) { $o_recurring_transaction->setCustomField5($vo_json["recurring_transaction_custom_field5"]); }
        if(array_key_exists("recurring_transaction_custom_field6", $vo_json) ) { $o_recurring_transaction->setCustomField6($vo_json["recurring_transaction_custom_field6"]); }
        if(array_key_exists("recurring_transaction_custom_field7", $vo_json) ) { $o_recurring_transaction->setCustomField7($vo_json["recurring_transaction_custom_field7"]); }
        if(array_key_exists("recurring_transaction_custom_field8", $vo_json) ) { $o_recurring_transaction->setCustomField8($vo_json["recurring_transaction_custom_field8"]); }
        if(array_key_exists("recurring_transaction_custom_field9", $vo_json) ) { $o_recurring_transaction->setCustomField9($vo_json["recurring_transaction_custom_field9"]); }
        if(array_key_exists("recurring_transaction_custom_field10", $vo_json) ) { $o_recurring_transaction->setCustomField10($vo_json["recurring_transaction_custom_field10"]); }

        return $o_recurring_transaction;
        
    }
    
    /**
     * Returns the JSON representation of the RecurringTransaction.
     * 
     * @return associated array : the json representation
     */
    public function getJSON() {
        
        $o_json = array();
        
        if ( !is_null( $this->in_recurring_transaction_id ) ) { $o_json[ "recurring_transaction_id" ] = $this->in_recurring_transaction_id; }
        if ( !is_null( $this->io_bank_card ) ) { $o_json[ "bank_card" ] = json_decode( $this->getBankCard()->getJSON() ); }
        if ( !is_null( $this->io_bank_account ) ) { $o_json[ "bank_account" ] = json_decode( $this->getBankAccount()->getJSON() ); }
        if ( !is_null( $this->is_frequency ) ) { $o_json[ "recurring_transaction_frequency" ] = $this->is_frequency; }
        if ( !is_null( $this->is_method ) ) { $o_json[ "recurring_transaction_method" ] = $this->is_method; }
        if ( !is_null( $this->is_type ) ) { $o_json[ "recurring_transaction_type" ] = $this->is_type; }
        if ( !is_null( $this->io_start_date ) ) { $o_json[ "recurring_transaction_start_date" ] = $this->io_start_date; }
        if ( !is_null( $this->io_end_date ) ) { $o_json[ "recurring_transaction_end_date" ] = $this->io_end_date; }
        if ( !is_null( $this->id_amount ) ) { $o_json[ "recurring_transaction_amount" ] = $this->id_amount; }
        if ( !is_null( $this->is_status ) ) { $o_json[ "recurring_transaction_status" ] = $this->is_status; }
        
        if( !is_null( $this->io_messages ) ){ $o_json[ 'messages' ]  =  $this->io_messages; }
        
        // BC-4246 : Added statement for Custom Field
        if ( !is_null( $this->is_custom_field1 ) ) { $o_json[ "recurring_transaction_custom_field1" ] = $this->is_custom_field1; }
        if ( !is_null( $this->is_custom_field2 ) ) { $o_json[ "recurring_transaction_custom_field2" ] = $this->is_custom_field2; }
        if ( !is_null( $this->is_custom_field3 ) ) { $o_json[ "recurring_transaction_custom_field3" ] = $this->is_custom_field3; }
        if ( !is_null( $this->is_custom_field4 ) ) { $o_json[ "recurring_transaction_custom_field4" ] = $this->is_custom_field4; }
        if ( !is_null( $this->is_custom_field5 ) ) { $o_json[ "recurring_transaction_custom_field5" ] = $this->is_custom_field5; }
        if ( !is_null( $this->is_custom_field6 ) ) { $o_json[ "recurring_transaction_custom_field6" ] = $this->is_custom_field6; }
        if ( !is_null( $this->is_custom_field7 ) ) { $o_json[ "recurring_transaction_custom_field7" ] = $this->is_custom_field7; }
        if ( !is_null( $this->is_custom_field8 ) ) { $o_json[ "recurring_transaction_custom_field8" ] = $this->is_custom_field8; }
        if ( !is_null( $this->is_custom_field9 ) ) { $o_json[ "recurring_transaction_custom_field9" ] = $this->is_custom_field9; }
        if ( !is_null( $this->is_custom_field10 ) ) { $o_json[ "recurring_transaction_custom_field10" ] = $this->is_custom_field10; }       
        
        return json_encode($o_json);
        
    }
    
    function date_create_from_format( $dformat, $dvalue ) {

        $schedule = $dvalue;
        $schedule_format = str_replace(array('Y','m','d', 'H', 'i','a'),array('%Y','%m','%d', '%I', '%M', '%p' ) ,$dformat);
        // %Y, %m and %d correspond to date()'s Y m and d.
        // %I corresponds to H, %M to i and %p to a
        $ugly = strptime($schedule, $schedule_format);
        $ymd = sprintf(
            // This is a format string that takes six total decimal
            // arguments, then left-pads them with zeros to either
            // 4 or 2 characters, as needed
            '%04d-%02d-%02d %02d:%02d:%02d',
            $ugly['tm_year'] + 1900,  // This will be "111", so we need to add 1900.
            $ugly['tm_mon'] + 1,      // This will be the month minus one, so we add one.
            $ugly['tm_mday'], 
            $ugly['tm_hour'], 
            $ugly['tm_min'], 
            $ugly['tm_sec']
        );
        $new_schedule = new DateTime($ymd);

       return $new_schedule;
    }
    
}
