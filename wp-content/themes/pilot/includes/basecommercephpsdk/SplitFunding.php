<?php

include_once 'FundingInstructions.php';

/**
 * SplitFunding is a simple re-usable entity class that defines attributes of a SplitFunding.
 *
 */
class SplitFunding extends FundingInstructions {

    public $id_amount;
    public $is_bank_account_token;
    public $is_type;

    /**
     * 
     * Default Constructor.
     */
    function __construct() {
        $this->is_type = "SPLIT_FUNDING";
    }

    private function setType($vs_type) {
        $this->is_type = $vs_type;
    }

    /**
     * Gets the transaction type
     * 
     */
    public function getType() {
        return $this->is_type;
    }

    /**
     * Sets the amount
     * 
     * @param vn_amount 
     */
    public function setAmount($vd_amount) {
        $this->id_amount = $vd_amount;
    }

    /**
     * Gets the amount
     * 
     */
    public function getAmount() {
        return $this->id_amount;
    }

    /**
     * Sets the Bank Account Token
     * 
     * param vs_bank_account_token
     */
    public function setBankAccountToken($vs_bank_account_token) {
        $this->is_bank_account_token = $vs_bank_account_token;
    }

    /**
     * Gets the Bank Account Token
     * 
     */
    public function getBankAccountToken() {
        return $this->is_bank_account_token;
    }

    /**
     * 
     * Builds and Returns an SplitFunding object based off of the JSON input.
     * @param vo_json JSON representation of an SplitFunding
     * @return  the SplitFunding
     */
    public static function buildFromJSON($vo_json) {
        $split_funding = new SplitFunding();

        if (array_key_exists("split_funding_amount", $vo_json)) {
            $split_funding->setAmount($vo_json["split_funding_amount"]);
        }
        if (array_key_exists("bank_account_token", $vo_json)) {
            $split_funding->setBankAccountToken($vo_json["bank_account_token"]);
        }
        if (array_key_exists("funding_instruction_type", $vo_json)) {
            $split_funding->setType(array_shift($vo_json['funding_instruction_type']));
        }

        return $split_funding;
    }

    /**
     * 
     * Returns a JSON representation of the SplitFunding
     * @return  the JSON representation
     */
    function getJSON() {

        $o_json = array();

        $o_json['split_funding_amount'] = $this->id_amount;
        $o_json['bank_account_token'] = $this->is_bank_account_token;
        $o_json['funding_instruction_type'] = $this->is_type;

        return json_encode($o_json);
    }

}
