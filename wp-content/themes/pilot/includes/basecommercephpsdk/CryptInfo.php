<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CryptInfo
 *
 */
class CryptInfo {

    private $is_ksn;
    private $is_cipherText;
    private $is_plainText;
    
    function __construct() {
        $this->io_messages = array();   
    }

    public function getKsn() {
        return $this->is_ksn;
    }

    public function setKsn($vs_ksn) {
        $this->is_ksn = $vs_ksn;
    }

    public function getCipherText() {
        return $this->is_cipherText;
    }

    public function setCipherText($vs_cipherText) {
        $this->is_cipherText = $vs_cipherText;
    }

    public function getPlainText() {
        return $this->is_plainText;
    }

    public function setPlainText($vs_plainText) {
        $this->is_plainText = $vs_plainText;
    }
    
    public function getMessages() {
        return $this->io_messages;
    }
    
    public function addMessage( $vs_error ) {
        array_push( $this->io_messages, $vs_error );
    }

    
    /***
     * builds the CryptInfo object from the given json object
     * 
     * @param $o_data the json object representation of a CryptInfo
     * @return  The constructed CryptInfo
     */
    static function buildFromJSON($o_data) {

        $o_instance = new CryptInfo();
        if ($o_data != NULL) {
            if (array_key_exists("ksn", $o_data) && $o_data["ksn"] !== NULL) {
                $o_instance->setKsn($o_data['ksn']);
            }
            if (array_key_exists("cipher_text", $o_data) && $o_data["cipher_text"] !== NULL) {
                $o_instance->setCipherText($o_data['cipher_text']);
            }
            if (array_key_exists("plain_text", $o_data) && $o_data["plain_text"] !== NULL) {
                $o_instance->setPlainText($o_data['plain_text']);
            }
            
           // Added to allow intercommunication between CLIENT objects during unit testing//
           if( array_key_exists( 'messages', $o_data  ) && !is_null( $vo_json[ 'messages' ] ) ) {
            foreach ( $o_data['messages'] as $s_key => $s_msg ) {  
                $o_instance->addMessage( $s_msg );
            }    
        }
        }
        return $o_instance;
    }

    /**
     * Returns the JSON representation of the CryptInfo.
     * @return associated array : the json representation
     */
    function getJSON() {

        $o_json = array();

        if (!is_null($this->is_ksn)) {
            $o_json["ksn"] = $this->is_ksn;
        }

        if (!is_null($this->is_cipherText)) {
            $o_json["cipher_text"] = $this->is_cipherText;
        }

        if (!is_null($this->is_plainText)) {
            $o_json["plain_text"] = $this->is_plainText;
        }
        
        if( !is_null( $this->io_messages ) ){           
            $o_json[ 'messages' ] = $this->io_messages;         
        }
        
        return json_encode($o_json);
    }

}
