<?php
	/**
	 * string $args[0]['title']
	 * string $args[0]['subtitle']
	 * string $args[0]['content']
	 * string $args[0]['bg_image_url']
	 * string $args[0]['overlay_opacity']			// from 0 to 1
	 * string $args[0]['overlay_color']				// hex value #010101
	 * string $args[0]['column_image_url']
	 */
	global $args;
?>

<?php if( isset( $args['bg_image_url'] ) ) : ?>
	<div class="bg-image" style="background-image: url(<?php echo $args['bg_image_url']; ?>);">
		<div class="img-overlay" style="background-color: <?php echo $args['overlay_color']; ?>; opacity: <?php echo $args['overlay_opacity']; ?>;"></div>
<div class="container">     
<div class="column_image">
<div class="wow fadeIn" data-wow-duration="1s">
<?php if( $args['column_image_url']!='' ) : ?> 
<img class="col-image" src="<?php echo $args['column_image_url']; ?>">
<?php endif; ?> 
</div>
</div>

<div class="title">
<div class="wow" data-wow-duration="1s">
	<div class="title-wrap">
		<?php if( isset( $args['title'] ) ) : ?>
			<h3><?php echo $args['title']; ?></h3>
		<?php endif; ?>
		<?php if( isset( $args['logo'] ) ) : ?>
			<img src="<?php echo $args['logo']['url']; ?>">
		<?php endif; ?>
		<span class="subtitle"><?php echo $args['subtitle']; ?></span>
		<div class="content"><?php echo $args['content']; ?></div>
	</div>
</div>
</div>
</div><!--#End container here-->
	</div>
<?php else : ?>
	<div class="bg-image no_bg">
		<div class="img-overlay" style="background-color: <?php echo $args['overlay_color']; ?>; opacity: <?php echo $args['overlay_opacity']; ?>;"></div>
<div class="container">    

    
<div class="column_image">
<div class="wow fadeInUp" data-wow-duration="1s">
<img class="col-image" src="<?php echo $args['column_image_url']; ?>">
</div>
</div>

<div class="title">
<div class="wow fadeInUp" data-wow-duration="1s">
	<div class="title-wrap">
		<?php if( isset( $args['title'] ) ) : ?>
			<h3><?php echo $args['title']; ?></h3>
		<?php endif; ?>
		<?php if( isset( $args['logo'] ) ) : ?>
			<img src="<?php echo $args['logo']['url']; ?>">
		<?php endif; ?>
		<span class="subtitle"><?php echo $args['subtitle']; ?></span>
		<div class="content"><?php echo $args['content']; ?></div>
	</div>
</div>
</div>

</div>
	</div>
<?php endif; ?>

