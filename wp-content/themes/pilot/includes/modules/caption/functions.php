<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}

	function build_caption_layout(){
		global $i;
		$suffixes = array( '' );
		$block_width = get_sub_field('caption_block_width');
		if( $block_width == 'half_width' ){
			$suffixes[] = '_second';
		}
		foreach( $suffixes as $suffix ){
			$custom_classes = " " . preg_replace('/_/','-',$block_width) . " " . get_sub_field('caption_block_custom_class');
			$caption_args = array(
				'width' => $block_width,
				'width_class' => preg_replace('/_/','-',$block_width),
				'id' => 'caption_block_'.$i,
				'overlay_color' => '',
				'overlay_opacity' => '',
				'title' => get_sub_field('caption_block_title'.$suffix)
			);
			if( get_sub_field('caption_block_modify'.$suffix) ){
				if( $opacity = get_sub_field('caption_block_overlay_opacity'.$suffix) ){
					$caption_args['overlay_opacity'] = $opacity;
				}
				if( $color = get_sub_field('caption_block_overlay_color'.$suffix) ){
					$caption_args['overlay_color'] = $color;
				}
			}				
			$image = get_sub_field('caption_block_image'.$suffix);
			if( is_array( $image ) ){
				$caption_args['image_url'] = $image['url'];
			}
			$args[] = $caption_args;
		}
		if( is_array( $args ) ){
			return $args;
		}
	}
?>