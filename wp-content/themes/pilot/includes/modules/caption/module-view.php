<?php
	/**
	 * $args = array() 								// array of caption blocks - if count($args) == 2 is a double;
	 * string $args[0]['image_url']
	 * string $args[0]['id']
	 * string $args[0]['width']						// get_block() sets class on wrapper div: full_width, half_width_center (half width, one per row), half_width_float (half_width_float: user is responsible for adding appropriate number of blocks)
	 * string $args[0]['overlay_opacity']			// from 0 to 1
	 * string $args[0]['overlay_color']				// hex value #010101
	 * string $args[0]['title']
	 *
	 */
	global $args;	
?>
<div class="container">
	<?php foreach ( $args as $arg ) : ?>
		<?php if( isset( $arg['image_url'] ) ) : ?>
			<div class="wow fadeInUp caption-block-wrap caption-<?php echo $arg['width_class']; ?> <?php echo $arg['width_class']; ?>" id="<?php echo $arg['id']; ?>">
				<div class="caption-title <?php if( $arg['title'] ){ echo 'has-caption'; }?>">
					<?php echo $arg['title']; ?>
				</div><!--/caption-title-->
				<img src="<?php echo $arg['image_url']; ?>">
			</div><!--img-block-wrap-->
		<?php endif; ?>		
	<?php endforeach; ?>
</div><!--/container-->