<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_three_column_image_grid_layout(){
		$args = array(
			'title' => get_sub_field('three_column_image_grid_block_title'),
			'images' => get_sub_field('images')
		);
		return $args;
	}

?>