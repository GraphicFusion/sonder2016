jQuery(document).ready( function ($) {
	$('.bg-image').on('mouseover', function () {
		$('.bg-image').not($(this)).addClass('img-fade');
	});

	$('.bg-image').on('mouseout', function () {
		$('.bg-image').removeClass('img-fade');
	});
});