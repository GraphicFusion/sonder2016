<?php 
	/**
	 * string	$args['title']
	 * array	$args['images']
	 * array	$args['images'][0]['image'] 		// an acf image array
	 * string	$args['images'][0]['title']		
	 * string	$args['images'][0]['link']			// an internal url (validated, absolute path)
	 * string	$args['images'][0]['custom_link']	// an unvalidated url (could be relative)		
	 */

	global $args; 
?>

<?php if( count($args['images']) > 0 ) : ?>

	<div class="container">
    <div class="three-column-image-grid-block wow fadeIn">
    
    <h3><?php echo $args['title']; ?></h3>
    	
		<?php if( is_array( $args['images'] ) ) : ?>
			<?php foreach( $args['images'] as $key => $image ): ?>
        <div class="wow fadeInUp" data-wow-duration="2s">
					<div class="wrapper <?php echo "col_".$key;?>">
					
						<?php /*?><div class="bg-image" style="background-image: url(<?php echo $image['image']['url']; ?>)"><?php */?>
            <div class="bg-image">
              <a href="<?php echo ($image['custom_link'] ? $image['custom_link'] : $image['link']); ?>">
                <img src="<?php echo $image['image']['url']; ?>" alt="" />	
								<?php if( $image['title'] ) : // if has text, add text box ?>
									<div class="image-text">
										<span class="title"><?php echo $image['title']; ?></span>
									</div>
								<?php endif; ?>
              </a>
            </div>
					</div>
        </div>
			<?php endforeach; ?>
		<?php endif; ?>
        </div>
	</div>
<?php endif; ?>