<?php 
	/**
	 * string	$args['title']
	 * array	$args['images']
	 * array	$args['images'][0]['image'] 		// an acf image array
	 * string	$args['images'][0]['title']		
	 * string	$args['images'][0]['subtitle']		
	 * string	$args['images'][0]['content']		
	 * string	$args['images'][0]['link']			// an internal url (validated, absolute path)
	 * string	$args['images'][0]['custom_link']	// an unvalidated url (could be relative)		
	 */

	global $args; 
?>
<h3><?php echo $args['title']; ?></h3>
<?php if( count($args['images']) > 0 ) : ?>

	<div class="two-column-image-block">
    <div class="container">
		<?php if( is_array( $args['images'] ) ) : ?>
			<?php foreach( $args['images'] as $key => $image ): ?>
            	<div class="wow fadeInUp" data-wow-duration="2s">
				<div class="wrapper <?php echo "col_".$key;?>">
					<a href="<?php echo ($image['custom_link'] ? $image['custom_link'] : $image['link']); ?>">
                    
						<?php /*?><div class="bg-image" style="background-image: url(<?php echo $image['image']['url']; ?>)"></div><?php */?>
                        	
                       <?php if( $image['image']['url']!='' ) : ?> 
                        
                        <img src="<?php echo $image['image']['url']; ?>" alt="" />
                       <?php endif; ?> 					
						<?php if( $image['title'] || $image['subtitle'] || $image['content'] ) : // if has text, add text box ?>
							<div class="image-text">
								<span class="title"><?php echo $image['title']; ?></span>
								<span class="subtitle"><?php echo $image['subtitle']; ?></span>					
								<span class="content"><?php echo $image['content']; ?></span>					
							</div>
						<?php endif; ?>
                        
					</a>
				</div>
                </div>
			
			<?php endforeach; ?>
		<?php endif; ?>
        </div>
	</div>
<?php endif; ?>