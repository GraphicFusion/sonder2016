<?php
	$filename = get_template_directory() . '/includes/modules/' . $module . '/module_layout_acf_def.php';
	if( file_exists ( $filename )){
		require $filename;
	}
	function build_two_column_image_layout(){
		$args = array(
			'title' => get_sub_field('two_column_image_block_title'),
			'images' => get_sub_field('image_blocks')
		);
		return $args;
	}


?>