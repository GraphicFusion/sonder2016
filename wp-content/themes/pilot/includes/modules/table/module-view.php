<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 * array	$args['table']
	 * string	$args['table']['left_text'] 
	 * string	$args['table']['right_text'] 
	 * string	$args['table']['link'] 
	 */
	global $args; 
?>
<h3><?php echo $args['title']; ?></h3>
<div class="wow fadeInUp" data-wow-duration="1s">
<div class="table-content"><?php echo $args['content']; ?></div>
</div>

<div class="wow fadeIn" data-wow-duration=".3s">
<?php if( count($args['table']) > 0 ) : ?>
	<div class="table-links">
		<?php if( is_array( $args['table'] ) ) : ?>
			<?php foreach( $args['table'] as $row ): ?>
				<div class="table-row">
					<span class="link-title wow fadeInRight"><?php echo $row['left_text']; ?></span>
					<span class="link-wrap wow fadeInRight"><a href="<?php echo $row['link']; ?>"><?php echo $row['right_text']; ?></a></span>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
<?php endif; ?>
</div>