<?php
	function pilot_get_title(){
		if (is_home()) {
			if (get_option('page_for_posts', true)) {
				return get_the_title(get_option('page_for_posts', true));
			}
			else {
				return __('Latest Posts', 'dorado');
			}
		} elseif (is_archive()) {
			return get_the_archive_title();
		}
		elseif (is_search()) {
			return sprintf(__('Search Results for %s', 'dorado'), get_search_query());
		}
		elseif (is_404()) {
			return __('Not Found', 'dorado');
		}
		else {
			return get_the_title();
		}
	}
	function pilot_get_view_format(){
		return;
	}
	function pilot_get_sidebar(){
		global $pilot;
		if( $pilot->sidebar ){
			get_sidebar();
		}
	}
	function pilot_get_comments(){
		global $pilot;
		if( $pilot->comments ){
			if ( comments_open() || get_comments_number() ){
				comments_template();
			}			
		}
	}
	function qcc_set_content_type(){
	    return "text/html";
	}
add_filter( 'gform_enable_credit_card_field', 'enable_creditcard', 11 );
function enable_creditcard( $is_enabled ) {
    return true;
}
add_action( 'gform_after_submission', 'sonder_cc_submission', 10, 2 );
function sonder_cc_submission($entry, $form){
	$testing = 0;
	if( $entry['form_id'] == 4 && !empty($_POST) ) :
	//test cc: 4222222222222
	global $output;
	$output = [];
	$output['card_num'] = "";
	$card_num = "";
	if( array_key_exists('input_1_1',$_POST) ){
		$card_num = $_POST['input_1_1'];
		$output['card_num'] =  substr($card_num, -4);
	}
	$output['card_name'] = "";
	if( array_key_exists('input_1_5',$_POST) ){
		$output['card_name'] = $_POST['input_1_5'];
	}
	$output['month'] = "";
	if( array_key_exists('input_1_2',$_POST) ){
		$output['month'] = $_POST['input_1_2'][0];
	}
	$output['year'] = "";
	if( array_key_exists('input_1_2',$_POST) ){
		$output['year'] = $_POST['input_1_2'][1];
	}
	$output['amount'] = "";
	if( array_key_exists('input_2',$_POST) ){
        $output['amount'] = preg_replace( '/[^0-9\.]/', '', $_POST['input_2']);
	}
	$output['cvv'] = "";
	if( array_key_exists('input_1_3',$_POST) ){
		$output['cvv'] = $_POST['input_1_3'];
	}
	$output['email'] = "";
	if( array_key_exists('input_3',$_POST) ){
		$output['email'] = $_POST['input_3'];
	}
	$base = get_template_directory().'/includes/basecommercephpsdk/';
	require $base.'BankCardTransaction.php';
	require $base.'BankAccountTransaction.php';
	require $base.'BaseCommerceClient.php';
	require $base.'BankCard.php';
	require $base.'BankAccount.php';
	require $base.'codeSamples.php';

	$o_bct = new BankCardTransaction();
    $o_bct->setType(BankCardTransaction::$XS_BCT_TYPE_SALE);

    $o_bct->setCardName($output['card_name']);
    $o_bct->setCardNumber($card_num);
    $o_bct->setCardExpirationMonth($output['month']);
    $o_bct->setCardExpirationYear($output['year']);
	$o_bct->setCardCVV2($output['cvv']);
    $o_bct->setAmount($output['amount']);

	if( $testing ){
		$username = "0011460001";
		$password = "5gypumhrXekEqni5VCiB";
		$key = "1FA48301766416D919E5BF4CD385BCF137F88F32A14C1526";
	}
	else{
		$username = "0031211001";
		$password = "vbAtrk8g8eNCqytJFMAt";
		$key = "AE92897CEAD9BAC813C4891C6E5E626480BC162F1AB96DD3";
	}

    $o_bcpc = new BaseCommerceClient( $username, $password, $key );
    //$o_bcpc->setSandbox( true );
    $o_bct = $o_bcpc->processBankCardTransaction( $o_bct );
	global $mssgs; 
	$mssgs = [];
    if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_FAILED ) ) {
        //Transaction Failed, look at messages array for reason(s) why
        $mssgs[] = $o_bct->getMessages();
    } else if( $o_bct->isStatus( BankCardTransaction::$XS_BCT_STATUS_DECLINED ) ) {
        //Transaction Declined, look at response code and response message
        $mssgs[] = $o_bct->getResponseCode();
        $mssgs[] = $o_bct->getResponseMessage();
    } else if( $o_bct->isStatus(BankCardTransaction::$XS_BCT_STATUS_CAPTURED) ) {
        //Transaction went through successfully

        $output['id'] = $o_bct->getTransactionID();
		$headers = "From: info@sonderagency.com" . "\r\n";
		add_filter( 'wp_mail_content_type','qcc_set_content_type' );
$content = '
<!DOCTYPE html
  PUBLIC "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title></title>
   </head>
   <body>
      <table style="width: 700px; margin: 0 auto; margin-top:50px; font-family: arial; font-size:16px; color: #333; border: 1px solid #C0C0C0; border-collapse:collapse; ">
         <tbody>
            <tr>
               <td style="text-align:center; padding:15px 0;"><img width="120" height="40" src="https://ccc.basecommercesandbox.com/email_templates/images/bc_logo_colored_small_400x120.png" alt=""></td>
            </tr>
            <tr style="center center no-repeat; background-color: #14426E;">
               <td style="text-align:center; padding:20px 0;">
                  <h1 style="color: #fff; font-size: 28px; letter-spacing:1px; font-weight: 300;">PAYMENT RECEIPT</h1>
               </td>
            </tr>
            <tr>
               <td style="text-align:center; padding:10px 0 20px;"><b style="font-size:20px; color:#434a66; line-height: 30px;">Sonder Agency, LLC</b><br>Sandbox DBA<br>TEMPE, AZ 85280<br>(623) 694 - 7560<br></td>
            </tr>
            <tr style="background: #14426E;">
               <td style="text-align:center; padding: 10px 0;"><span style="color:#FFFFFF; letter-spacing: .5px;">Transaction ID:&nbsp;'. $output['id'] .'</span></td>
            </tr>
            <tr style="padding-top:30px;">
               <td>
                  <h1 style="text-align:center; font-size:28px; font-weight:300; letter-spacing:1px; color:#434a66;">Transaction Details</h1>
               </td>
            </tr>
            <tr style=" right bottom  no-repeat;">
               <td><br><table style="font-family: arial;">
                     <tbody>
                        <tr style="line-height: 10px;">
                           <td width="80"><img src="https://ccc.basecommercesandbox.com/email_templates/images/clock_icon.png" style="padding-left:42px;" width="23" height="23" alt=""></td>
                           <td width="215" style="color: #163f64;"><b>Date/Time:</b></td>
                           <td>'. date('l, F j Y h:i A T' ) .'</td>
                        </tr>
                     </tbody>
                  </table><br><table style="font-family: arial;">
                     <tbody>
                        <tr style="line-height: 10px;">
                           <td width="80"><img src="https://ccc.basecommercesandbox.com/email_templates/images/folder_icon.png" style="padding-left: 42px;" height="23" width="23" alt=""></td>
                           <td width="215" style="color: #163f64;"><b>Account Name:</b></td>
                           <td>'.$output['card_name'].'</td>
                        </tr>
                        <tr>
                           <td width="80"></td>
                           <td width="215" style="color: #163f64;"><b>Card:</b></td>
                           <td>
                              Ending in
                              '. $output['card_num'].'
                           </td>
                        </tr>
                     </tbody>
                  </table><br><table style="font-family: arial;">
                     <tbody>
                        <tr style="line-height: 10px;">
                           <td width="80"><img src="https://ccc.basecommercesandbox.com/email_templates/images/transaction_icon.png" style="padding-left: 42px;" height="23" width="23" alt=""></td>
                           <td width="215" style="color: #163f64;"></td>
                           <td></td>
                        </tr>
                        <tr>
                           <td width="80"></td>
                           <td width="215" style="color: #163f64;"><b>Transaction Type:</b></td>
                           <td>SALE</td>
                        </tr>
                        <tr>
                           <td width="80"></td>
                           <td width="215" style="color: #163f64;"><b>Transaction Status:</b></td>
                           <td>CAPTURED</td>
                        </tr>
                     </tbody>
                  </table><br><table style="padding:0 80px; font-family: arial; width: 100%;">
                     <tbody>
                        <tr>
                           <td style="padding-top:20px; text-align:center;">
                              This will appear on your statement as<br>Sonder Agency, LLC
                           </td>
                        </tr>
                        <tr>
                           <td style="text-align:center; padding-top: 20px; padding-bottom:30px;"></td>
                        </tr>
                     </tbody>
                  </table>
                  <table style="font-family: arial; width: 100%;">
                     <tbody></tbody>
                  </table>
               </td>
            </tr>
            <tr style="background: #14426E;">
               <td style="padding:10px 20px 10px 80px; color: white; font-size:18px;"><span style="display: inline-block; width:  475px;">
                     Total:
                     </span>
                  $'. $output['amount'] .'
               </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>';
	  	wp_mail( $output['email'], 'Credit Card receipt from Sonder Agency, LLC', $content, $headers );
    }

//$newUrl = "http://sonder.dev/confirmation?arr=".json_encode($mssgs);
//echo $newUrl;
//header('Location: '.$newURL);
//die();
endif;
}

// Styles for login screen
function my_login_logo() { ?>
  <style type="text/css">
    #login h1 a, .login h1 a {
      background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/sonder-logo-full.svg);
      width: 300px;
      height: 75px;
      background-size: 300px 75px;
      margin-bottom: 50px;
    }
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
?>