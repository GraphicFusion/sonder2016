=== Pilot ===

Contributors: sonder agency
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.0
Tested up to: 4.3.1
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A starter theme called Pilot.

== Description ==

Pilot is a module-based starter theme for projects on a small or medium scale.  Individual modules are 
located inside the 'includes' folder and can be added or removed as necessary.  Grunt tasks compile and
minify their components.

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Pilot includes support for Infinite Scroll in Jetpack.

