<?php
/**
 * Template Name: Blog Page
 *
 * @package WordPress
 * @subpackage Pilot
 * @since Pilot
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<?php get_template_part( 'views/content', 'page' ); ?>
<?php endwhile; ?>

<div id="content" class="blog-page">
<div class="container">

<div class="blog-page-top">
<div class="wow fadeInUp" data-wow-duration="1s">  
<div class="post-search">
<form role="search" method="get" id="searchform" action="<?php bloginfo('url'); ?>">
    <input type="text" value="" name="s" id="s" placeholder="Search" />
    <input type="submit" id="searchsubmit" value="Search" />
</form>
</div>

<div class="categories-list">
<form id="category-select" class="category-select" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
		<?php
		$args = array(
			'show_option_none' => __( 'All Categories' ),
			'show_count'       => 0,
			'orderby'          => 'name',
			'echo'             => 0,
		);
		?>
		<?php $select  = wp_dropdown_categories( $args ); ?>
		<?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
		<?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>
		<?php echo $select; ?>
		<noscript>
			<input type="submit" value="View" />
		</noscript>
	</form>

</div>

<div class="archives-list">
<select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
  <option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option> 
  <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 0 ) ); ?>
</select>
</div>
</div>
</div>


	<?php query_posts('post_type=post&post_status=publish&posts_per_page=10&paged='. get_query_var('paged')); ?>
    
    <?php if( have_posts() ): ?>
    <?php while( have_posts() ): the_post(); ?>
	
	<div class="wow fadeInUp" data-wow-duration="1s">    
    <div id="post-<?php get_the_ID(); ?>" <?php post_class(); ?>>
		
        <div class="blog-section <?php echo (++$j % 2 == 0) ? 'evenpost' : 'oddpost'; ?>" >
    	<div class="blog-content">	
        	<div class="blog-text-intro">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <div class="post-meta">
                <time class="published"> <?php the_time('F j, Y') ?></time></span>
                </div>
                <?php the_excerpt(__('Continue reading »','example')); ?>
            </div>
        </div>
        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
        <div class="blog-post-img" style="background-position: right; background-repeat:no-repeat; background-size: cover; background-image: url('<?php echo $thumb['0'];?>')">
        	<?php //the_post_thumbnail('full'); ?>
        </div>
        
        
        
     </div>
    </div><!-- /#post-<?php get_the_ID(); ?> -->
	</div>

    <?php endwhile; ?>



    <div class="navigation">
    <span class="older"><?php next_posts_link(__('« Older Posts ','example')) ?></span>
    <span class="newer"><?php previous_posts_link(__('Newer Posts »','example')) ?></span> 
    </div><!-- /.navigation -->
    
    <?php else: ?>
    
    <div id="post-404" class="noposts">
    
    <p><?php _e('None found.','example'); ?></p>
    
    </div><!-- /#post-404 -->
    
    <?php endif; wp_reset_query(); ?>
</div>
</div><!-- /#content -->

<?php get_footer(); ?>
