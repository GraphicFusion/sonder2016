(function() {

    wow = new WOW({
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       75,          // default
        mobile:       true,       // default
        live:         true        // default
    });
    wow.init();

})();

// Configuration for PACE screen loader

paceOptions = {
  target: '.brandingNav',
};

// Cool header scroll effect
    
$(document).scroll(function() {
    var x = $(this).scrollTop();
    if ( $(window).width() > 768 ) {
        $('.header-media .bg-image, .header-banner .bg-image').css({
            'transform' : 'translateY(' + x/1.5 + 'px)'
        });
        $('.header-media .title section, .header-banner .title section').css({
            'transform' : 'translateY(' + x/3 + 'px)',
            'opacity': 1 - (x/300)
        });
    }
});


jQuery(document).ready(function($) {

    // Remove opacity for loader

    Pace.once('done', function () {
        $('.site-content').removeClass('loading');
        $('.site-footer').removeClass('loading');
    })
});

