var paymentForm = $('#gform_4');

/**
* Focus in form when clicked on label
*/

paymentForm.find('label').on('click tap', function(){
  $(this).siblings('input, select').focus();
});


/**
* Add the data attributes
*/

$('#input_4_1_1, #input_4_1_5, #input_4_1_2_month, #input_4_1_2_year').attr('data-side', 'front');
$('#input_4_1_1').attr('maxlength', 16);
$('#input_4_1_3').attr('data-side', 'back').attr('maxlength', 3);

$('#input_4_1_3').attr('data-focusable', 'ccv');
$('#input_4_1_1').attr('data-focusable', 'card-number');
$('#input_4_1_5').attr('data-focusable', 'cardholder');
$('#input_4_1_2_month').attr('data-focusable', 'month');
$('#input_4_1_2_year').attr('data-focusable', 'year');

/**
 * Flip card an highlight focused field on card
 */
paymentForm.find('input, select').on('focus', function(){

  //flip to the correct side of the card
  var side = $(this).attr('data-side');
  var card = $('.reference-card');

  (side !== 'back') ? card.removeClass('flipped') : card.addClass('flipped');

  //focus field on the card
  var field = $(this).attr('data-focusable');
  var focusables = card.find('.focusable');
  focusables.removeClass('focused');
  card.find('.' + field + '.focusable').addClass('focused');

});

/**
 * Update info on the card - card number
 */
paymentForm.find('input[name="input_1.1"]').on('keyup', function(){
  var value = $(this).val();
  var cardNumber = $('.reference-card .card-number');
  
  //add filled class if value exists
  if(value.length > 0){
    $(this).addClass('filled');
  }

  //remove previous value
  cardNumber.find('div').remove();

  //split value in groups of four
  for(var i = 0; i<4; i++){
      var part = '<div>{{number}}</div>';
      var number = value.slice(4 * i, (4 * i) + 4);
      part = part.replace('{{number}}', number);
      cardNumber.append(part);
  }

  var icon = $('.card-icon');
  
  icon.attr('class', 'card-icon fa ' + getCardIcon(value).icon);

});

/**
 * Update info on the card - all others
 */

paymentForm.find('input').not('input[name="input_1.1"]').not('#input_4_1_2_month').not('#input_4_1_2_year').on('keyup', function(){

  var value = $(this).val();
      //add filled class if value exists

  if(value.length > 0){
    $(this).addClass('filled');
  }
  
  var fieldOnCard = $(this).attr('data-focusable');

  $('.focusable.' + fieldOnCard).text(value);
});

/**
 * Update select options info
 */

paymentForm.find('select').on('change', function () {

  var value = $(this).val();

  if(value.length > 0){
    $(this).addClass('filled');
  }
  
  var fieldOnCard = $(this).attr('data-focusable');

  $('.focusable.' + fieldOnCard).text(value);
});

function getCardIcon(number){
  var types = [
    {name: "visa", icon:"gform_card_icon_visa", re: /^4[0-9]{6,}$/},
    {name: "mastercard", icon:"gform_card_icon_mastercard", re: /^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$/},
    {name: "amex", icon:"gform_card_icon_amex", re: /^3[47][0-9]{5,}$/}/*,
    {name: "diners", icon:"fa-cc-diners-club", re: /^3(?:0[0-5]|[68][0-9])[0-9]{4,}$/},
    {name: "discover", icon:"fa-cc-discover", re: /^6(?:011|5[0-9]{2})[0-9]{3,}$/},
    {name: "jcb", icon:"fa-cc-jcb", re: /^(?:2131|1800|35[0-9]{3})[0-9]{3,}$/}*/
  ];

  var type = {name: "general", icon:"fa-credit-card"};

  $.each(types, function(i, v){
    if(v.re.test(number)){
        type = v;
        return false;
    }
  });

  return type;
}
